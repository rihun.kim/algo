// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "10 11 12";
input = input.toString().trim().split(" ");

input = input.map((v) => BigInt(v));
let A = input[0];
let B = input[1];
let C = input[2];

console.log(Number(pow(A, B, C)));

function pow(A, B, C) {
  if (B === BigInt(0)) {
    return BigInt(1);
  } else if (B % BigInt(2) == BigInt(0)) {
    const t = BigInt(pow(A, B / BigInt(2), C)) % C;
    return (t * t) % C;
  } else {
    return (BigInt(A % C) * BigInt(pow(A, B - BigInt(1), C) % C)) % C;
  }
}
