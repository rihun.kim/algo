// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "2\n6 12 12\n30 50 31\n";
input = input.toString().trim().split("\n");

let N = +input.shift();
while (N--) {
  let [Y, X, target] = input
    .shift()
    .split(" ")
    .map((v) => +v);

  let floor = target % Y == 0 ? Y : target % Y;
  let room = Math.ceil(target / Y);

  if (room < 10) console.log(floor + "0" + room);
  else console.log(floor + "" + room);
}
