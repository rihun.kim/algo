// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "5\n1 10 3\n0\n1 100 2\n1 20 1\n0";
input = input.toString().trim().split("\n");

input.shift();

let ans = 0;
let stack = [];
for (let i = 0; i < input.length; ++i) {
  let [BOOL, POINT, TIME] = input[i].split(" ").map((v) => +v);

  if (BOOL) {
    --TIME;
    if (TIME == 0) ans += POINT;
    else stack.unshift([POINT, TIME]);
  } else {
    let val = stack.shift();
    if (val != null) {
      --val[1];
      if (val[1] == 0) ans += val[0];
      else stack.unshift(val);
    }
  }
}

console.log(ans);
