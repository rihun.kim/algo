// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "2\n0 0 10 10\n2 2 6 6";
input = input.toString().trim().split("\n");

input.shift();

let matrix = Array.from({ length: 101 }, () => Array.from({ length: 101 }, () => 0));

for (let i = 0; i < input.length; ++i) {
  const [X, Y, W, H] = input[i].split(" ").map((v) => +v);
  for (let x = X; x < X + W; ++x) for (let y = Y; y < Y + H; ++y) matrix[y][x] = i + 1;
}

let ans = [];
for (let i = 0; i < input.length; ++i) {
  const [X, Y, W, H] = input[i].split(" ").map((v) => +v);
  let cnt = 0;
  for (let x = X; x < X + W; ++x) for (let y = Y; y < Y + H; ++y) if (matrix[y][x] == i + 1) ++cnt;

  ans.push(cnt);
}

console.log(ans.join("\n"));
