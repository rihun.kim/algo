// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

input = input.toString().trim().split("");

let dic = [
  [3, "ABC"],
  [4, "DEF"],
  [5, "GHI"],
  [6, "JKL"],
  [7, "MNO"],
  [8, "PQRS"],
  [9, "TUV"],
  [10, "WXYZ"],
];

let time = 0;
for (let i = 0; i < input.length; ++i) {
  for (let d = 0; d < dic.length; ++d) {
    if (dic[d][1].includes(input[i])) {
      time += dic[d][0];
      break;
    }
  }
}

console.log(time);
