// 완전탐색(DFS) + 구현
// 다시 코드 잘 살펴보기

let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");

// let input = "5 3\n0 0 1 0 0\n0 0 2 0 1\n0 1 2 0 0\n0 0 1 0 0\n0 0 0 0 2";
input = input.toString().trim().split("\n");

const [N, M] = input.shift().split(" ");
let matrix = [];
makeMatrix(input);

let chicken = [];
let house = [];
for (let y = 0; y < N; ++y) {
  for (let x = 0; x < N; ++x) {
    if (matrix[y][x] == 1) house.push([y, x]);
    else if (matrix[y][x] == 2) chicken.push([y, x]);
  }
}

let ans = Infinity;
let visited = [];

dfs(0, 0);
console.log(ans);

function dfs(idx, cnt) {
  if (cnt == M) {
    // M : 제거되지 않을 최대 치킨집
    let temp = 0;
    for (let h = 0; h < house.length; ++h) {
      let dist = Infinity;
      for (let c = 0; c < chicken.length; ++c) {
        if (visited[c]) dist = Math.min(dist, distance(house[h], chicken[c]));
      }
      temp += dist;
    }
    ans = Math.min(ans, temp);
    return;
  }

  if (idx == chicken.length) return;

  visited[idx] = true;
  dfs(idx + 1, cnt + 1); // cnt+1 : 치킨집 선택되서 수량 늘음
  visited[idx] = false;
  dfs(idx + 1, cnt);
}

function distance(house, chicken) {
  return Math.abs(chicken[0] - house[0]) + Math.abs(chicken[1] - house[1]);
}

function makeMatrix(input) {
  for (let y = 0; y < N; ++y) {
    let temp = input[y].split(" ").map((v) => +v);
    matrix.push(temp);
  }
}
