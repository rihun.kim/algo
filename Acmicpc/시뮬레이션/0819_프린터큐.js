// 프린터큐는 두 개를 준비해야한다.
// 우선순위 큐를 해야하므로, 두 개의 큐를 준비한다.

// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin").toString();

let input = "3\n1 0\n5\n4 2\n1 2 3 4\n6 0\n1 1 9 1 1 1";
input = input.toString().trim().split("\n");

let N = input.shift();
let ans = [];
while (N--) {
  let targetIndex = input.shift().split(" ")[1];
  let data = input.shift();
  let valueQ = [];
  let indexQ = [];

  data.split(" ").map((v, i) => {
    valueQ.push(+v);
    indexQ.push(i);
  });

  let index = 0;
  while (true) {
    let max = Math.max(...valueQ);

    let val = valueQ.shift();
    let idx = indexQ.shift();
    if (val == max) {
      if (idx == targetIndex) {
        ans.push(index + 1);
        break;
      } else ++index;
    } else {
      valueQ.push(val);
      indexQ.push(idx);
    }
  }
}

console.log(ans.join("\n").trim());
