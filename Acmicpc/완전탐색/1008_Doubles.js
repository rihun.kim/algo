// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

input = "1 4 3 2 9 7 18 22 0\n2 4 8 10 0\n7 5 11 13 1 3 0\n-1\n";
input = input.toString().trim().split("\n");

for (let i = 0; i < input.length - 1; ++i) {
  let nums = input[i].split(" ").map((v) => +v);
  nums.pop();

  let arr = Array.from({ length: 100 }, () => 0);
  for (let num of nums) arr[num] = num;

  let cnt = 0;
  for (let num of nums) if (num * 2 <= 100 && arr[num * 2] != 0) ++cnt;

  console.log(cnt);
}
