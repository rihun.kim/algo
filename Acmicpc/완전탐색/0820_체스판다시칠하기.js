// 카카오 문제와 비슷

let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");
input = input.toString().trim().split("\n");

let [Y, X] = input.shift().split(" ");
let ans = Infinity;
for (let a = 0; a <= Y - 8; ++a) {
  for (let b = 0; b <= X - 8; ++b) {
    for (let type = 0; type < 2; ++type) {
      let paint = type == 0 ? "B" : "W";
      let cnt = 0;
      for (let y = a; y < 8 + a; ++y) {
        for (let x = b; x < 8 + b; ++x) {
          if (paint != input[y][x]) ++cnt;
          paint = paint == "B" ? "W" : "B";
        }
        paint = paint == "B" ? "W" : "B";
      }
      ans = Math.min(ans, cnt);
    }
  }
}

console.log(ans);
