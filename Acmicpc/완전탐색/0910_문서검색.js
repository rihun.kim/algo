let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");

input = input.toString().trim().split("\n");

let source = input[0];
let comparer = input[1];

let ans = 0;
for (let sIdx = 0; sIdx <= source.length - comparer.length; ++sIdx) {
  let cnt = 0;
  for (let i = sIdx; i <= source.length - comparer.length; ) {
    let flag = true;
    for (let j = i, k = 0; k < comparer.length; ++j, ++k) {
      if (source[j] != comparer[k]) {
        flag = false;
        break;
      }
    }

    if (flag) {
      i += comparer.length;
      ++cnt;
    } else ++i;
  }
  ans = Math.max(ans, cnt);
}

console.log(ans);
