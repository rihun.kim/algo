// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "2\n5 6\n0 0 1 0\n";
input = input.toString().trim().split("\n");

let N = input.shift();
let arr = input
  .shift()
  .split(" ")
  .map((v) => +v);
let op = input
  .shift()
  .split(" ")
  .map((v) => +v);

let max = -1 * Infinity;
let min = Infinity;
dfs(0, arr[0], op);

console.log(max ? max : 0);
console.log(min ? min : 0);

function dfs(idx, sum, op) {
  if (idx == N - 1) {
    max = Math.max(max, sum);
    min = Math.min(min, sum);
    return;
  }

  for (let i = 0; i < 4; ++i) {
    if (op[i] >= 1) {
      --op[i];
      if (i == 0) dfs(idx + 1, sum + arr[idx + 1], op);
      else if (i == 1) dfs(idx + 1, sum - arr[idx + 1], op);
      else if (i == 2) dfs(idx + 1, sum * arr[idx + 1], op);
      else {
        if (sum >= 0) dfs(idx + 1, Math.floor(sum / arr[idx + 1]), op);
        else dfs(idx + 1, -Math.floor(-sum / arr[idx + 1]), op);
      }
      ++op[i];
    }
  }
}
