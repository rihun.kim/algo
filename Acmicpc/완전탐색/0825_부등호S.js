// [0-9][0-9][0-9] 이렇게 각 숫자 만들기 문제는
// 백트래킹(DFS) 을 이용하여 풀되
// 매개변수에 각 자리의 숫자와 결과값의 문자열을
// 계속 해서 넘겨주고, visited 방문내역을 계속 기록한다.
// 방문내역은 백트래킹의 핵심이므로
// 항상 구비하며
// 매개변수의 숫자와 문자열을 넣는 센스는 항상 필두

// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "2\n< >";
input = input.toString().trim().split("\n");
let N = +input.shift();

let ops = input.shift().split(" ");
let visited = [];
let ans = [];
for (let i = 0; i <= 9; ++i) {
  visited[i] = true;
  dfs(i, 0, "" + i);
  visited[i] = false;
}
console.log(ans[ans.length - 1]);
console.log(ans[0]);

function dfs(val, cnt, str) {
  if (cnt == N) {
    ans.push(str);
  } else {
    for (let i = 0; i <= 9; ++i) {
      if (ops[cnt] == "<") if (val > i) continue;
      if (ops[cnt] == ">") if (val < i) continue;

      if (!visited[i]) {
        visited[i] = true;
        dfs(i, cnt + 1, str + i);
        visited[i] = false;
      }
    }
  }
}
