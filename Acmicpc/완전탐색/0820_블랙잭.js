// dfs 로 하나 뽑고, 안뽑고
// 중요한 건, idx 는 계속 늘어난다는 것

// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "10 500\n93 181 245 214 315 36 185 138 216 295\n";
input = input.toString().trim().split("\n");

const [N, M] = input
  .shift()
  .split(" ")
  .map((v) => +v);

const arr = input
  .shift()
  .split(" ")
  .map((v) => +v)
  .sort((a, b) => b - a);

let ans = -1;
dfs(0, 0, 0);
console.log(ans);

function dfs(idx, sum, cnt) {
  if (cnt == 3 && sum <= M) {
    ans = Math.max(ans, sum);
    return;
  } else if (cnt >= 4) return;

  if (idx >= N) return;

  sum += arr[idx];
  dfs(idx + 1, sum, cnt + 1);
  sum -= arr[idx];
  dfs(idx + 1, sum, cnt);
}
