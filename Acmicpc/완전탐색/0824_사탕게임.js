// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "4\nCCCP\nCCCP\nCCCP\nCCCP\n";
input = input.toString().trim().split("\n");
let N = input.shift();

let matrix = [];
for (let i = 0; i < N; ++i) matrix.push(input[i].split(""));

let ans = 0;
for (let y = 0; y < N; ++y) {
  for (let x = 0; x < N; ++x) {
    // 오른쪽과 교환
    let temp = matrix[y][x];
    if (x + 1 < N && matrix[y][x] != matrix[y][x + 1]) {
      matrix[y][x] = matrix[y][x + 1];
      matrix[y][x + 1] = temp;
      exam(matrix);
      matrix[y][x + 1] = matrix[y][x];
      matrix[y][x] = temp;
    }

    // 아래쪽과 교환
    temp = matrix[y][x];
    if (y + 1 < N && matrix[y + 1][x] != matrix[y][x]) {
      matrix[y][x] = matrix[y + 1][x];
      matrix[y + 1][x] = temp;
      exam(matrix);
      matrix[y + 1][x] = matrix[y][x];
      matrix[y][x] = temp;
    }
  }
}
console.log(ans);

function exam(matrix) {
  for (let y = 0; y < N; ++y) {
    for (let x = 0; x < N; ++x) {
      // 좌우 검사
      let cnt = 1;
      let lxx = x - 1;
      let rxx = x + 1;
      let lflag = true;
      let rflag = true;
      let startNum = matrix[y][x];
      while (true) {
        if (lflag && 0 <= lxx && startNum == matrix[y][lxx--]) ++cnt;
        else lflag = false;

        if (rflag && rxx < N && startNum == matrix[y][rxx++]) ++cnt;
        else rflag = false;

        if (!lflag && !rflag) break;
      }
      ans = Math.max(ans, cnt);

      // 위아래쪽 검사
      cnt = 1;
      let uyy = y - 1;
      let dyy = y + 1;
      let upflag = true;
      let downflag = true;
      while (true) {
        if (upflag && 0 <= uyy && startNum == matrix[uyy--][x]) ++cnt;
        else upflag = false;

        if (downflag && dyy < N && startNum == matrix[dyy++][x]) ++cnt;
        else downflag = false;

        if (!upflag && !downflag) break;
      }
      ans = Math.max(ans, cnt);
    }
  }
}
