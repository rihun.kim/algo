const input = +require("fs").readFileSync("/dev/stdin");

let ans = 0;
for (let i = 1; i <= input; ++i) {
  if (i.toString().length == 1) {
    if (input == i * 2) {
      ans = i;
      break;
    }
  } else if (i.toString().length >= 2) {
    let str = i.toString();
    let sum = str
      .split("")
      .map((v) => +v)
      .reduce((prev, curr) => prev + curr);
    if (input == i + sum) {
      ans = i;
      break;
    }
  }
}

console.log(ans);

//////////////
// const input = +require("fs").readFileSync("/dev/stdin");
const input = 216;

function test(num) {
  let temp = num;
  let sum = num;
  while (temp) {
    sum += temp % 10;
    temp = parseInt(temp / 10);
  }
  return sum;
}

let ans = [];
for (let i = 1; i <= input; ++i) {
  if (test(i) == input) {
    ans.push(i);
    break;
  }
}

ans.length == 0 ? console.log(0) : console.log(ans[0]);
