// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "7\n8\n10\n13\n15\n19\n20\n23\n25\n";
input = input.toString().trim().split("\n");

input = input.map((v) => +v);

dfs(0, [], 0, 0);

function dfs(idx, arr, cnt, sum) {
  if (cnt == 7) {
    if (sum == 100) {
      console.log(arr.join("\n").trim());
    }
    return;
  } else if (cnt > 8 || sum > 100 || idx >= input.length) return;

  arr.push(input[idx]);
  sum += input[idx];
  dfs(idx + 1, arr, cnt + 1, sum);
  arr.pop();
  sum -= input[idx];
  dfs(idx + 1, arr, cnt, sum);
}
