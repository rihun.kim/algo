// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "2 25 25 25 25";
input = input.toString().trim().split(" ");

let N = input.shift();
let probab = []; // 동, 서, 남, 북
for (let i = 0; i < 4; ++i) probab[i] = input[i] / 100;

let checked = Array.from({ length: 29 }, () => Array.from({ length: 29 }, () => false));

let dy = [-1, 1, 0, 0];
let dx = [0, 0, -1, 1];
checked[14][14] = 1;

let res = dfs(14, 14, 0);

console.log(res.toFixed(10));

function dfs(y, x, cnt) {
  if (cnt == N) return 1;

  checked[y][x] = true;
  let res = 0;

  for (let i = 0; i < 4; ++i) {
    let ny = dy[i] + y;
    let nx = dx[i] + x;

    if (checked[ny][nx] == true) continue;
    res = res + probab[i] * dfs(ny, nx, cnt + 1);
  }

  checked[y][x] = false;
  return res;
}
