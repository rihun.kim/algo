const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.on("line", function (line) {
  let input = line.split(" ");

  const num = Number(input);
  let cnt = 0;
  for (let i = 1; i <= num; ++i) {
    let temp = "" + i;
    let str = temp.split("");
    let len = str.length;

    if (i <= 99) ++cnt;
    else {
      let diff = +str[0] - +str[1];
      let flag = true;
      for (let j = 1; j < len - 1; ++j) {
        if (diff != +str[j] - +str[j + 1]) {
          flag = false;
          break;
        }
      }

      if (flag) ++cnt;
    }
  }

  console.log(cnt);
}).on("close", function () {
  process.exit();
});

///////////////////
const n = +require("fs").readFileSync("/dev/stdin");

function test(n) {
  // INT 형 숫자를 문자로 변경 및 바로 배열화
  const arr = Array.from(n.toString());

  let diff = null;
  // 순차대로 비교 진행
  for (let i = arr.length - 1; i > 0; --i) {
    if (diff == null) {
      // 최초상황에만 유효하도록 설정
      diff = arr[i] - arr[i - 1];
      continue;
    }

    // 다르면 false 화
    if (arr[i] - arr[i - 1] != diff) return false;
  }

  return true;
}

let cnt = 0;
// 반복문으로 명확하게 완전탐색 상황 정리
// 1 부터 n 까지 다 탐색해야 하는 상황 정의
for (let i = 1; i <= n; ++i) {
  if (test(i)) ++cnt; // 테스트 하는 부분을 함수로 모듈화
}

console.log(cnt);
