let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");

let answer = [];
let num = 0;
while (input > answer.length - 1) {
  let tmp, itr, isAnswer, pow;
  tmp = num.toString();
  itr = tmp.length - 1;
  isAnswer = true;
  for (let i = 0; i < itr; i++) {
    if (tmp[i] <= tmp[i + 1]) {
      isAnswer = false;
      // 핵심부분
      // jump 구현부분
      pow = itr - i;
      break;
    }
  }
  if (isAnswer) {
    answer.push(num);
    num += 1;
  } else {
    if (num > 9876543210) {
      answer.push(-1);
      break;
    }
    // 자릿수를 올려버림
    // 54331 이렇게 됬을때
    // i = 2, itr = 4
    // pow = 2
    // 100 - 31 = 69
    // 54331 + 69 = 54400 이 수에서 다시 시작
    num += Math.pow(10, pow) - (num % Math.pow(10, pow));
  }
}

console.log(answer.pop());
