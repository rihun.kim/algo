// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "5 8\nTATGATAC\nTAAGCTAC\nAAAGATCC\nTGAGATAC\nTAAGATGT\n";
input = input.toString().trim().split("\n");

input.shift();
const N = input.length;
const LEN = input[0].length;

let ans = "";
let cnt = 0;
for (let i = 0; i < LEN; ++i) {
  let arr = [
    { tag: "A", cnt: 0 },
    { tag: "C", cnt: 0 },
    { tag: "G", cnt: 0 },
    { tag: "T", cnt: 0 },
  ];

  for (let word of input) {
    if (word[i] == "A") ++arr[0].cnt;
    else if (word[i] == "C") ++arr[1].cnt;
    else if (word[i] == "G") ++arr[2].cnt;
    else ++arr[3].cnt;
  }

  arr.sort((a, b) => {
    if (a.cnt < b.cnt) return 1;
    else {
      if (a.cnt == b.cnt) {
        if (a.tag.localeCompare(b.tag) == 1) return 1;
        else return -1;
      } else return -1;
    }
  });

  ans += arr[0].tag;
  cnt += N - arr[0].cnt;
}

console.log(ans);
console.log(cnt);
