// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "6 5 2\n1 0 1 0 1\n0 1 0 1 0\n1 1 0 0 0\n0 0 0 1 1\n1 1 0 1 1\n0 0 1 0 0\n";
input = input.toString().trim().split("\n");

let [Y, X, D] = input
  .shift()
  .split(" ")
  .map((v) => +v);

let matrix = [];
for (let i = 0; i < input.length; ++i) {
  matrix.push(input[i].split(" ").map((v) => +v));
}
matrix.push(Array.from({ length: matrix.length }, () => 8));

let AY = matrix.length - 1;
let maxAns = 0;
let ans = 0;

let trial = 0;

dfs(0, [], 0);
console.log(maxAns);

function goWork(arr) {
  let map = JSON.stringify(matrix);
  map = JSON.parse(map);

  ans = 0;
  while (remain(map)) {
    attack(map, [
      [AY, arr[0]],
      [AY, arr[1]],
      [AY, arr[2]],
    ]);
    forward(map);
  }

  maxAns = Math.max(ans, maxAns);
}

function dfs(idx, arr, cnt) {
  if (cnt == 3) {
    goWork(arr);
    return;
  } else if (cnt > 3 || idx >= X) return;

  arr.push(idx);
  dfs(idx + 1, arr, cnt + 1);
  arr.pop();
  dfs(idx + 1, arr, cnt);
}

function attack(matrix, archor) {
  let deathNote = [];
  for (let arcIdx = 0; arcIdx < 3; ++arcIdx) {
    let targets = [];
    for (let y = matrix.length - 2; y >= 0; --y) {
      for (let x = 0; x < X; ++x) {
        let dist = distance(y, x, archor[arcIdx][0], archor[arcIdx][1]);
        // console.log(y, x, archor[0], archor[1], "dist:", dist);
        if (matrix[y][x] == 1 && dist <= D) {
          targets.push([y, x, dist]);
        }
      }
    }

    if (targets.length != 0) {
      targets = sorting(targets);
      deathNote.push([targets[0][0], targets[0][1]]);
    }
  }

  for (let i = 0; i < deathNote.length; ++i) {
    if (matrix[deathNote[i][0]][deathNote[i][1]] == 1) {
      ++ans;
      matrix[deathNote[i][0]][deathNote[i][1]] = 0;
    }
  }
}

function distance(y1, x1, y2, x2) {
  return Math.abs(y1 - y2) + Math.abs(x1 - x2);
}

function sorting(targets) {
  return targets.sort((a, b) => {
    if (a[2] == b[2]) {
      return a[1] - b[1];
    } else {
      return a[2] - b[2];
    }
  });
}

function forward(matrix) {
  for (let y = matrix.length - 2; y > 0; --y) matrix[y] = matrix[y - 1];
  matrix[0] = Array.from({ length: matrix.length - 1 }, () => 0);
}

function remain(matrix) {
  for (let y = matrix.length - 2; y >= 0; --y) {
    for (let x = 0; x < X; ++x) {
      if (matrix[y][x] == 1) {
        return true;
      }
    }
  }

  return false;
}
