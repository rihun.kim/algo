// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

// let input = "5\n6 8 2 6 2\n3 2 3 4 6\n6 7 3 3 2\n7 2 5 3 6\n8 9 5 2 7\n";
let input = "3\n1 1 1\n1 1 1\n1 1 1\n";
input = input.toString().trim().split("\n");

const N = input.shift();
const Y = [0, 0, 1, -1];
const X = [1, -1, 0, 0];

let matrix = [];
for (let i = 0; i < input.length; ++i) matrix.push(input[i].split(" ").map((v) => +v));

// 비가 안오는 경우도 체크했기에 1로 시작
let ans = 1;

for (let i = 1; i <= 100; ++i) {
  for (let y = 0; y < N; ++y) {
    for (let x = 0; x < N; ++x) {
      if (matrix[y][x] <= i) matrix[y][x] = 0;
    }
  }

  let cnt = 0;
  let visited = makeVisited();
  for (let y = 0; y < N; ++y) {
    for (let x = 0; x < N; ++x) {
      if (!visited[y][x] && matrix[y][x] != 0) {
        dfs(y, x, visited);
        ++cnt;
      }
    }
  }

  ans = Math.max(ans, cnt);
}

console.log(ans);

function dfs(y, x, visited) {
  visited[y][x] = true;
  for (let j = 0; j < 4; ++j) {
    let dy = y + Y[j];
    let dx = x + X[j];
    if (0 <= dx && dx < N && 0 <= dy && dy < N && !visited[dy][dx] && matrix[dy][dx] != 0) {
      dfs(dy, dx, visited);
    }
  }
}

function makeVisited() {
  return Array.from({ length: N }, () => Array.from({ length: N }, () => false));
}
