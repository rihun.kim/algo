// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "3\n10\n20\n1000\n";
input = input.toString().trim().split("\n");
let N = input.shift();

let ans = [];
while (N--) {
  let num = input.shift();
  let flag = false;
  for (let a = 1; a < num; ++a) {
    for (let b = 1; b < num; ++b) {
      if ((a * (a + 1)) / 2 + (b * (b + 1)) / 2 > num) break;
      for (let c = 1; c < num; ++c) {
        if ((a * (a + 1)) / 2 + (b * (b + 1)) / 2 + (c * (c + 1)) / 2 > num) break;

        if ((a * (a + 1)) / 2 + (b * (b + 1)) / 2 + (c * (c + 1)) / 2 == num) {
          flag = true;
          ans.push(1);
        }

        if (flag) break;
      }
      if (flag) break;
    }
    if (flag) break;
  }
  if (!flag) ans.push(0);
}

console.log(ans.join("\n"));
