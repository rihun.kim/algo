// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input =
  "7 3\n2 0 2 0 1 1 0\n0 0 1 0 1 0 0\n0 1 1 1 1 0 0\n2 1 0 0 0 0 2\n1 0 0 0 0 1 1\n0 1 0 0 0 0 0\n2 1 0 0 2 0 2\n";
input = input.toString().trim().split("\n");
const [N, M] = input.shift().split(" ");

let matrix = [];
let visited = [];
let zeroCnt = 0;
let ans = Infinity;
for (let i = 0; i < input.length; ++i) {
  matrix.push(input[i].split(" ").map((v) => +v));
  visited.push(input[i].split(" ").map((v) => false));
}

let virus = [];
for (let y = 0; y < N; ++y)
  for (let x = 0; x < N; ++x) {
    if (matrix[y][x] == 2) virus.push([y, x]);
    if (matrix[y][x] == 0) ++zeroCnt;
  }

let dy = [0, 0, 1, -1];
let dx = [1, -1, 0, 0];

if (zeroCnt == 0) {
  console.log(0);
  return;
}

dfs(0, [], 0);
console.log(ans == Infinity ? -1 : ans);

function dfs(idx, arr) {
  if (arr.length == M) {
    bfs(arr);

    return;
  } else if (idx >= virus.length) return;

  arr.push(virus[idx]);
  dfs(idx + 1, arr);
  arr.pop();
  dfs(idx + 1, arr);
}

function bfs(arr) {
  let queue = JSON.stringify(arr);
  queue = JSON.parse(queue);
  let map = JSON.stringify(matrix);
  map = JSON.parse(map);
  let vis = JSON.stringify(visited);
  vis = JSON.parse(vis);

  for (let i = 0; i < arr.length; ++i) map[arr[i][0]][arr[i][1]] = "*";

  let cnt = 0;
  let time = 0;
  let flag = false;
  while (queue.length != 0) {
    queue = JSON.stringify(queue);
    let tempQueue = JSON.parse(queue);
    queue = [];
    while (tempQueue.length != 0) {
      let point = tempQueue.shift();
      for (let i = 0; i < 4; ++i) {
        let ny = point[0] + dy[i];
        let nx = point[1] + dx[i];

        if (borderChk(ny, nx) && !vis[ny][nx] && canGo(ny, nx, map)) {
          vis[ny][nx] = true;
          if (map[ny][nx] == 0) ++cnt;
          map[ny][nx] = "A";

          if (cnt == zeroCnt) {
            queue = [];
            flag = true;
            break;
          } else {
            queue.push([ny, nx]);
          }
        }
      }
    }
    ++time;
  }

  if (flag) {
    ans = Math.min(ans, time);
  }
}

function canGo(y, x, map) {
  if (map[y][x] == 0 || map[y][x] == 2) return true;
  return false;
}

function borderChk(y, x) {
  if (0 <= y && y < N && 0 <= x && x < N) return true;
  return false;
}
