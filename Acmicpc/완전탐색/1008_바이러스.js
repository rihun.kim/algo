// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

input = "7\n6\n1 2\n2 3\n1 5\n5 2\n5 6\n4 7\n";
input = input.toString().trim().split("\n");

const N = input.shift();
const M = input.shift();

let map = new Map();
for (let i = 1; i <= N; ++i) map[i] = [];

input.map((v) => {
  let [a, b] = v.split(" ");

  map[a].push(+b);
  map[b].push(+a);
});

for (let key in map) map[key].sort((a, b) => a - b);

let checked = Array.from({ length: N + 1 }, () => false);
let ans = [1];
dfs(1, checked, ans);
console.log(ans.length - 1);

function dfs(start, checked, ans) {
  checked[start] = true;
  for (let i = 0; i < map[start].length; ++i) {
    if (!checked[map[start][i]]) {
      checked[map[start][i]] = true;
      ans.push(map[start][i]);
      dfs(map[start][i], checked, ans);
    }
  }
}
