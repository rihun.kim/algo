// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input =
  "8\n0 5 4 5 4 5 4 5\n4 0 5 1 2 3 4 5\n9 8 0 1 2 3 1 2\n9 9 9 0 9 9 9 9\n1 1 1 1 0 1 1 1\n8 7 6 5 4 0 3 2\n9 1 9 1 9 1 0 9\n6 5 4 3 2 1 9 0\n";
input = input.toString().trim().split("\n");

const N = input.shift();
let matrix = [];

for (let i = 0; i < input.length; ++i) matrix.push(input[i].split(" ").map((v) => +v));

const arr = [];
for (let i = 0; i < N; ++i) arr.push(i);

let teamA = [];
combi(arr, [], 0, N, N / 2);

let ans = Infinity;
for (let i = 0; i < teamA.length / 2; ++i) {
  let startTeam = teamA[i];
  let linkTeam = teamA[teamA.length - i - 1];

  let sumA = 0;
  for (let j = 0; j < startTeam.length - 1; ++j) {
    for (let k = j + 1; k < startTeam.length; ++k) {
      sumA += matrix[startTeam[j]][startTeam[k]] + matrix[startTeam[k]][startTeam[j]];
    }
  }

  let sumB = 0;
  for (let j = 0; j < linkTeam.length - 1; ++j) {
    for (let k = j + 1; k < linkTeam.length; ++k) {
      sumB += matrix[linkTeam[j]][linkTeam[k]] + matrix[linkTeam[k]][linkTeam[j]];
    }
  }

  ans = Math.min(Math.abs(sumA - sumB), ans);
}

console.log(ans);

function combi(source, target, idx, n, r) {
  if (r == 0) {
    teamA.push([...target]);
    return;
  } else if (n < r || idx == N) return;

  target.push(source[idx]);
  combi(source, target, idx + 1, n - 1, r - 1);
  target.pop();
  combi(source, target, idx + 1, n - 1, r);
}
