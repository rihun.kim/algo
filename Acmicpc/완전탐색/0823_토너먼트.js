// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "16 8 9";
input = input.toString().trim().split(" ");

let [N, A, B] = input.map((v) => +v);

for (let i = 1; i < N; ++i) {
  if (A % 2 == 0) A /= 2;
  else A = (A + 1) / 2;

  if (B % 2 == 0) B /= 2;
  else B = (B + 1) / 2;

  if (A == B) {
    console.log(i);
    return;
  }
}
