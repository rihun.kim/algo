// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input =
  "8\n1 1 0 0 0 0 1 1\n1 1 0 0 0 0 1 1\n0 0 0 0 1 1 0 0\n0 0 0 0 1 1 0 0\n1 0 0 0 1 1 1 1\n0 1 0 0 1 1 1 1\n0 0 1 1 1 1 1 1\n0 0 1 1 1 1 1 1\n";
input = input.toString().trim().split("\n");
const N = +input.shift();

let matrix = [];
for (let i = 0; i < input.length; ++i) {
  matrix.push(input[i].split(" ").map((v) => +v));
}

let queue = [{ x: 0, y: 0, xl: N, yl: N, l: N }];
let wcnt = 0;
let bcnt = 0;
let trial = 0;
while (queue.length != 0) {
  let point = queue.shift();
  if (!check(point.x, point.y, point.xl, point.yl)) {
    let len = point.l / 2;
    queue.push(
      { x: point.x, y: point.y, xl: point.x + len, yl: point.y + len, l: len },
      { x: point.x + len, y: point.y, xl: point.xl, yl: point.y + len, l: len },
      { x: point.x, y: point.y + len, xl: point.x + len, yl: point.yl, l: len },
      { x: point.x + len, y: point.y + len, xl: point.xl, yl: point.yl, l: len }
    );
  }
}
console.log(wcnt);
console.log(bcnt);

function check(Y, X, YY, XX) {
  let w = 0;
  let b = 0;
  for (let y = Y; y < YY; ++y) {
    for (let x = X; x < XX; ++x) {
      if (matrix[y][x] == 0) w = 1;
      else b = 1;

      if (w == 1 && b == 1) {
        return false;
      }
    }
  }

  if (w == 1) ++wcnt;
  else ++bcnt;

  return true;
}
