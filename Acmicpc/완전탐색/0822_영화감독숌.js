let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");
input = input.toString().trim().split("\n");

const N = +input.shift();
let cnt = 0;
for (let num = 665; num <= Infinity; ++num) {
  let str = "" + num;
  if (str.includes("666")) {
    ++cnt;
    if (cnt == N) {
      console.log(num);
      break;
    }
  }
}
