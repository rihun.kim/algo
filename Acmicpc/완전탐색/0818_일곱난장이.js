const fs = require("fs");
const x = fs.readFileSync("/dev/stdin").toString();
const input = x.trim().split("\n").map(Number);

// let input = [20, 7, 23, 19, 10, 15, 25, 8, 13];

input.sort((a, b) => a - b);
let arr = [];
permu(input, [], 0);
arr.sort((a, b) => a - b);

let ans = "";
for (let i = 0; i < arr.length; ++i) ans += arr[i] + "\n";
console.log(ans.trim());

function permu(source, target, cnt) {
  if (cnt >= 7) {
    if (target.reduce((prev, cur) => prev + cur) == 100) arr = [...target];
    return;
  }

  for (let i = 0; i < source.length; ++i) {
    let temp = [...source];
    let tag = temp.splice(i, 1);
    let temp2 = [...target];
    temp2.push(...tag);
    permu([...temp], [...temp2], cnt + 1);
  }
}
