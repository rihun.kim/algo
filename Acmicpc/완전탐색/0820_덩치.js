// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "5\n55 185\n58 183\n88 186\n60 175\n46 155\n";
input = input.toString().trim().split("\n");

input.shift();
let arr = input.map((v) => v.split(" ").map((c) => +c));

let ans = [];
for (let i = 0; i < arr.length; ++i) {
  let cnt = 0;
  for (let j = 0; j < arr.length; ++j) {
    if (i == j) continue;
    if (arr[i][0] < arr[j][0] && arr[i][1] < arr[j][1]) {
      ++cnt;
    }
  }
  ans.push(cnt + 1);
}

console.log(ans.join(" ").trim());
