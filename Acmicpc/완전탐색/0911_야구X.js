// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input =
  "50\n1 2 4 3 0 2 1 0 3\n1 2 1 2 0 0 0 0 1\n3 4 2 3 1 2 3 4 0\n0 1 2 3 4 2 1 0 0\n0 0 0 0 0 0 1 4 4\n0 4 0 4 0 4 0 4 0\n0 4 2 2 2 2 2 2 2\n1 1 1 1 1 1 1 1 0\n0 2 0 3 0 1 0 2 0\n1 2 4 3 0 2 1 0 3\n1 2 1 2 0 0 0 0 1\n3 4 2 3 1 2 3 4 0\n0 1 2 3 4 2 1 0 0\n0 0 0 0 0 0 1 4 4\n0 4 0 4 0 4 0 4 0\n0 4 2 2 2 2 2 2 2\n1 1 1 1 1 1 1 1 0\n0 2 0 3 0 1 0 2 0\n1 2 4 3 0 2 1 0 3\n1 2 1 2 0 0 0 0 1\n3 4 2 3 1 2 3 4 0\n0 1 2 3 4 2 1 0 0\n0 0 0 0 0 0 1 4 4\n0 4 0 4 0 4 0 4 0\n0 4 2 2 2 2 2 2 2\n1 1 1 1 1 1 1 1 0\n0 2 0 3 0 1 0 2 0\n1 2 4 3 0 2 1 0 3\n1 2 1 2 0 0 0 0 1\n3 4 2 3 1 2 3 4 0\n0 1 2 3 4 2 1 0 0\n0 0 0 0 0 0 1 4 4\n0 4 0 4 0 4 0 4 0\n0 4 2 2 2 2 2 2 2\n1 1 1 1 1 1 1 1 0\n0 2 0 3 0 1 0 2 0\n1 2 4 3 0 2 1 0 3\n1 2 1 2 0 0 0 0 1\n3 4 2 3 1 2 3 4 0\n0 1 2 3 4 2 1 0 0\n0 0 0 0 0 0 1 4 4\n0 4 0 4 0 4 0 4 0\n0 4 2 2 2 2 2 2 2\n1 1 1 1 1 1 1 1 0\n0 2 0 3 0 1 0 2 0\n3 4 2 3 1 2 3 4 0\n0 1 2 3 4 2 1 0 0\n0 0 0 0 0 0 1 4 4\n0 4 0 4 0 4 0 4 0\n0 4 2 2 2 2 2 2 2\n";
input = input.toString().trim().split("\n");

let N = +input.shift();

let arr = [];
for (let i = 1; i < 9; ++i) arr.push(i);

let inning = [];
for (let i = 0; i < input.length; ++i) inning.push([...input[i].split(" ").map((v) => +v)]);

let ans = 0;
permutation(arr, []);
console.log(ans);

function permutation(src, des) {
  if (src.length == 0) {
    test = [...des];
    test.splice(3, 0, 0);
    playBaseball(inning, [...test], N, 0);
    return;
  }

  for (let i = 0; i < src.length; ++i) {
    let temp = src.splice(i, 1);
    des.push(...temp);
    permutation(src, des);
    des.pop();
    src.splice(i, 0, ...temp);
  }
}

function playBaseball(inning, arr, N, cnt) {
  let i = 0;
  for (let inn = 0; inn < N; ++inn) {
    let play = [0, 0, 0, 0];
    for (let out = 0; out < 3; ) {
      if (inning[inn][arr[i]] == 0) ++out;
      else if (inning[inn][arr[i]] == 1) {
        for (let j = 3; j >= 1; --j) play[j] = play[j - 1];
        play[0] = 1;
        if (play[3] == 1) {
          play[3] = 0;
          ++cnt;
        }
      } else if (inning[inn][arr[i]] == 2) {
        play[3] += play[2];
        play[3] += play[1];
        cnt += play[3];
        play[3] = 0;
        play[2] = 0;
        play[1] = 1;
        play[0] = 0;
      } else if (inning[inn][arr[i]] == 3) {
        play[3] += play[2];
        play[3] += play[1];
        play[3] += play[0];
        cnt += play[3];
        play[3] = 0;
        play[2] = 1;
        play[1] = 0;
        play[0] = 0;
      } else if (inning[inn][arr[i]] == 4) {
        for (let i = 0; i < 4; ++i) {
          cnt += play[i];
          play[i] = 0;
        }
        ++cnt;
      }

      ++i;
      i %= 9;
    }
  }

  ans = Math.max(ans, cnt);
}
