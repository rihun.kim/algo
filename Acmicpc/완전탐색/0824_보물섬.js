// BFS 의 특징을 보여주는 문제이다.
// DFS 는 끝까지 갈 수 있다는 장점은 있으나
// 모든 거리의 최단 경로를 알아내는 것은 어렵다.
// 따라서 이러한 경로가 여러방향으로 갈 수 있는 상황에서의
// 최단 거리를 구해야하는 문제는 BFS 을 이용해야한다.
// BFS 는 각 경로마다, 최단 거리를 구해낼 수 있기 떄문에
// 꼬불꼬불 길도 최단 경로로 계산이 가능하다
// 반면 DFS 는 꼬불꼬불 길을 잘못 들어가면, 빙둘러서 갔다올 수 있기 때문에
// 최단 경로 보장이 어렵다.
// BFS 는 모든 위치를 도달하면 바로 픽스하기 떄문에
// 최단경로만 계속 확보된다.

// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "5 7\nWLLWWWL\nLLLWLLL\nLWLWLWW\nLWLWLLL\nWLLWLWW\n";
input = input.toString().trim().split("\n");

const [Y, X] = input.shift().split(" ");

let matrix = [];
for (let i = 0; i < input.length; ++i) matrix.push(input[i].split(""));

let ans = 0;
for (let x = 0; x < X; ++x) {
  for (let y = 0; y < Y; ++y) {
    if (matrix[y][x] == "L") {
      ans = Math.max(ans, bfs(y, x));
    }
  }
}

console.log(ans);

function borderCheck(y, x) {
  if (0 <= y && y < Y && 0 <= x && x < X) return true;
  return false;
}

function bfs(y, x) {
  let queue = [[y, x]];
  let dy = [0, 0, 1, -1];
  let dx = [1, -1, 0, 0];

  let visited = Array.from({ length: Y }, () => Array.from({ length: X }, () => -1));
  visited[y][x] = 0;
  let ans = 0;

  while (queue.length != 0) {
    let [yy, xx] = queue.shift();
    for (let i = 0; i < 4; ++i) {
      const ny = yy + dy[i];
      const nx = xx + dx[i];

      if (borderCheck(ny, nx) && visited[ny][nx] == -1 && matrix[ny][nx] == "L") {
        visited[ny][nx] = visited[yy][xx] + 1;
        queue.push([ny, nx]);
        ans = Math.max(ans, visited[ny][nx]);
      }
    }
  }

  return ans;
}
