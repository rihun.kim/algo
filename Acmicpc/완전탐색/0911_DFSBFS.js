// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "1000 1 1\n999 1000\n";
input = input.toString().trim().split("\n");

const [M, N, S] = input
  .shift()
  .split(" ")
  .map((v) => +v);

let map = new Map();
for (let i = 0; i <= M; ++i) map[i] = [];

for (let i = 0; i < input.length; ++i) {
  const [A, B] = input[i].split(" ").map((v) => +v);

  map[A].push(B);
  map[B].push(A);
}

for (let key in map) map[key].sort((a, b) => a - b);

let checked = Array.from({ length: M + 1 }, () => false);
let ans = [S];
checked[S] = true;
dfs(S, checked, ans);

checked = Array.from({ length: M + 1 }, () => false);
checked[S] = true;
let ans2 = [S];
bfs(S, checked, ans2);

console.log(ans.join(" "));
console.log(ans2.join(" "));

function dfs(start, checked, ans) {
  for (let node of map[start]) {
    if (checked[node] == false) {
      checked[node] = true;
      ans.push(node);
      dfs(node, checked, ans);
    }
  }
}

function bfs(start, checked, ans2) {
  let queue = map[start];
  while (queue.length != 0) {
    let node = queue.shift();

    if (checked[node] == false) {
      checked[node] = true;
      ans2.push(node);
      for (let nextNode of map[node]) {
        queue.push(nextNode);
      }
    }
  }
}
