let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");

// let input = "3 5\n42101\n22100\n22101\n";
input = input.toString().trim().split("\n");
const [N, M] = input.shift().split(" ");

function solution(N, M, input) {
  let matrix = [];
  for (let i = 0; i < N; ++i) matrix.push(input[i].split("").map((v) => +v));

  let min = Math.min(N, M);

  for (let side = min; 1 <= side; --side) {
    for (let y = 0; y <= N - side; ++y) {
      for (let x = 0; x <= M - side; ++x) {
        let leftUpPnt = matrix[y][x];
        let rightUpPnt = matrix[y][x + side - 1];
        let leftDownPnt = matrix[y + side - 1][x];
        let rigthDownPnt = matrix[y + side - 1][x + side - 1];

        if (leftUpPnt == rightUpPnt && leftUpPnt == leftDownPnt && leftUpPnt == rigthDownPnt) {
          return side * side;
        }
      }
    }
  }
}

console.log(solution(N, M, input));
