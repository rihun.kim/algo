let fs = require("fs");
let input = fs.readFileSync("/dev/stdin");
input = input.toString().trim().split("\n");

const [M, N, S] = input
  .shift()
  .split(" ")
  .map((v) => +v);

let map = new Map();
for (let i = 1; i <= M; ++i) map[i] = [];

for (let i = 0; i < input.length; ++i) {
  const [A, B] = input[i].split(" ").map((v) => +v);

  map[A].push(B);
  map[B].push(A);
}
for (let key in map) map[key].sort((a, b) => a - b);

let checked = Array.from({ length: M + 1 }, () => false);
let ans = [S];
dfs(S, checked, ans);

console.log(ans.join(" "));

function dfs(start, checked, ans) {
  checked[start] = true;
  for (let i = 0; i < map[start].length; ++i) {
    if (!checked[map[start][i]]) {
      checked[map[start][i]] = true;
      ans.push(map[start][i]);
      dfs(map[start][i], checked, ans);
    }
  }
}

checked = Array.from({ length: M + 1 }, () => false);
ans = [];
bfs(S, checked, ans);
console.log(ans.join(" "));

function bfs(start, checked, ans) {
  let queue = [start];
  while (queue.length != 0) {
    let num = queue.shift();

    if (!checked[num]) {
      checked[num] = true;
      ans.push(num);

      for (let i = 0; i < map[num].length; ++i) {
        queue.push(map[num][i]);
      }
    }
  }
}
