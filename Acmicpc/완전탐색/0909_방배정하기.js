// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "3 6 9 112";

input = input
  .toString()
  .trim()
  .split(" ")
  .map((v) => +v);

console.log(solve());

function solve() {
  for (let a = 0; input[3] >= a * input[0]; ++a) {
    for (let b = 0; input[3] >= a * input[0] + b * input[1]; ++b) {
      if ((input[3] - (a * input[0] + b * input[1])) % input[2] == 0) {
        return 1;
      }
    }
  }
  return 0;
}
