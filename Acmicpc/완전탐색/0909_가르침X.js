// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");
// 시간초과...
let input =
  "50 15\nantarctica\nantahellotica\nantacafsdrtica\nantarngjocbtica\nantarnfzojctica\nantarnaffuctica\nantarnaapoctica\nantarnapgoctica\nantardsfjoctica\nantanapgoctica\nantaafsdrctica\nantarjhgctica\nantarfjhctica\nantarfbncttica\nantarrtctica\nantarbetrectica\nantarqreufctica\nantarrectica\nantarqfjectica\nantarafjtica\nantarqvawectica\nantarqofwyctica\nantarvnfjctica\nantarqovaeutica\nantareufjectica\nantarqefjtica\nantarqefjectica\nantaructica\nantarqafdoctica\nantarqictica\nantarqerjictica\nantarqoeictica\nantarqeeictica\nantarbroctica\nantarbuyuoctica\nantarbvuroctica\nantarbiuyyctica\nantarbdsfhctica\nantarbquroctica\nantarinfjctica\nantarirjectica\nantariqwctica\nantariwyectica\nantarigsdvctica\nantarijectica\nantarqegafdtica\nantarqctica\nantarqgoeictica\nantarqtqtoetica\nantarqnuyectica\n";

input = input.toString().trim().split("\n");
let [N, K] = input.shift().split(" ");
K -= 5;

let source = [];
for (let ch = 97; ch <= 122; ++ch) source.push(ch);

let ans = 0;
combi(source, [], 26, K, 0);

console.log(ans);

function combi(source, answer, n, r, index) {
  if (r == 0) {
    let cnt = 0;
    for (let word of input) {
      word = word.substring(4, word.length - 4);
      if (word == "") {
        ++cnt;
        continue;
      } else {
        let flag = true;
        for (let ch of word) {
          if (!answer.includes(ch.charCodeAt())) {
            flag = false;
            break;
          }
        }
        if (flag) ++cnt;
      }
    }

    ans = Math.max(ans, cnt);
  } else if (n == 0 || n < r) return;
  else {
    answer.push(source[index]);
    combi(source, [...answer], n - 1, r - 1, index + 1);
    answer.pop();
    combi(source, [...answer], n - 1, r, index + 1);
  }
}
