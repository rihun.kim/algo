// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "3\n3 7\n15 7\n5 2\n";
input = input.toString().trim().split("\n");

input.shift();
let matrix = Array.from({ length: 100 }, () => Array.from({ length: 100 }, () => 0));

for (let i = 0; i < input.length; ++i) {
  const [Y, X] = input[i].split(" ").map((v) => +v - 1);

  for (let y = Y; y < Y + 10; ++y) {
    if (y >= 100) break;
    for (let x = X; x < X + 10; ++x) {
      if (x >= 100) break;
      matrix[y][x] = 1;
    }
  }
}

let cnt = 0;
for (let y = 0; y < 100; ++y) {
  for (let x = 0; x < 100; ++x) {
    if (matrix[y][x] == 1) ++cnt;
  }
}

console.log(cnt);
