// 핵심은 센스
// 두개의 좌표를 지속적으로 저장하며, 변경시킬 수 있어야함
// 이 경우, {} 을 이용한 객체가 편함
// {R: [rX, rY], B: [bX, bY]} 이런식으로 좌표를 관리
// Q 에 넣을땐, {R:[rX, rY], B:[bX, bY]} 이런식으로 넣고
// 빼서 쓸 땐, const {R, B} = q.shift() 을 해서 쓰거나 for 문을 써도 됨
// 이런 뒤, 그 이후론 R, B 이런식으로 접근해서 사용 가능
// 두개의 공이 양 옆에 있을 경우, 움직일떄 문제가 될 수 도 있다
// 따라서, 움직일때, count 을 세서, 어떤게 앞에 있는지 체크할 수 있다.
// 좋은 코드!

// // let fs = require("fs");
// // let input = fs.readFileSync("/dev/stdin");

let input = "5 5\n#####\n#..B#\n#.#.#\n#RO.#\n#####\n";
input = input.toString().trim().split("\n");

const [Y, X] = input
  .shift()
  .split(" ")
  .map((v) => +v);
const matrix = [];
for (let i = 0; i < input.length; ++i) matrix.push(input[i].split(""));

const { R, B } = (() => {
  let location = { R: [0, 0], B: [0, 0] };
  for (let y = 0; y < Y; ++y) {
    for (let x = 0; x < X; ++x) {
      if (matrix[y][x] == "R") location.R = [y, x];
      if (matrix[y][x] == "B") location.B = [y, x];
    }
  }
  return location;
})();

const BLOCKED = "#";
const HOLE = "O";

const dy = [0, 0, -1, 1];
const dx = [-1, 1, 0, 0];

const move = (loc, way) => {
  let cur = [...loc];
  let cnt = 0;

  while (true) {
    const ny = cur[0] + dy[way];
    const nx = cur[1] + dx[way];

    if (matrix[ny][nx] == BLOCKED) break;

    cur = [ny, nx];
    ++cnt;
    if (matrix[ny][nx] == HOLE) break;
  }

  return [...cur, cnt];
};

const createKey = (loc1, loc2) => `(${loc1.toString()}),(${loc2.toString()})`;
let Q = [{ R, B }];
let locationSet = new Set();
locationSet.add(createKey(R, B));
let count = 0;

while (Q.length != 0 && count <= 10) {
  let newQ = [];
  for (const { R, B } of Q) {
    if (matrix[B[0]][B[1]] == HOLE) continue;
    if (matrix[R[0]][R[1]] == HOLE) {
      console.log(count);
      return;
    }

    for (let way = 0; way < 4; ++way) {
      let nR = move(R, way);
      let nB = move(B, way);
      if (nR[0] == nB[0] && nR[1] == nB[1] && matrix[nR[0]][nR[1]] != HOLE) {
        if (nR[2] > nB[2]) {
          nR[0] -= dy[way];
          nR[1] -= dx[way];
        } else {
          nB[0] -= dy[way];
          nB[1] -= dx[way];
        }
      }

      const key = createKey(nR.slice(0, 2), nB.slice(0, 2));
      if (!locationSet.has(key)) {
        locationSet.add(key);
        newQ.push({
          R: nR.slice(0, 2),
          B: nB.slice(0, 2),
        });
      }
    }
  }
  Q = newQ;
  ++count;
}

console.log(-1);

///////////////////////////////////
// // let fs = require("fs");
// // let input = fs.readFileSync("/dev/stdin");

let input = "5 5\n#####\n#..B#\n#.#.#\n#RO.#\n#####\n";
input = input.toString().trim().split("\n");

input.shift();

let matrix = [];
for (let i = 0; i < input.length; ++i) matrix.push(input[i].split("").map((v) => v));

let redPos = [];
let bluPos = [];
let goal = [];
const L = input.length;

for (let y = 1; y < L - 1; ++y) {
  for (let x = 1; x < L - 1; ++x) {
    if (matrix[y][x] == "B") {
      bluPos = [y, x];
      matrix[y][x] = ".";
    }
    if (matrix[y][x] == "R") {
      redPos = [y, x];
      matrix[y][x] = ".";
    }
    if (matrix[y][x] == "O") goal = [y, x];
  }
}

let N = 1;
let dy = [0, 0, -1, 1]; // 왼, 오, 위, 아래
let dx = [-1, 1, 0, 0];
let Q = [[redPos, bluPos, 0]];

let ans = Infinity;
while (N <= 10) {
  if (Q.length == 0) break;
  let [rpos, bpos, cnt] = Q.shift();

  redVis[rpos[0]][rpos[1]] = true;
  bluVis[bpos[0]][bpos[1]] = true;
  ++cnt;

  let [red, redMov] = trav(rpos, redVis, "B", cnt);
  let [blu, bluMov] = trav(bpos, bluVis, "A", cnt);

  for (let i = 0; i < 4; ++i) {
    if (rpos[0] == bpos[0]) {
      if (rpos[1] < bpos[1]) {
        if (blu[i] != "NO" && blu[i] != "DONE") ++blu[i][1];
      } else {
        if (red[i] != "NO" && red[i] != "DONE") ++red[i][1];
      }
    }

    if (rpos[1] == bpos[1]) {
      if (rpos[0] < bpos[0]) {
        if (blu[i] != "NO" && blu[i] != "DONE") ++blu[i][0];
      } else {
        if (red[i] != "NO" && red[i] != "DONE") ++red[i][0];
      }
    }

    let temp = [];
    if (blu[i] == "NO") {
    } else if (blu[i] == "DONE") {
    } else {
    }

    if (red[i] == "NO") {
    } else if (red[i] == "DONE") {
    } else {
    }
  }

  ++N;
}

console.log(-1);

function trav(rpos, visited, other) {
  let move = [];
  let ry, rx;
  let rQ = [];
  for (let i = 0; i < 4; ++i) {
    let moveCnt = 0;
    ry = rpos[0] + dy[i];
    rx = rpos[1] + dx[i];
    if (!visited[ry][rx] && matrix[ry][rx] != "#" && matrix[ry][rx] != other) {
      while (true) {
        if (matrix[ry][rx] == "O") {
          rQ.push("DONE");
          break;
        } else if (!visited[ry][rx] && matrix[ry][rx] != "#" && matrix[ry][rx] != "B") {
        } else {
          rQ.push([ry - dy[i], rx - dx[i]]);
          break;
        }
        visited[ry][rx] = true;
        ++moveCnt;
        ry = ry + dy[i];
        rx = rx + dx[i];
      }
    } else {
      rQ.push("NO");
    }
    move.push(moveCnt);
  }

  return [rQ, move];
}
