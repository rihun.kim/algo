// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin").toString().trim().split("\n");

let input = "7\n3 10\n5 20\n1 10\n1 20\n2 15\n4 40\n2 200\n".trim().split("\n");

let schedule = input;
let N = input[0];
let max = -1;

for (let i = 0; i < N; ++i) {
  schedule[i + 1] = schedule[i + 1].split(" ");
}

getMoney(0, 0, 0);
console.log(max);

function getMoney(day, pay, payDone) {
  if (day >= N) {
    if (day == N) {
      pay += payDone;
    }
    max = Math.max(max, pay);
    return;
  }

  pay += payDone;
  for (let i = day; i < N; ++i) {
    getMoney(+schedule[i + 1][0] + i, pay, +schedule[i + 1][1]);
  }
}
