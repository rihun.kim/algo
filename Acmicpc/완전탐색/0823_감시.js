// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

// let input = "6 6\n0 0 0 0 0 0\n0 2 0 0 0 0\n0 0 0 0 6 0\n0 6 0 0 2 0\n0 0 0 0 0 0\n0 0 0 0 0 5\n";
// let input = "4 6\n0 0 0 0 0 0\n0 0 0 0 0 0\n0 0 1 0 6 0\n0 0 0 0 0 0\n";
// let input = "6 6\n1 0 0 0 0 0\n0 1 0 0 0 0\n0 0 1 5 0 0\n0 0 5 1 0 0\n0 0 0 0 1 0\n0 0 0 0 0 1\n";

input = input.toString().trim().split("\n");

const [Y, X] = input
  .shift()
  .split(" ")
  .map((v) => +v);

let matrix = [];
let cctvs = [];
let cctvs5 = [];
for (let i = 0; i < Y; ++i) {
  let temp = input[i].split(" ").map((v) => +v);
  for (let x = 0; x < X; ++x) {
    if (temp[x] == 5) {
      cctvs5.push([i, x, temp[x]]);
    } else if (1 <= temp[x] && temp[x] < 6) {
      cctvs.push([i, x, temp[x]]);
    }
  }
  matrix.push(temp);
}

let dy = [0, 0, 1, -1];
let dx = [1, -1, 0, 0];

for (let cctv of cctvs5) {
  matrix = watch(cctv[0], cctv[1], [0, 1, 2, 3], JSON.stringify(matrix));
}

let ans = Infinity;
dfs(0, matrix, cctvs);
console.log(ans);

function bordering(y, x) {
  if (0 <= y && y < Y && 0 <= x && x < X) return true;
  return false;
}

// 0 오른쪽, 1 왼쪽, 2 위, 3 아래
function watch(y, x, ways, matrix) {
  matrix = JSON.parse(matrix);

  if (ways.includes(0)) {
    let ny = y + dy[0];
    let nx = x + dx[0];
    while (true) {
      if (bordering(ny, nx)) {
        if (matrix[ny][nx] == 6) break;
        else if (matrix[ny][nx] == 0) matrix[ny][nx] = "#";
      } else break;
      ny += dy[0];
      nx += dx[0];
    }
  }
  if (ways.includes(1)) {
    let ny = y + dy[1];
    let nx = x + dx[1];
    while (true) {
      if (bordering(ny, nx)) {
        if (matrix[ny][nx] == 6) break;
        else if (matrix[ny][nx] == 0) matrix[ny][nx] = "#";
      } else break;
      ny += dy[1];
      nx += dx[1];
    }
  }
  if (ways.includes(2)) {
    let ny = y + dy[2];
    let nx = x + dx[2];
    while (true) {
      if (bordering(ny, nx)) {
        if (matrix[ny][nx] == 6) break;
        else if (matrix[ny][nx] == 0) matrix[ny][nx] = "#";
      } else break;
      ny += dy[2];
      nx += dx[2];
    }
  }
  if (ways.includes(3)) {
    let ny = y + dy[3];
    let nx = x + dx[3];
    while (true) {
      if (bordering(ny, nx)) {
        if (matrix[ny][nx] == 6) break;
        else if (matrix[ny][nx] == 0) matrix[ny][nx] = "#";
      } else break;
      ny += dy[3];
      nx += dx[3];
    }
  }

  return matrix;
}

function zeroCnt(matrix) {
  let cnt = 0;
  for (y = 0; y < Y; ++y) {
    for (x = 0; x < X; ++x) {
      if (matrix[y][x] == 0) {
        ++cnt;
      }
    }
  }
  return cnt;
}

function dfs(index, matrix, cctvs) {
  if (index == cctvs.length) {
    ans = Math.min(zeroCnt(matrix), ans);
    return;
  }

  const [y, x, cctvNum] = cctvs[index];

  if (cctvNum == 1) {
    let tMatrix;
    tMatrix = watch(y, x, [0], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [1], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [2], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);
  } else if (cctvNum == 2) {
    tMatrix = watch(y, x, [0, 1], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [2, 3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);
  } else if (cctvNum == 3) {
    tMatrix = watch(y, x, [0, 2], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [0, 3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [1, 2], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [1, 3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);
  } else {
    tMatrix = watch(y, x, [0, 1, 2], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [1, 2, 3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [0, 1, 3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);

    tMatrix = watch(y, x, [0, 2, 3], JSON.stringify(matrix));
    dfs(index + 1, [...tMatrix], cctvs);
  }
}
