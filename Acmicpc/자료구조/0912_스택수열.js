// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "8\n4\n3\n6\n8\n7\n5\n2\n1\n";
input = input.toString().trim().split("\n");

let ans = [];
function Stack() {
  this.store = [];
}

Stack.prototype.push = function (val) {
  ans.push("+");
  this.store.push(val);
};
Stack.prototype.pop = function () {
  ans.push("-");
  return this.store.pop();
};
Stack.prototype.peek = function () {
  return this.store[this.store.length - 1];
};

input = input.map((v) => +v);
const N = input.shift();
let stack = new Stack();

for (let i = 1, inputIdx = 0; ; ) {
  if (stack.peek() == input[inputIdx]) {
    ++inputIdx;
    stack.pop();
  } else {
    stack.push(i);
    ++i;
  }

  if (inputIdx == N) break;
  if (input[inputIdx] < stack.peek()) break;
}

if (stack.peek() == undefined) console.log(ans.join("\n"));
else console.log("NO");
