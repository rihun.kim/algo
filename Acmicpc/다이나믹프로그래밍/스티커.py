tc = int(input())
for _ in range(tc):
  n = int(input())
  arr = []
  d = [] 
  for _ in range(2):
    arr.append(list(map(int, input().split())))
  
  d.append([0, arr[0][0], arr[1][0]])

  for i in range(1,len(arr[0])):
    d.append([])
    d[i].append(max(d[i-1][0], d[i-1][1], d[i-1][2]))
    d[i].append(max(d[i-1][0], d[i-1][2]) + arr[0][i])
    d[i].append(max(d[i-1][0], d[i-1][1]) + arr[1][i])

  print(max(d[n-1][0], d[n-1][1], d[n-1][2]))
  