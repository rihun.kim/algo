// let fs = require("fs");
// let input = fs.readFileSync("/dev/stdin");

let input = "11";
input = input.toString().trim().split(" ");

let d = [];
d[0] = -1;
d[1] = -1;
d[2] = -1;
d[3] = 1;
d[4] = -1;
d[5] = 1;
const N = +input[0];

for (let i = 6; i <= N; ++i) {
  if (d[i - 5] == -1) {
    if (d[i - 3] != -1) d[i] = d[i - 3] + 1;
    else d[i] = -1;
  } else {
    if (d[i - 3] != -1) {
      d[i] = Math.min(d[i - 3], d[i - 5]) + 1;
    } else {
      d[i] = d[i - 5] + 1;
    }
  }
}

console.log(d[N]);
