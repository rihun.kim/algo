num = int(input())

d = [[]]
for i in range(0, 10):
  d[0].append(1)

for i in range(1, num):
  d.append([])
  for j in range(0, 10):
    sum = 0
    for k in range(0, j+1):
      sum += d[i-1][k]
    d[i].append(sum)

ans = 0;
for i in range(0, 10):
  ans += d[num-1][i]

print(ans)


############
N = int(input())
d = [[0] * 10 for i in range(N+1)]
for i in range(10):
  d[0][i] = 1

for i in range(1, N):
  for j in range(10):
    for k in range(j+1):
      d[i][j] += d[i-1][k]


print(sum(d[N-1]) % 10007)
