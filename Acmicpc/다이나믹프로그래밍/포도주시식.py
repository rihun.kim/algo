n = int(input())
arr = [int(input()) for _ in range(n)]

d = [[0, arr[0], 0]]

for i in range(1, n):
  d.append([])
  d[i].append(max(d[i-1][0], d[i-1][1], d[i-1][2]))
  d[i].append(d[i-1][0] + arr[i])
  d[i].append(d[i-1][1] + arr[i])

print(max(d[n-1][0], d[n-1][1], d[n-1][2]))