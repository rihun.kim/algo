class PQ {
  constructor() {
    this.arr = []
    this.len = 0;
  }

  size() { return this.len }
  push(num) {
    this.arr.push(num);
    ++this.len;
  }
  pop() {
    let start = 0;
    this.arr.map((_, index) => {
      if (this.arr[start] === Infinity || this.arr[start] > this.arr[index]) start = index;
    });

    let temp = this.arr[start];
    this.arr[start] = Infinity;
    --this.len;
    return temp;
  }
}

function solution(S) {
  const pq = new PQ();

  for (let num of S) pq.push(num);

  let ans = 0;
  while (pq.size() > 1) {
    let sum = +pq.pop() + +pq.pop();
    ans += sum;
    pq.push(sum);
  }

  // console.log(ans, pq)
  return ans;
}

solution([100, 250, 1000])
