function solution(A) {
  // 01010
  let cnt1 = 0;
  for (let i = 0; i < A.length; ++i) {
    // 짝수 0, 2, 4, 6
    if (i % 2 == 0 && A[i] != 0) {
      ++cnt1;
    } else if (i % 2 != 0 && A[i] != 1) {
      ++cnt1;
    }
  }

  let cnt2 = 0;
  for (let i = 0; i < A.length; ++i) {
    // 짝수 0, 2, 4, 6
    if (i % 2 == 0 && A[i] != 1) {
      ++cnt2;
    } else if (i % 2 != 0 && A[i] != 0) {
      ++cnt2;
    }
  }

  console.log(Math.min(cnt1, cnt2));
  return Math.min(cnt1, cnt2);
}

solution([0, 1, 0, 1]);
// solution([1, 0, 1, 0, 1, 1]);
