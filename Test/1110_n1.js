// P 가 0 이거나 N-1 이면 ROW 의 합은 0 으로 가정
// Q 가 0 이거나 M-1 이면 ROW 의 합은 0 으로 가정


function solution(A) {
  let cnt = 0;

  let leftCol, rigthCol, upRow, downRow;
  for (let y = 0; y < A.length; ++y) {
    for (let x = 0; x < A[0].length; ++x) {
      leftCol = 0;
      rigthCol = 0;
      upRow = 0;
      downRow = 0;

      // leftCol
      if (x === 0) leftCol = 0;
      else {
        for (let i = 0; i < x; ++i) {
          leftCol += getColSum(i)
        }
      }

      // rigthCol
      if (x === A[0].length - 1) rigthCol = 0;
      else {
        for (let i = x + 1; i < A[0].length; ++i) {
          rigthCol += getColSum(i);
        }
      }

      // upRow
      if (y === 0) upRow = 0;
      else {
        for (let i = 0; i < y; ++i) {
          upRow += getRowSum(i)
        }
      }

      // downRow
      if (y === A.length - 1) downRow = 0;
      else {
        for (let i = y + 1; i < A.length; ++i) {
          downRow += getRowSum(i)
        }
      }

      // console.log(leftCol, rigthCol, upRow, downRow)
      if (leftCol === rigthCol && upRow === downRow) ++cnt;
    }
  }

  // console.log(cnt)
  return cnt;


  function getRowSum(row) {
    let total = 0;
    for (let c = 0; c < A[0].length; c++) total += A[row][c];
    return total;
  }

  function getColSum(col) {
    let total = 0;
    for (let r = 0; r < A.length; r++) total += A[r][col];
    return total;
  }
}

// solution([[2, 7, 5], [3, 1, 1], [2, 1, -7], [0, 2, 1], [1, 6, 8]])
solution([[2]])