// A
// 1   2 ..... 702
// 703

// 알파벳 갯수 26개

// 숫자가 입력되면,
// 702 로 나눔
// 나누면 몫은 행을 의미 
// 나머지는 27 으로 나눔

// 몫은 27 부터 A
// 몫이 1이면 A, 2면 B, 2
// 27 이상이면,
// 나머지가 알파벳 순
// String.fromCharCode([아스키코드값]);
// 65 : A  ==> 나머지에 65 더해서 아스키 값 만들기

function solution(n) {
  // 행 구하기
  let rquo = parseInt(n / 702) + 1;

  let value = n % 702;

  let cquo = parseInt(value / 26);
  if (cquo == 0) {
    let cremain = value % 26;
    let col = String.fromCharCode(cremain + 64);

    console.log(rquo + col)
  } else {
    let head = String.fromCharCode(cquo + 64);
    let cremain = value % 26;
    let col = String.fromCharCode(cremain + 65);
    console.log(rquo + head + col)
  }
}

solution(27);
// 1AA

// solution(703);
// 2B