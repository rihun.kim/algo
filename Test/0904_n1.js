function solution(A) {
  let sortedA = JSON.stringify(A);
  sortedA = JSON.parse(sortedA).sort((a, b) => a - b);

  let cnt = 0;
  for (let i = 0; i < sortedA.length; ) {
    let oriPosA = A.indexOf(sortedA[i]);

    if (oriPosA == i) {
      ++cnt;
      ++i;
    } else {
      ++cnt;
      i = oriPosA + 1;
    }
  }

  // console.log(cnt);
  return cnt;
}

// B = 1,2,3,4,6,7
// A = 2,1,6,4,3,7

solution([2, 4, 1, 6, 5, 9, 7]);
solution([4, 3, 2, 6, 1]);
solution([2, 1, 6, 4, 3, 7]);
