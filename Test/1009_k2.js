function segment(x, space) {
  let min = getMini(space, 0, x);
  let max = min;

  function getMini(arr, start, end) {
    let min = arr[start];
    for (let i = start; i < end; ++i) min = min > arr[i] ? arr[i] : min;
    return min;
  }

  for (let i = 1; i < space.length; ++i) {
    if (i + x > space.length) break;

    if (space[i - 1] == min) min = getMini(space, i, i + x);
    else min = min > space[i] ? space[i] : min;

    max = max < min ? min : max;
  }

  return max;
}
