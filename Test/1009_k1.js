function requestsServed(timestamp, top) {
  let stack = [];
  let cnt = 0;

  timestamp.sort((a, b) => a - b);
  top.sort((a, b) => a - b);

  for (let i = 0; i < top.length; ++i) {
    while (timestamp.length > 0 && timestamp[0] <= top[i]) {
      stack.push(timestamp.shift());
    }

    for (let i = 0; i < 5; ++i) {
      if (stack.length > 0) {
        stack.pop();
        ++cnt;
      }
    }
  }

  return cnt;
}
