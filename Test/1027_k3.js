function solution(arr) {
  let matrix = [];


  for (let y = 0; y < arr.length; ++y) {
    matrix.push(arr[y].split(""));
  }
  const X = matrix[0].length;
  const Y = matrix.length;

  let ans = 0;
  while (!isFinish(matrix)) {
    let dy = [0, 0, 1, -1];
    let dx = [1, -1, 0, 0];

    let start = getTarget(matrix);
    let queue = [start]
    while (queue.length !== 0) {
      let cell = queue.shift();

      matrix[cell[1]][cell[2]] = 0;
      for (let i = 0; i < 4; ++i) {
        let ny = cell[1] + dy[i];
        let nx = cell[2] + dx[i];


        if (borderCheck(ny, nx) && matrix[ny][nx] === cell[0]) {
          queue.push([cell[0], ny, nx]);
          matrix[ny][nx] = 0;
        }
      }
    }

    ++ans;
  }

  // console.log(ans)
  return ans;


  function borderCheck(y, x) {
    if (0 <= y && y < Y && 0 <= x && x < X) return true;
    return false;
  }


  function getTarget(matrix) {
    for (let y = 0; y < matrix.length; ++y) {
      for (let x = 0; x < matrix[0].length; ++x) {
        if (matrix[y][x] !== 0) {
          return [matrix[y][x], y, x];
        }
      }
    }
  }


  function isFinish(matrix) {
    for (let y = 0; y < matrix.length; ++y) {
      for (let x = 0; x < matrix[0].length; ++x) {
        if (matrix[y][x] !== 0) return false;
      }
    }

    return true;
  }
}

solution(["bbba", "abba", "acaa", "aaac"]);