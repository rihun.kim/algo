function solution(N) {
  let nums = N.toString(2).split("");

  let maxlen = 0;
  let cnt = 0;
  for (let num of nums) {
    if (num == "1") {
      maxlen = Math.max(cnt, maxlen);
      cnt = 0;
    } else {
      ++cnt;
    }
  }

  // console.log(nums, maxlen);
  return maxlen;
}

solution(1041);
