function solution(A, K) {
  if (A.length == K) {
    return A;
  }

  while (K > 0) {
    K--;
    A.unshift(A.pop());
  }

  console.log(A);
  return A;
}

solution([3, 8, 9, 7, 6], 3);

function solution(A) {
  let map = new Map();

  for (let a of A) {
    if (map[a] == undefined) {
      map[a] = 1;
    } else {
      if (map[a] == 1) map[a] = 0;
      else map[a] = 1;
    }
  }

  for (let key in map) {
    if (map[key] == 1) return key;
  }
}

solution([9, 3, 9, 3, 9, 7, 9]);
