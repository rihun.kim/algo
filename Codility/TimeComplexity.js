function solution(A) {
  let arr = Array.from({ length: A.length + 2 }, () => false);

  for (let a of A) arr[a] = true;

  for (let i = 1; i < arr.length; ++i) {
    if (arr[i] == false) return i;
  }
}

function solution(A) {
  let left = A[0];
  let right = 0;
  for (let i = 1; i < A.length; ++i) right += A[i];

  let min = Math.abs(left - right);

  for (let i = 1; i < A.length - 1; ++i) {
    left += A[i];

    right -= A[i];

    min = Math.min(min, Math.abs(left - right));
  }

  return min;
}
