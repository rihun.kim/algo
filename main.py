# 뒤집어 푸는 센스를 보여준다.
# 뒤집으면, 순차적으로 찾아나아가기 쉬워진다.
# 그 상태에서, 감소하는 수열을 찾는 것임에도 불구하고
# 증가하는 것을 찾아간다
# 그 이유는 증가하는 수열을 찾아서 다시 원래대로 뒤집으면 감소하는 수열이 되기 떄문이다.

n = int(input())
lst = list(map(int, input().split()))

lst.reverse()
dp = [1 for i in range(len(lst))]

for i in range(1, len(lst)):
  for j in range(0, i):
    if lst[j] < lst[i]:
      dp[i] = max(dp[i], dp[j]+1)
print(max(dp))

