//// FIBO
function fibo(val) {
  if (val == 1 || val == 0) return 1;
  else return fibo(val - 1) + fibo(val - 2);
}

let memo = [];
function fibo(num) {
  if (num == 0 || num == 1) return 1;
  if (memo[num] != undefined) return memo[num];

  const val1 = (memo[num - 1] = fibo(num - 1, memo));
  const val2 = (memo[num - 2] = fibo(num - 2, memo));
  return val1 + val2;
}

//// FACTO
function fac(num) {
  if (num == 1) return 1;
  else return num * fac(num - 1);
}

//// BS
function binarySearch(arr, val) {
  let startIdx = 0;
  let endIdx = arr.length - 1;

  while (startIdx <= endIdx) {
    let mid = Math.floor((startIdx + endIdx) / 2);
    if (arr[mid] == val) return mid;

    if (arr[mid] < val) startIdx = mid + 1;
    else endIdx = mid - 1;
  }

  return -1;
}

//// PERMU
permutation([1, 2, 3], []);
function permutation(src, des) {
  if (src.length == 0) console.log(des);

  for (let i = 0; i < src.length; ++i) {
    let temp = src.splice(i, 1);
    des.push(...temp);
    permutation(src, des);
    src.splice(i, 0, ...temp);
    des.pop();
  }
}

//// COMBI
combination([1, 2, 3], [], 3, 2, 0);
function combination(src, des, n, r, idx) {
  if (r == 0) console.log(des);
  else if (n == 0 || n < r) return;
  else {
    des.push(src[idx]);
    combination(src, [...des], n - 1, r - 1, idx + 1);
    des.pop();
    combination(src, [...des], n - 1, r, idx + 1);
  }
}

//// DFS
function dfs(start, checked, ans) {
  for (let node of map[start]) {
    if (checked[node] == false) {
      checked[node] = true;
      ans.push(node);
      dfs(node, checked, ans);
    }
  }
}

//// BFS
function bfs(start, checked, ans) {
  let queue = map[start];
  while (queue.length != 0) {
    let node = queue.shift();
    if (checked[node] == false) {
      checked[node] = true;
      for (let nextNode of map[node]) queue.push(nextNode);
    }
  }
}

//// GCD
function gcd(a, b) {
  return a % b ? gcd(b, a % b) : b;
}

//// LCM
function lcm(a, b) {
  return (a * b) / gcd(a, b);
}
