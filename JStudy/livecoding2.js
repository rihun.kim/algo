function bubbleSort(arr) {
  let swap;
  for (let i = 0; i < arr.length; ++i) {
    for (let j = 0; j < arr.length - i - i; ++j) {
      if (arr[j] > arr[j + 1]) {
        swap = arr[j];
        arr[j] = arr[j + 1];
        arr[j + 1] = swap;
      }
    }
  }
  return arr;
}

function mergeSort(arr) {
  if (arr.length < 2) return arr;

  let piv = Math.floor(arr.length / 2);
  let left = mergeSort(arr.slice(0, piv));
  let right = mergeSort(arr.slice(piv, arr.length));

  let res = [];
  let leftIdx = 0;
  let rightIdx = 0;
  while (leftIdx < left.length && rightIdx < right.length) {
    if (left[leftIdx] <= right[rightIdx]) res.push(left[leftIdx++]);
    else res.push(right[rightIdx++]);
  }

  while (leftIdx < left.length) res.push(left[leftIdx++]);
  while (rightIdx < right.length) res.push(right[rightIdx++]);

  return res;
}

////
function closure(name) {
  let _name = name;
  return function innerFunction() {
    return _name;
  };
}

let func = closure("rihun");
let func2 = closure("minkyung");
console.log(func2());

//// hi, bye 다음에 1 이 찍힘
console.log("hi");
let func = new Promise(function (resolve, reject) {
  setTimeout(function () {
    resolve(1);
  }, 2000);
}).then((prop) => console.log(prop));
console.log("bye");


//// 에라토스테네스체
let checked = Array.from({ length: arr.length }, () => false);
let ans = [];
for (let i = 2; i < arr.length; ++i) {
  if (!checked[i]) {
    for (let j = i * i; j < arr.length; j += i) {
      checked[j] = true;
    }
  }
}

console.log(checked);
