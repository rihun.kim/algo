// 순열은 쉽다
// 순열의 핵심은 각 자릿수의 숫자를 결정하는 것이다.
// 각 자리에 올 경우의 수를 생각하면 쉽다.
// 가령 1,2,3,4 일떄
// 처음에 올 숫자 는 총 4 가지 경우가 있다.
// 배열을 두 개 준비한다.
// 하나는 계속해서 쓸 재료
// 또 하나는 답을 만들어 가는 요리
// 재료를 통해서, 요리를 계속 만들어간다고 보면 쉽다
// 따라서, 재료를 하나씩 쓸 때마다, 요리도 하나씩 는다.

const permu = (arr, ans) => {
  if (arr.length == 0) {
    console.log(ans);
  }

  // 느린버전
  // for (let i = 0; i < arr.length; ++i) {
  //   let tempArr = [...arr];
  //   let tempAns = [...ans];
  //   tempAns.push(...tempArr.splice(i, 1));
  //   permu(tempArr, tempAns);
  // }

  // 속도개선버전
  // for (let i = 0; i < arr.length; ++i) {
  //   let temp = arr.splice(i, 1);   // 삭제
  //   ans.push(...temp);
  //   permu(arr, ans);
  //   arr.splice(i, 0, ...temp);     // 삽입
  //   ans.pop();
  // }
};

// let x = permu([1, 2, 3], []);
// console.log(x);

// 조합
// 조합은 nCr = n-1Cr-1 + n-1Cr 로 계산하면 쉽다.
// const combi = (source, answer, n, r, index) => {
//   if (r == 0) console.log(answer);
//   else if (n == 0 || n < r) return;
//   else {
//     answer.push(source[index]);
//     combi(source, [...answer], n - 1, r - 1, index + 1);
//     answer.pop();
//     combi(source, [...answer], n - 1, r, index + 1);
//   }
// };

combi([1, 2, 3, 4], [], 4, 3, 0);
