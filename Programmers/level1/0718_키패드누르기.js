// 단순 구현문제
function solution(numbers, hand) {
  let lpo = [[0, 1], [3, 0], [3, 1], [], [2, 0], [2, 1], [], [1, 0], [1, 1], []];
  let rpo = [[0, 1], [], [3, 1], [3, 0], [], [2, 1], [2, 0], [], [1, 1], [1, 0]];

  let ls = [0, 0];
  let rs = [0, 0];
  return numbers
    .map((v) => {
      if (v == 1 || v == 4 || v == 7) {
        ls = lpo[v];
        return "L";
      } else if (v == 3 || v == 6 || v == 9) {
        rs = rpo[v];
        return "R";
      } else {
        let l = Math.abs(lpo[v][0] - ls[0]) + Math.abs(lpo[v][1] - ls[1]);
        let r = Math.abs(rpo[v][0] - rs[0]) + Math.abs(rpo[v][1] - rs[1]);
        if (l < r) {
          ls = lpo[v];
          return "L";
        } else if (l > r) {
          rs = rpo[v];
          return "R";
        } else {
          if (hand == "left") {
            ls = lpo[v];
            return "L";
          } else {
            rs = rpo[v];
            return "R";
          }
        }
      }
    })
    .join("");
}
