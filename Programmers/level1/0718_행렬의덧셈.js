function solution(arr1, arr2) {
  for (let r = 0; r < arr1.length; ++r) {
    for (let c = 0; c < arr1[r].length; ++c) {
      arr1[r][c] += arr2[r][c];
    }
  }

  return arr1;
}

// 압축형
function solution(arr1, arr2) {
  return arr1.map((arr, i) => arr.map((ele, j) => ele + arr2[i][j]));
}
