function solution(answers) {
  let stu = [
    [1, 2, 3, 4, 5],
    [2, 1, 2, 3, 2, 4, 2, 5],
    [3, 3, 1, 1, 2, 2, 4, 4, 5, 5],
  ];
  let ans = [
    [1, 0],
    [2, 0],
    [3, 0],
  ];

  for (let i = 0; i < answers.length; ++i) {
    if (stu[0][i % 5] == answers[i]) ++ans[0][1];
    if (stu[1][i % 8] == answers[i]) ++ans[1][1];
    if (stu[2][i % 10] == answers[i]) ++ans[2][1];
  }

  let max = Math.max(ans[0][1], ans[1][1], ans[2][1]);
  return ans.filter((v) => v[1] == max).map((v) => v[0]);
}

function solution(answers) {
  let answer = [];
  let a1 = [1, 2, 3, 4, 5];
  let a2 = [2, 1, 2, 3, 2, 4, 2, 5];
  let a3 = [3, 3, 1, 1, 2, 2, 4, 4, 5, 5];

  let a1c = answers.filter((a, i) => a == a1[i % a1.length]).length;
  let a2c = answers.filter((a, i) => a == a2[i % a2.length]).length;
  let a3c = answers.filter((a, i) => a == a3[i % a3.length]).length;

  let max = Math.max(a1c, a2c, a3c);
  if (a1c == max) answer.push(1);
  if (a2c == max) answer.push(2);
  if (a3c == max) answer.push(3);

  return answer;
}
