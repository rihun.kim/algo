function solution(board, moves) {
  let stack = [];
  let ans = 0;
  for (let col of moves) {
    --col;
    for (let row = 0; row < board.length; ++row) {
      if (board[row][col] != 0) {
        if (stack[stack.length - 1] == board[row][col]) {
          stack.pop();
          ans += 2;
        } else stack.push(board[row][col]);

        board[row][col] = 0;
        break;
      }
    }
  }
  return ans;
}
