function solution(dartResult) {
  let d = dartResult;
  let stk = [];

  for (let i = 0; i < d.length; ) {
    if (isNaN(+d[i])) {
      if (d[i] == "*") {
        stk[stk.length - 1] = stk[stk.length - 1] * 2;
        stk[stk.length - 2] = stk[stk.length - 2] * 2;
      } else stk[stk.length - 1] = stk[stk.length - 1] * -1;
      ++i;
    } else {
      let num = 0;
      let idx = 1;

      if (d[i + 1] == "0") {
        num = 10;
        idx = 2;
      } else num = +d[i];

      if (d[i + idx] == "S") stk.push(num);
      else if (d[i + idx] == "D") stk.push(Math.pow(num, 2));
      else if (d[i + idx] == "T") stk.push(Math.pow(num, 3));

      idx == 1 ? (i += 2) : (i += 3);
    }
  }

  return stk.reduce((p, c) => p + c);
}
