function solution(s, n) {
  let str = "";
  for (let i = 0; i < s.length; ++i) {
    let ch = s[i].charCodeAt();
    if (s[i] == " ") {
      str += " ";
    } else if (96 < ch) {
      // 소문자
      ch = ch + n > 122 ? 96 + ((ch + n) % 122) : ch + n;
      str += String.fromCharCode(ch);
    } else {
      // 대문자
      ch = ch + n > 90 ? 64 + ((ch + n) % 90) : ch + n;
      str += String.fromCharCode(ch);
    }
  }

  return str;
}
