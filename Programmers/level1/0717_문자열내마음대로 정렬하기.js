// js sort 함수는
// 첫 번째 인수가 두 번째 인수보다 작을 경우 - 값
// 두 인수가 같을 경우 0
// 첫 번째 인수가 두 번째 인수보다 클 경우 + 값

// js 는 문자열 끼리 비교 가능하고, true or false
// 문자끼리는 아스키코드 값으로 비교 해야한다.
function solution(strings, n) {
  strings.sort((a, b) => {
    if (a[n] != b[n]) return a[n].charCodeAt() - b[n].charCodeAt();
    return -1 * (a < b ? 1 : -1);
  });

  return strings;
}
