function solution(arr) {
  let answer = [];
  let temp = "A";
  for (let ele of arr) {
    if (ele !== temp) {
      answer.push(ele);
      temp = ele;
    }
  }

  return answer;
}

function solution(arr) {
  return arr.filter((val, index) => val != arr[index + 1]);
  // 충격적인 코드 스킬
  // 이런 생각은 배워야곘다.
}
