// 아이디어
// 먼저 sort 을 해야될지 생각해보자!

function solution(participant, completion) {
  let map = new Map();
  for (let par of participant) {
    if (map.get(par) === undefined) {
      map.set(par, 1);
    } else {
      map.set(par, map.get(par) + 1);
    }
  }

  for (let com of completion) {
    if (map.get(com) - 1 == 0) {
      map.delete(com);
    } else {
      map.set(com, map.get(com) - 1);
    }
  }

  for (let [key, val] of map) {
    return key;
  }
}

function solution(participant, completion) {
  participant.sort();
  completion.sort();

  for (let i in participant) {
    if (participant[i] != completion[i]) return participant[i];
  }
}
