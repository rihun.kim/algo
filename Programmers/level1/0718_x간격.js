function solution(x, n) {
  let arr = [];
  for (let i = 1; i <= n; ++i) arr.push(x * i);
  return arr;
}

// 압축형
// 배열 크기를 n 이라 해서 만들고
// 바로 map 을 돌려서 세팅함
// 굿굿
function solution(x, n) {
  return Array(n)
    .fill(x)
    .map((v, i) => v * (i + 1));
}
