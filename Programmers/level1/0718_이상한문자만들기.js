function solution(s) {
  return s
    .split(" ")
    .map((word) => {
      let even = true;
      let str = "";
      for (let w of word) {
        str += even ? w.toUpperCase() : w.toLowerCase();
        even = !even;
      }
      return str;
    })
    .join(" ");
}
