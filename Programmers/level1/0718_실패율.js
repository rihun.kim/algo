function solution(N, stages) {
  let arr = Array.from({ length: N + 2 }, () => 0);
  let sum = stages.length;
  for (let ele of stages) ++arr[ele];
  for (let i = 1; i <= N; ++i) {
    if (arr[i] == 0) {
      arr[i] = [i, 0];
      continue;
    }
    let tmp = arr[i] / sum;
    sum -= arr[i];
    arr[i] = [i, tmp];
  }

  arr.splice(0, 1);
  arr.splice(arr.length - 1, 1);
  return arr
    .sort((a, b) => {
      if (b[1] == a[1]) {
        return a[0] - b[0];
      } else {
        return b[1] - a[1];
      }
    })
    .map((v) => v[0]);
}

function solution(N, stages) {
  let arr = Array.from({ length: N + 2 }, () => 0);
  let sum = stages.length;

  for (let ele of stages) ++arr[ele];
  for (let i = 1; i <= N; ++i) {
    if (arr[i] == 0) {
      arr[i] = { index: i, players: 0 }; // 주요 변화 방식, 배열의 인덱스를 탈피
      continue;
    }
    let tmp = arr[i] / sum;
    sum -= arr[i];
    arr[i] = { index: i, players: tmp };
  }

  arr.splice(0, 1);
  arr.splice(arr.length - 1, 1);
  return arr
    .sort((a, b) => {
      if (b.players == a.players) {
        return a.index - b.index;
      } else return b.players - a.players;
    })
    .map((v) => v.index);
}
