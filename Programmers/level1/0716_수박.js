function solution(n) {
  // if(n == 0) return str;
  // return f % 2 == 0 ? solution(n-1,str+"박",f+1) : solution(n-1,str+"수",f+1);
  let str = "";
  let exit = parseInt(n / 2);
  for (let i = 1; i <= exit; ++i) str += "수박";

  return n % 2 == 0 ? str : str + "수";
}

function solution(n) {
  return "수박".repeat(n / 2) + (n % 2 ? "수" : "");
}
