function solution(a, b) {
  return ((Math.max(a, b) - Math.min(a, b) + 1) * (a + b)) / 2;
}
