function solution(n) {
  n = "" + n;
  return n
    .split("")
    .reverse()
    .map((v) => +v);
}
