function solution(phone_number) {
  let len = phone_number.length;
  return Array.from({ length: len - 4 }, () => "*").join("") + phone_number.slice(len - 4, len);
}

// slice 에 -4 와 같은 음값 적용 가능!
// 문자열에 repeat 사용가능!
function solution(phone_number) {
  return "*".repeat(phone_number.length - 4) + phone_number.slice(-4);
}
