// 에라토스테네스체로 풀어야함
// 2부터 시작시켜서 2의 배수는 모두 지움

function solution(n) {
  let arr = [];
  for (let i = 2; i <= n; ++i) arr[i] = i;
  // 포인트는 배열의 인덱스와 배열의 값이 같다는 것!

  for (let i = 2; i <= n; ++i) {
    if (arr[i] == 0) continue;
    for (let j = i + i; j <= n; j += i) arr[j] = 0;
  }

  let sum = 0;
  for (let i = 2; i < arr.length; ++i) {
    if (arr[i] != 0) ++sum;
  }

  return sum;
}
