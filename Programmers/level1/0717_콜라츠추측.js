function solution(num, cnt = 0) {
  if (num == 1) return cnt;
  if (cnt == 500) return -1;

  if (num % 2 == 0) return solution(num / 2, cnt + 1);
  else return solution(num * 3 + 1, cnt + 1);
}

function solution(num, cnt = 0) {
  return num == 1 ? cnt : cnt == 500 ? -1 : solution(num % 2 == 0 ? num / 2 : num * 3 + 1, ++cnt);
}
