// 아이디어
// 그리디 문제이다.
// 문제를 작은 단위로 쪼개고 반복적으로 진행하며 접근하는 방식은 완전탐색 등과 유사
// But, 그리디는 각 단계에서 그 시점에 가장 좋아 보이는 선택을 한다.
// 앞으로의 선택 혹은 최종 결과는 고려하지 않는다.
// 탐욕법은 당장 앞에 보이는 가장 좋아보이는 선택을 하는 특징이 있는데,
// 이는 반드시 최적의 해임을 보장할 수는 없지만, 간단한 알고리즘문제의 경우 얼추 괜찮은 결과를 보여주기도 하는 풀이방법입니다.

function solution(n, lost, reserve) {
  let arr = Array.from({ length: n }, () => 1);

  for (let l of lost) --arr[l - 1];
  for (let r = 0; r < reserve.length; ++r) {
    if (arr[reserve[r] - 1] == 0) {
      arr[reserve[r] - 1] = 1;
      reserve[r] = false;
    }
  }
  for (let r of reserve) {
    if (!r) continue;
    if (r - 2 >= 0 && arr[r - 2] == 0) arr[r - 2] = 1;
    else if (r < n && arr[r] == 0) arr[r] = 1;
  }

  return arr.reduce((p, c) => p + c);
}

//forEach 을 쓰면 좋음

function solution(n, lost, reserve) {
  let arr = Array.from({ length: n }, () => 1);

  lost.forEach((l) => --arr[l - 1]);
  reserve.forEach((r, i) => {
    if (arr[reserve[i] - 1] == 0) {
      arr[reserve[i] - 1] = 1;
      reserve[i] = false;
    }
  });

  for (let r of reserve) {
    if (!r) continue;
    if (r - 2 >= 0 && arr[r - 2] == 0) arr[r - 2] = 1;
    else if (r < n && arr[r] == 0) arr[r] = 1;
  }

  return arr.reduce((p, c) => p + c);
}
