function solution(x) {
  let xx = "" + x;
  return x % xx.split("").reduce((p, c) => +p + +c) ? false : true;
}

// 나눔수 문제는
// 먼저 나머지를 구하고, 값을 나누어서 줄여야 한다.
function solution(x) {
  let xx = x;
  let sum = 0;
  while (xx > 0) {
    sum += xx % 10;
    xx = Math.floor(xx / 10);
  }

  return x % sum ? false : true;
}
