function solution(n, arr1, arr2) {
  arr1 = arr1.map((v) => {
    let s = v.toString(2);
    return s.length == n ? s : "0".repeat(n - s.length) + s;
  });
  arr2 = arr2.map((v) => {
    let s = v.toString(2);
    return s.length == n ? s : "0".repeat(n - s.length) + s;
  });

  return arr1.map((row, r) => {
    return row
      .split("")
      .map((col, c) => {
        if (col == "1" || arr2[r][c] == "1") return "#";
        else return " ";
      })
      .join("");
  });
}
