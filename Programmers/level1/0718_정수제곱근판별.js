function solution(n) {
  let nn = Math.sqrt(n);
  return Number.isInteger(nn) ? (nn + 1) * (nn + 1) : -1;
}

function solution(n) {
  let nn = Math.sqrt(n);
  return Number.isInteger(nn) ? (nn + 1) ** 2 : -1;
}
