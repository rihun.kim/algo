function solution(arr, divisor) {
  arr = arr.filter((val) => (val % divisor == 0 ? true : false)).sort((a, b) => a - b);
  return arr.length == 0 ? [-1] : arr;
}
