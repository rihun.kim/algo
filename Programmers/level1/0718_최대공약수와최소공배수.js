// 아이디어
// 최대공약수와 최소공배수
// t = n % m
// 두 수를 아무 숫자가 나눈 나머지를 다시금 나누는 숫자로 두고,
// 이전에 나눔을 당한 숫자는 사라진다.
// (순서1) newVal1 = n1 % m1
// (순서2) newVal2 = n2(=m1) % m2(=newVal1)
// 이런식으로 하나씩 앞으로 밀린다고 보면된다.
// 그런뒤, n2의 값을 출력하면 된다.
// 계속 반복시킨 결과를 내면되고
// 최소공배수는 두 수를 최대공약수로 나누면, 최대공약수가 한번밖에 안 남으므로
// 결과가 도출된다.

function gcd(n, m) {
  let t;
  while (m != 0) {
    t = n % m;
    n = m;
    m = t;
  }

  return n;
}

function solution(n, m) {
  let gcdV = gcd(n, m);
  return [gcdV, (n * m) / gcdV];
}
