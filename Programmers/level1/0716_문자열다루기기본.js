function solution(s) {
  return (s.length == 6 || s.length == 4) && "" + parseInt(s) === s;
}
