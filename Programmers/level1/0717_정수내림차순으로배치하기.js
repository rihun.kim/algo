function solution(n) {
  n = "" + n;
  return Number(
    n
      .split("")
      .sort((a, b) => b - a)
      .join("")
  );
}

function solution(n) {
  n = "" + n;
  return +n
    .split("")
    .sort((a, b) => b - a)
    .join("");
}
