function solution(a, b) {
  const day = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
  let date = new Date(["2016-" + a + "-" + b]).getDay();
  // let date = new Date([`2016-${a}-${b}`]).getDay();

  return day[date];
}

function solution(a, b) {
  return new Date(2016, a - 1, b).toString().slice(0, 3).toUpperCase();
}
