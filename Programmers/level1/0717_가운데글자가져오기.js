function solution(s) {
  return s.length % 2 == 0 ? s.substr(s.length / 2 - 1, 2) : s.substr(parseInt(s.length / 2), 1);
}

// parseInt 대신 Math.floor 를 쓸것
// floor 은 소수점 이하 버림
// ceil 은 소수점 존재시 올림
function solution(s) {
  let mid = Math.floor(s.length / 2);
  return s.length % 2 != 0 ? s[mid] : s[mid - 1] + s[mid];
}
