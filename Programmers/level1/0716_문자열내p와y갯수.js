function solution(s) {
  let arr = [0, 0];
  s.toLowerCase()
    .split("")
    .map((val) => {
      if (val == "p") ++arr[0];
      else if (val == "y") ++arr[1];
    });

  return arr[0] == arr[1];
}

function solution(s) {
  return s.toUpperCase().split("P").length === s.toUpperCase().split("Y").length;
}
