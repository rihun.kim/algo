// 기본적으로 두개의 Queue 를 가지고 푸는 문제
// 여기선 jobs 와 queue 의 큐를 가지고 품

function solution(jobs) {
  let N = jobs.length;
  let ans = 0;
  let tick = 0;
  let queue = [];

  while (true) {
    let index = 0;

    // 동적으로 삭제할 때 좋은 코드
    // jobs 가 동적으로 삭제되는 경우
    while (true) {
      if (index >= jobs.length) break;

      if (jobs[index][0] <= tick) queue.push(jobs.splice(index, 1)[0]);
      else index++;
    }

    // queue 가 다 비워지면 탈출
    if (queue.length == 0) {
      if (jobs.length == 0) break;
      else ++tick;
    } else {
      let minIndex = 0;
      for (let i = 1; i < queue.length; ++i) {
        if (queue[minIndex][1] > queue[i][1]) minIndex = i;
      }

      // 시간을 건너뛰도록 설정함 좋은 코드
      tick += queue[minIndex][1];
      ans += tick - queue[minIndex][0];
      queue.splice(minIndex, 1);
    }
  }

  console.log(ans);
  return Math.floor(ans / N);
}

solution([
  [0, 3],
  [1, 9],
  [2, 6],
]);
