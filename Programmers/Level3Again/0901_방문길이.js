function solution(dirs) {
  let checked = new Map();

  let dy = [0, 0, 1, -1]; // 오, 왼, 아래, 위
  let dx = [1, -1, 0, 0];
  let prev = [5, 5];
  let curr = [5, 5];
  let cnt = 0;
  for (let dir of dirs) {
    if (dir == "U") {
      if (canGo(curr[0] + dy[3], curr[1] + dx[3])) {
        prev[0] = curr[0];
        prev[1] = curr[1];
        curr[0] += dy[3];
        curr[1] += dx[3];
        check();
      }
    } else if (dir == "L") {
      if (canGo(curr[0] + dy[1], curr[1] + dx[1])) {
        prev[0] = curr[0];
        prev[1] = curr[1];
        curr[0] += dy[1];
        curr[1] += dx[1];
        check();
      }
    } else if (dir == "R") {
      if (canGo(curr[0] + dy[0], curr[1] + dx[0])) {
        prev[0] = curr[0];
        prev[1] = curr[1];
        curr[0] += dy[0];
        curr[1] += dx[0];
        check();
      }
    } else {
      if (canGo(curr[0] + dy[2], curr[1] + dx[2])) {
        prev[0] = curr[0];
        prev[1] = curr[1];
        curr[0] += dy[2];
        curr[1] += dx[2];
        check();
      }
    }
    // console.log(checked, cnt);
  }

  console.log(cnt);

  function canGo(y, x) {
    if (0 <= y && y < 11 && 0 <= x && x < 11) return true;
    return false;
  }

  function check() {
    let way1 = prev[0] + "," + prev[1] + "-" + curr[0] + "," + curr[1];
    let way2 = curr[0] + "," + curr[1] + "-" + prev[0] + "," + prev[1];
    if (checked[way1] == undefined) {
      checked[way1] = true;
      checked[way2] = true;
      ++cnt;
    }
  }
}
