function solution(operations) {
  let queue = [];

  for (let i = 0; i < operations.length; ++i) {
    if (operations[i][0] == "I") queue.push(+operations[i].split(" ")[1]);
    else if (operations[i] == "D 1") queue.splice(queue.indexOf(Math.max(...queue)), 1);
    else queue.splice(queue.indexOf(Math.min(...queue)), 1);
  }

  if (queue.length == 0) {
    console.log([0, 0]);
    return [0, 0];
  } else {
    console.log([Math.max(...queue), Math.min(...queue)]);
    return [Math.max(...queue), Math.min(...queue)];
  }
}

// solution(["I 16", "D 1"]);
solution(["I -45", "I 653", "D 1", "I -642", "I 45", "I 97", "D 1", "D -1", "I 333"]);
