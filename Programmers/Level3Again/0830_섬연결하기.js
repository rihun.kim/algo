// 프림 알고리즘은
// 최소 스패닝 트리를 만드는 데 사용된다
// 그리디적으로 최소 cost 로 이루어진 다리로
// 정렬하고, 그 다음에 최소값으로 연결할 수 있는 다리를 연결한다.
// 분산적으로 다리가 생겨날 수 있고
// 이 분산적인 다리를 연결하는 방식은
// 각 다리에 부모를 지정시키면서
// 최초의 부모들끼리 비교하는 방식을 사용하면 된다.
// 이 방식을 통헤 최소 스패닝 트리가 완성된다.
// 스패닝 트리란, 모든 노드를 연결하는 트리를 의미한다.
// 네트워크에서 주로 나오는 용어가운데 하나이다.

function solution(n, costs) {
  let i = 0;
  let d = Array.from({ length: n }, () => i++);
  let ans = 0;

  costs.sort((a, b) => a[2] - b[2]);
  for (let cost of costs) {
    let start = unionFind(cost[0]);
    let end = unionFind(cost[1]);
    let val = cost[2];

    if (start != end) {
      d[start] = end;
      ans += val;
    }
  }

  console.log(ans);
  return ans;

  function unionFind(node) {
    if (node == d[node]) return node;
    return (d[node] = unionFind(d[node]));
  }
}
