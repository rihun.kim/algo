// 이 문제는 DFS 로 풀어야하는 것 과 같은 느낌을 준다.
// 하지만 이 문제는 DP 로 푸는 문제이다.
// 굉장히 좋은 문제

function solution(n, money) {
  let mod = 1000000007;
  // d 는 잔돈으로 거슬러 주어야하는 금액까지 준비
  let d = Array.from({ length: n + 1 }, () => 0);
  d[0] = 1;

  // 결국 DP 테이블을 만드는 방법을 계산하는 과정
  // 2차원 배열에서 Y 축은 돈의 종류 갯수, X 축은 만들 수 있는 거스름돈(1~N)
  // cnt 는 사용하는 돈의 종류의 갯수를 컨트롤
  for (let cnt = 1; cnt <= money.length; ++cnt) {
    // cost 는 비용을 측정하는 것으로
    // d[cost] 는 cost 를 만드는 방법의 수를 의미
    for (let cost = 1; cost <= n; ++cost) {
      if (cost < money[cnt - 1]) {
        // 만들어야 되는 돈보다, 더 큰 돈이 들어오면
        // 그대로 유지
        d[cost] = d[cost] % mod;
      } else {
        // cost 를 만드는 방법의 수 더하기
        // 해당하는 돈(money[cnt-1]) 을 뺐을 때,
        // 만들 수 있는 방법의 수 를 더하면 됨
        d[cost] = (d[cost] % mod) + (d[cost - money[cnt - 1]] % mod);
      }
    }
  }

  return d[n];
}
