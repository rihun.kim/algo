function solution(tickets) {
  let N = tickets.length;
  let checked = Array.from({ length: tickets.length }, () => false);

  let ans = [];
  for (let i = 0; i < tickets.length; ++i) {
    if (tickets[i][0] == "ICN") {
      checked[i] = true;
      dfs(tickets[i], [tickets[i]], 1);
      checked[i] = false;
    }
  }

  console.log(ans);
  return ans;

  function dfs(cur, res, cnt) {
    if (cnt == N) {
      let temp = [];
      for (let i = 0; i < res.length - 1; ++i) temp.push(res[i][0]);
      temp.push(res[res.length - 1][0], res[res.length - 1][1]);

      if (ans.length == 0) ans = [...temp];
      else {
        if (JSON.stringify(temp).localeCompare(JSON.stringify(ans)) == -1) ans = [...temp];
      }
      return;
    }

    for (let i = 0; i < tickets.length; ++i) {
      if (cur[0] == tickets[i][0] && cur[1] == tickets[i][1]) continue;

      if (!checked[i] && cur[1] == tickets[i][0]) {
        checked[i] = true;
        res.push(tickets[i]);
        dfs(tickets[i], res, cnt + 1);
        res.pop();
        checked[i] = false;
      }
    }
  }
}

solution([
  ["ICN", "SFO"],
  ["ICN", "ATL"],
  ["SFO", "ATL"],
  ["ATL", "ICN"],
  ["ATL", "SFO"],
]);

solution([
  ["ICN", "JFK"],
  ["HND", "IAD"],
  ["JFK", "HND"],
]);
