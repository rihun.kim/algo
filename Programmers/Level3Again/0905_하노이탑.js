function solution(n) {
  let ans = [];
  move(1, 3, 2, n);

  return ans;

  function move(start, end, mid, cnt) {
    if (cnt == 1) {
      ans.push([start, end]);
      return;
    }

    move(start, mid, end, cnt - 1);
    move(start, end, mid, 1);
    move(mid, end, start, cnt - 1);
  }
}

solution(2);
