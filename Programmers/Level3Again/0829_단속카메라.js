// 그리디는 정렬부터 시작하는게 컨벤션
// 이 문제는 나간 시점에 단속 카메라를 설치한다는 생각으로 접근
// 한 자동차가 나가는 부분에 설치하면,
// 그 사이에 들어온 자동차는 다 뺀다.
// 여기서 동적으로 빼기위한 스킬을 사용한다.

function solution(routes) {
  routes.sort((a, b) => a[1] - b[1]);

  let cam = 0;
  while (routes.length != 0) {
    let car = routes.shift();

    // 동적으로 빼는 스킬
    let index = 0;
    while (true) {
      if (index >= routes.length) break;

      if (routes[index][0] <= car[1]) routes.splice(index, 1);
      else ++index;
    }

    console.log(car, routes);
    ++cam;
  }

  return cam;
}
