// 무식하게 브루트포스 형식으로 푸는 문제
// 내림차순으로 정렬한다.
// 내림차순으로 정렬되면,
// 가장 큰 맨 앞의 값을 빼서
// 하나 줄이고
// 적절한 위치에 넣어 준다.
// 끝....

function solution(n, works) {
  works = works.sort((a, b) => b - a);

  while (n > 0) {
    let val = works.shift();

    --val;
    --n;

    if (val == 0 || works.length == 0) continue;

    let flag = true;
    for (let i = 0; i < works.length; ++i) {
      if (val > works[i]) {
        works.splice(i, 0, val);
        flag = false;
        break;
      }
    }

    if (flag) works.push(val);
  }

  if (works.length == 0) return 0;
  let res = 0;
  works.map((curr) => (res += curr ** 2));
  console.log(works);
  return res;
}

solution(3, [1, 1]);
