// DP 라고 하면 DP 같고
// 완전탐색 이라 하면 완전탐색 같도다.

function solution(N, number) {
  const s = [new Set()];

  for (let i = 1; i <= 8; ++i) {
    s.push(new Set([new Array(i).fill(N).join("") * 1]));
    // new Array(i) : i 는 길이
    // fill(N) 은 N 으로 채우기
    // join 으로 문자열화 시킨 후 * 1 을 통한 숫자화
    // 집합에 넣음 좋은코드!
    for (let j = 1; j <= i; ++j) {
      // i 는 범위화 시킴
      // 즉 숫자의 범위를 1 부터 i 까지 하면서
      // i 범위대역이 1 부터 연산해주면서 자신의 집합을 만듬
      for (const x1 of [...s[j]]) {
        for (const x2 of [...s[i - j]]) {
          // j 와 i-j 을 더하면, i 가 나오고 이는
          // N 의 갯수를 의미
          s[i].add(x1 + x2);
          s[i].add(x1 - x2);
          s[i].add(x1 * x2);
          if (x2 != 0) s[i].add(parseInt(x1 / x2));
        }
      }
    }
    // 해당 집합에 존재시,
    // 최소에 해당하므로 바로 출력 후 종료
    if (s[i].has(number)) return i;
  }

  return -1;
}

solution(5, 12);
