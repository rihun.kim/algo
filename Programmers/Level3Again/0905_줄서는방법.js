function solution(n, k) {
  let cnt = 1;

  let arr = [];
  for (let i = 1; i <= n; ++i) arr.push(i);

  let ans = [];
  for (let i = 0; i < n; ++i) {
    const res = arr.splice(i, 1);
    permu(arr, res);
    arr.splice(i, 0, res[0]);
  }

  return ans;

  function permu(arr, res) {
    if (arr.length == 0) {
      if (cnt == k) {
        ans = [...res];
      }
      ++cnt;
      return;
    }

    for (let i = 0; i < arr.length; ++i) {
      let arr2 = [...arr];
      arr2.splice(i, 1);

      res.push(arr[i]);
      permu([...arr2], res);
      res.pop();
    }
  }
}

solution(3, 5);
