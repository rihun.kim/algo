function solution(n) {
  let matrix = Array.from({ length: n }, () => Array.from({ length: n }, () => 0));

  let cnt = 0;
  for (let y = 0; y < n; ++y) {
    let newMatrix = JSON.stringify(matrix);
    newMatrix = JSON.parse(newMatrix);

    newMatrix[y][0] = 1;
    go(y, 0, 1, newMatrix);
    dfs(1, newMatrix);
  }

  console.log(cnt);
  return cnt;

  function dfs(X, matrix) {
    if (X == n) {
      ++cnt;
      return;
    }

    for (let y = 0; y < n; ++y) {
      if (matrix[y][X] != 1) {
        let newMatrix = JSON.stringify(matrix);
        newMatrix = JSON.parse(newMatrix);

        go(y, X, 1, newMatrix);
        dfs(X + 1, newMatrix);
      }
    }
  }

  function go(Y, X, val, matrix) {
    // 우측 직선
    for (let x = X + 1; x < n; ++x) {
      matrix[Y][x] = val;
    }

    // 우측 상단 대각선
    for (let y = Y - 1, x = X + 1; y >= 0 && x < n; --y, ++x) {
      matrix[y][x] = val;
    }

    // 우측 하단 대각선
    for (let y = Y + 1, x = X + 1; y < n && x < n; ++y, ++x) {
      matrix[y][x] = val;
    }
  }
}

solution(4);
