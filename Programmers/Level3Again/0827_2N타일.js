// 그냥 피보나치 수열처럼 규칙이 있는 DP 문제

function solution(n) {
  let d = [1, 2, 3];

  for (let i = 3; i <= n; ++i) d[i] = (d[i - 1] + d[i - 2]) % 1000000007;

  return d[n - 1] % 1000000007;
}
