let checked;
let N;

function solution(n, computers) {
  N = n;
  checked = Array.from({ length: n }, () => false);

  let cnt = 0;
  for (let i = 0; i < n; ++i) {
    if (!checked[i]) {
      bfs(i, computers);
      ++cnt;
    }
  }

  console.log(cnt);
}

function bfs(idx, computers) {
  let q = [];
  for (let i = 0; i < N; ++i) {
    if (i != idx && computers[idx][i] == 1) {
      q.push(i);
    }
  }

  while (q.length != 0) {
    let com = q.shift();
    if (!checked[com]) {
      checked[com] = true;
      for (let i = 0; i < N; ++i) {
        if (i != com && computers[com][i] == 1) {
          q.push(i);
        }
      }
    }
  }
}

solution(3, [
  [1, 1, 0],
  [1, 1, 1],
  [0, 1, 1],
]);
