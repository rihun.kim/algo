function solution(n, stations, w) {
  let pos = 1;
  let idx = 0;
  let cnt = 0;

  while (pos <= n) {
    if (idx < stations.length && pos >= stations[idx] - w) {
      // 해당 위치가 이미 설치된 기지국 영향에 있는 경우
      pos = stations[idx] + w + 1;
      ++idx;
    } else {
      // 기지국 설치하는 경우
      pos += 2 * w + 1;
      ++cnt;
    }
  }

  console.log(cnt);
  return cnt;
}

solution(16, [9], 2);
