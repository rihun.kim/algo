function solution(N, road, K) {
  let map = new Map();

  for (roa of road) {
    if (map[roa[0]] == undefined) map[roa[0]] = [[roa[1], roa[2]]];
    else map[roa[0]].push([roa[1], roa[2]]);

    if (map[roa[1]] == undefined) map[roa[1]] = [[roa[0], roa[2]]];
    else map[roa[1]].push([roa[0], roa[2]]);
  }

  for (let key in map) map[key].sort((a, b) => a[1] - b[1]);

  let dist = Array.from({ length: N + 1 }, () => Infinity);
  let check = Array.from({ length: N + 1 }, () => false);
  dist[1] = 0;
  check[1] = true;

  bfs(1);
  let cnt = 0;
  for (let di of dist) di <= K ? ++cnt : null;

  console.log(cnt);
  return cnt;

  function bfs(start) {
    let queue = [start];

    while (queue.length != 0) {
      let temp = [];

      for (let qq of queue) {
        for (let edge of map[qq]) {
          if (!check[edge[0]] || dist[edge[0]] > dist[qq] + edge[1]) {
            check[edge[0]] = true;
            dist[edge[0]] = Math.min(dist[edge[0]], dist[qq] + edge[1]);

            temp.push(edge[0]);
          }
        }
      }

      queue = [...temp];
    }
  }
}
