function solution(s) {
  let ans = 1;

  for (let i = 0; i < s.length - 1; ++i) {
    for (let j = s.length; j >= i + 1; --j) {
      if (ans > j - i) continue;
      if (isPalindrome(s.substring(i, j))) {
        ans = Math.max(ans, j - i);
      }
    }
  }

  return ans;

  function isPalindrome(str) {
    let left = 0;
    let right = str.length - 1;
    while (left <= right) {
      if (str[left] != str[right]) return false;
      left++;
      right--;
    }
    return true;
  }
}

solution("abcdcba");
solution("abacde");
