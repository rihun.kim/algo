function solution(n) {
  let d = [];
  d[1] = 1;
  d[2] = 2;

  for (let i = 3; i <= n; ++i) {
    d[i] = (d[i - 2] + d[i - 1]) % 1234567;
  }

  // console.log(d[n]);
  return d[n] % 1234567;
}
