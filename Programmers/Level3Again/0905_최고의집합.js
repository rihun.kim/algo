function solution(n, s) {
  if (n > s) return [-1];

  let num = parseInt(s / n);
  let arr = [];
  for (let i = 1; i <= n; ++i) arr.push(num);

  s -= num * n;
  for (let i = 0; s > 0; s--, ++i) ++arr[i % n];

  arr.sort((a, b) => a - b);
  return arr;
}

solution(2, 9);
