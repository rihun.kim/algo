function solution(triangle) {
  let N = triangle.length - 1;
  for (let i = 1; i < triangle.length; ++i) {
    for (let j = 0; j < triangle[i].length; ++j) {
      let left, right;
      if (j == 0) right = triangle[i - 1][0];
      else if (j == triangle[i].length - 1) left = triangle[i - 1][triangle[i].length - 2];

      if (left == null && right == null) {
        left = triangle[i - 1][j - 1];
        right = triangle[i - 1][j];
      } else if (left == null) {
        left = -1;
      } else {
        right = -1;
      }

      triangle[i][j] += Math.max(left, right);
    }
  }

  return Math.max(...triangle[N]);
}

solution([[7], [3, 8], [8, 1, 0], [2, 7, 4, 4], [4, 5, 2, 6, 5]]);
