function solution(n, edge) {
  let map = new Map();
  let ans = [];
  let checked = Array.from({ length: n + 1 }, () => false);
  checked[1] = true;

  for (let ed of edge) {
    if (map[ed[0]] == undefined) map[ed[0]] = [[ed[1], 0]];
    else map[ed[0]].push([ed[1], 0]);

    if (map[ed[1]] == undefined) map[ed[1]] = [[ed[0], 0]];
    else map[ed[1]].push([ed[0], 0]);
  }

  bfs();

  ans = ans.sort((a, b) => b[1] - a[1]);
  let maxNum = ans[0][1];
  let cnt = 0;
  for (let i = 0; i < ans.length; ++i) {
    if (maxNum == ans[i][1]) ++cnt;
    else break;
  }

  console.log(cnt);
  return cnt;

  function bfs() {
    let queue = [[1, 0]];

    while (queue.length != 0) {
      let node = queue.shift();
      for (let point of map[node[0]]) {
        if (!checked[point[0]]) {
          checked[point[0]] = true;
          ans.push([point[0], node[1] + 1]);
          queue.push([point[0], node[1] + 1]);
        }
      }
    }
  }
}

solution(6, [
  [3, 6],
  [4, 3],
  [3, 2],
  [1, 3],
  [1, 2],
  [2, 4],
  [5, 2],
]);
