function solution(n, times) {
  let startTime = 1;
  let endTime = Math.max(...times) * n;
  let ans = Infinity;

  // 이분탐색의 종료조건
  // start 가 end 을 넘어설 때!
  while (startTime <= endTime) {
    let midTime = parseInt((startTime + endTime) / 2);

    let cnt = 0;
    for (let time of times) cnt += parseInt(midTime / time);

    ///// mid 보다 하나 적거나 크게 갱신값을 설정해준다.
    if (cnt >= n) endTime = midTime - 1;
    else startTime = midTime + 1;

    if (n <= cnt) ans = Math.min(ans, midTime);
  }

  console.log(ans);
  return ans;
}

solution(6, [7, 10]);
