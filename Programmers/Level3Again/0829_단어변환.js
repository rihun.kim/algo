function solution(begin, target, words) {
  let ans = Infinity;
  dfs(begin, 0);

  return ans == Infinity ? 0 : ans;

  function dfs(word, cnt) {
    // console.log(cnt, word);
    if (word == target) {
      ans = Math.min(ans, cnt);
    }

    for (let i = 0; i < words.length; ++i) {
      if (words[i] == "_") continue;

      if (canChange(word, words[i])) {
        let temp = words[i];
        words[i] = "_";
        dfs(temp, cnt + 1);
        words[i] = temp;
      }
    }
  }

  function canChange(wordA, wordB) {
    let cnt = 0;
    for (let i = 0; i < wordA.length; ++i) {
      if (wordA[i] != wordB[i]) {
        if (cnt == 0) ++cnt;
        else return false;
      }
    }

    return true;
  }
}

// solution("hit", "cog", ["hot", "dot", "dog", "lot", "log", "cog"]);
solution("hit", "cog", ["hot", "dot", "dog", "lot", "log"]);
