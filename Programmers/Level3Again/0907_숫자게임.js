// 최소차 승리, 최대차 패배를 이루면 됨
function solution(A, B) {
  A.sort((a, b) => a - b);
  B.sort((a, b) => a - b);

  let cnt = 0;
  let idx = 0;
  for (let a of A) {
    for (let i = idx; i < B.length; ++i) {
      if (B[i] > a) {
        idx = i + 1;
        ++cnt;
        break;
      }
    }
  }

  return cnt;
}
