function solution(genres, plays) {
  let map = new Map();

  for (let i = 0; i < genres.length; ++i) {
    if (map[genres[i]] == undefined) {
      map[genres[i]] = { cnt: [genres[i], plays[i]], arr: [[i, plays[i]]] };
    } else {
      map[genres[i]].cnt[1] += plays[i];
      map[genres[i]].arr.push([i, plays[i]]);
    }
  }

  let gen = [];
  for (let key in map) gen.push(map[key].cnt);
  gen.sort((a, b) => b[1] - a[1]);

  let ans = [];
  for (let ge of gen) {
    ans.push(
      map[ge[0]].arr
        .sort((a, b) => {
          if (a[1] == b[1]) {
            return a[0] - b[0];
          } else {
            return b[1] - a[1];
          }
        })
        .splice(0, 1)[0][0]
    );
    if (map[ge[0]].arr.length != 0) {
      ans.push(map[ge[0]].arr[0][0]);
    }
  }

  console.log(ans);
  return ans;
}

solution(["classic", "pop", "classic", "classic", "pop"], [500, 600, 150, 800, 2500]);
