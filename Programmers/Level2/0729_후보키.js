function transpose(relation) {
  let matrix = [];
  for (let c = 0; c < relation[0].length; ++c) {
    let arr = [];
    for (let r = 0; r < relation.length; ++r) arr.push(relation[r][c]);
    matrix.push(arr);
  }
  return matrix;
}

let final = [];
function recurv(source, target, n, r, count) {
  if (r == 0) final.push(target);
  else if (n == 0 || n < r) return;
  else {
    target.push(source[count]);
    recurv(source, [...target], n - 1, r - 1, count + 1);
    target.pop();
    recurv(source, [...target], n - 1, r, count + 1);
  }
}

function isUnique(combi, matrix) {
  let ans = [];
  for (let r = 0; r < matrix[0].length; ++r) {
    let row = "";
    for (let com of combi) row += matrix[com][r];

    if (ans.includes(row)) return false;
    ans.push(row);
  }
  return true;
}

function solution(relation) {
  let matrix = transpose(relation);
  let ans = 0,
    combiSet = [];
  matrix.map((v, i) => (new Set(v).size == v.length ? ++ans : combiSet.push(i)));

  let skip = [];
  for (let r = 2; r <= combiSet.length; ++r) {
    recurv(combiSet, [], combiSet.length, r, 0);

    for (let combi of final) {
      let combiHasAllSkip = [];
      for (let sk of skip) {
        for (let s of sk) {
          if (!combi.includes(s)) {
            combiHasAllSkip.push(false);
            break;
          }
        }
      }
      if (combiHasAllSkip.length !== skip.length) continue;

      if (isUnique(combi, matrix)) {
        skip.push(combi);
        ++ans;
      }
    }

    final = [];
  }

  return ans;
}
