// 그리디 문제
// 오름차순시켜서 맨앞과 맨뒤의 인덱스를 잡고
// 상황에 따라 줄이는 문제

function solution(people, limit) {
  let cnt = 0;
  let asc = [...people.sort((a, b) => a - b)];
  let dec = [...people.sort((a, b) => b - a)];

  for (let a = 0, d = 0; asc.length != 0 && dec.length != 0; ) {
    if (asc[a] + dec[d] <= limit) {
      asc.shift();
      asc.pop();
      dec.shift();
      dec.pop();
      ++cnt;
    } else {
      dec.shift();
      asc.pop();
      ++cnt;
    }
  }

  return cnt;
}

function solution(people, limit) {
  people.sort((a, b) => a - b);
  let i, j;
  for (i = 0, j = people.length - 1; i < j; --j) {
    if (people[i] + people[j] <= limit) ++i;
  }

  return people.length - i;
}
