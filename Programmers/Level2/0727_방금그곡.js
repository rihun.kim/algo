function rep(str) {
  str = str
    .replace(/(C#)/g, "c")
    .replace(/(D#)/g, "d")
    .replace(/(F#)/g, "f")
    .replace(/(G#)/g, "g")
    .replace(/(A#)/g, "a");
  return str;
}

function filter(str, len) {
  str = rep(str);

  while (str.length < len) str += str;
  return str.substring(0, len);
}

function solution(m, musicinfos) {
  m = rep(m);
  musicinfos = musicinfos.map((v, i) => {
    let arr = v.split(/[:,]/);
    let musicLen = (arr[2] - arr[0]) * 60 + (arr[3] - arr[1]);

    return [i, arr[4], musicLen, filter(arr[5], musicLen)];
  });

  let ans = [Infinity, "", 0, ""];
  for (let music of musicinfos) {
    if (music[3].includes(m)) {
      if (music[2] == ans[2]) {
        ans = ans[0] < music[0] ? ans : music;
      } else {
        ans = ans[2] < music[2] ? music : ans;
      }
    }
  }

  return ans[1] == "" ? "(None)" : ans[1];
}
