function solution(n, words) {
  let who = 0;
  let round = 1;
  let isLoserExisted = false;
  let prevWord = words[0];
  let usedWords = [words[0]];
  for (let i = 1; i < words.length; ++i) {
    who = ++who % n;
    if (who == 0) ++round;

    if (usedWords.indexOf(words[i]) != -1 || prevWord[prevWord.length - 1] != words[i][0]) {
      isLoserExisted = true;
      break;
    } else {
      prevWord = words[i];
      usedWords.push(words[i]);
    }
  }

  return isLoserExisted ? [who + 1, round] : [0, 0];
}
