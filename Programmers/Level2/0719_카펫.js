// 완전탐색
function solution(brown, yellow) {
  for (let row = 1; row <= brown / 2; ++row) {
    let yelCol = yellow / row;
    if (Number.isInteger(yelCol)) {
      if ((row + 2) * 2 + yelCol * 2 == brown) {
        return [yelCol + 2, row + 2];
      }
    }
  }
}
