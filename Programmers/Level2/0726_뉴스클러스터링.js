function filter(str) {
  str = str.split("");
  const check_eng = /[a-zA-Z]/;
  let stk = [];
  let ans = [];
  for (let i = 0; i < str.length; ) {
    if (check_eng.test(str[i])) {
      stk.push(str[i]);
      if (stk.length == 2) {
        ans.push(stk.join("").toUpperCase());
        stk = [str[i]];
      }
      ++i;
    } else {
      stk = [];
      ++i;
    }
  }

  return ans;
}

function solution(str1, str2) {
  str1 = filter(str1);
  str2 = filter(str2);

  if (str1.length == 0 && str2.length == 0) return 65536;

  let allSet = str1.length + str2.length;
  let inSet = 0;
  str1.map((v) => {
    if (str2.includes(v)) {
      str2.splice(str2.indexOf(v), 1);
      ++inSet;
    }
  });
  allSet -= inSet;

  return parseInt((inSet / allSet) * 65536);
}

//////
function filter(str) {
  let res = [];
  for (let i = 0; i < str.length - 1; ++i) {
    let text = str.substr(i, 2);
    if (text.match(/[a-zA-Z]{2}/)) {
      res.push(text.toUpperCase());
    }
  }
  return res;
}

function solution(str1, str2) {
  str1 = filter(str1);
  str2 = filter(str2);

  if (str1.length == 0 && str2.length == 0) return 65536;

  let allSet = str1.length + str2.length;
  let inSet = 0;
  str1.map((v) => {
    if (str2.includes(v)) {
      str2.splice(str2.indexOf(v), 1);
      ++inSet;
    }
  });
  allSet -= inSet;

  return parseInt((inSet / allSet) * 65536);
}
