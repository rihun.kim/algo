function solution(arr1, arr2) {
  // arr1 의 가로행과 arr2 의 세로열 곱하기

  let ans = [];
  for (let ar1 of arr1) {
    let temp = [];
    for (let col = 0; col < arr2[0].length; ++col) {
      let sum = 0;
      for (let row = 0; row < arr2.length; ++row) {
        sum += ar1[row] * arr2[row][col];
      }
      temp.push(sum);
    }
    ans.push(temp);
  }

  return ans;
}

function solution(arr1, arr2) {
  return arr1.map((row) => arr2[0].map((x, y) => row.reduce((a, b, c) => a + b * arr2[c][y], 0)));
}
