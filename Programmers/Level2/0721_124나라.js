// 규칙을 찾아내는 문제
// 3진법이라는 생소한 방식을 제공함..

function solution(n) {
  let ans = "";
  while (n > 0) {
    let tmp = n % 3;
    if (tmp == "0") {
      ans = "4" + ans;
      --n;
    } else ans = tmp + ans;
    n = parseInt(n / 3);
  }

  return ans;
}

function solution(n) {
  let ans = "";
  while (n) {
    let tmp = n % 3;
    if (tmp == "0") {
      ans = "4" + ans;
      --n;
    } else ans = tmp + ans;
    n = parseInt(n / 3);
  }

  return ans;
}
