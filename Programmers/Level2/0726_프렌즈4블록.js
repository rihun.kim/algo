function solution(m, n, board) {
  board = board.map((v) => v.split("").map((v) => [v, 0]));

  let ans = 0;
  let flag;
  while (!flag) {
    flag = true;
    for (let r = 0; r < m - 1; ++r) {
      for (let c = 0; c < n - 1; ++c) {
        if (board[r][c][0] == "NO") continue;
        if (
          board[r][c][0] == board[r + 1][c + 1][0] &&
          board[r][c][0] == board[r + 1][c][0] &&
          board[r][c][0] == board[r][c + 1][0]
        ) {
          board[r][c][1] = 1;
          board[r + 1][c + 1][1] = 1;
          board[r + 1][c][1] = 1;
          board[r][c + 1][1] = 1;
          flag = false;
        }
      }
    }

    for (let c = 0; c < n; ++c) {
      let remain = [];
      for (let r = m - 1; r >= 0; --r) {
        if (board[r][c][1] == 1) ++ans;
        else remain.push(board[r][c]);
      }

      for (let r = m - 1; r >= 0; --r) {
        if (remain.length != 0) board[r][c] = remain.shift();
        else board[r][c] = ["NO", 0];
      }
    }
  }

  return ans;
}
