function solution(progresses, speeds) {
  let ans = [];
  while (progresses.length != 0) {
    progresses.forEach((v, i) => (progresses[i] += speeds[i]));

    let cnt = 0;
    while (progresses[0] >= 100) {
      progresses.shift();
      speeds.shift();
      ++cnt;
    }

    cnt ? ans.push(cnt) : null;
  }

  return ans;
}
