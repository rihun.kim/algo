function solution(s) {
  let ans = s
    .split(" ")
    .map((v) => {
      if (v != "") return v[0].toUpperCase() + v.toLowerCase().substring(1, v.length) + " ";
      else return " ";
    })
    .join("");

  return ans.substring(0, ans.length - 1);
}

// split(" ") 의 특징
// let x = "A   B" // 빈칸이 세개 존재
// x.split(" ") 하면 ["A","","","B"] 가 나옴
// "" 아무것도없는 문자열을 의미
// 이렇게 한 이유는 join 을 통해, 동일하게 복원하기 위함이다.
// 따라서, x.join(" ") 하면
// A + " " + "" + " " + "" + " " + B 이런식으로 완성된다.
function solution(s) {
  return s
    .split(" ")
    .map((v) => v.charAt(0).toUpperCase() + v.substring(1).toLowerCase())
    .join(" ");
}

function solution(s) {
  return s
    .split(" ")
    .map((v) => v.charAt(0).toUpperCase() + v.substring(1).toLowerCase())
    .join(" ");
}
