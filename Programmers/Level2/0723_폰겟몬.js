function solution(nums) {
  let len = nums.length / 2;
  let arr = [];
  nums.forEach((v) => {
    if (!arr.includes(v)) arr.push(v);
  });

  return len > arr.length ? arr.length : len;
}

function solution(nums) {
  const setSize = new Set(nums).size;
  const numSize = nums.length / 2;
  return setSize > numSize ? numSize : setSize;
}
