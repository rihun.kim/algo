// 재귀함수 짜는 요령
// 코테의 경우 내부에 재귀함수 짜는 것이 유리
// 재귀함수의 핵심은 터미네이트 조건
// 명확히 그 부분부터 세워야함
// return 에 목숨걸 필요 없음, 되돌아올 경우가 아니라면...
// 경우에 따른 recur 복수개 등장 필요

function solution(numbers, target) {
  let ans = 0;

  function recur(count, sum) {
    if (count == numbers.length) {
      sum == target ? ++ans : false;
      return;
    }

    recur(count + 1, sum + numbers[count]);
    recur(count + 1, sum - numbers[count]);
  }

  recur(0, 0);
  return ans;
}
