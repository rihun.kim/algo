function recurv(source) {
  if (source == "") return "";

  let lstk = 0,
    rstk = 0;
  let ustr = [];
  for (let i = 0; i < source.length; ++i) {
    if (source[i] == "(") {
      ustr.push("(");
      ++lstk;
    } else {
      ustr.push(")");
      ++rstk;
    }
    if (lstk == rstk) break;
  }

  let vstr = source.substring(ustr.length);

  let test = [];
  for (let ele of ustr) {
    if (ele == "(") test.push("(");
    else test.pop();
  }

  if (test.length != 0) {
    let str = "(" + recurv(vstr) + ")";
    ustr.shift();
    ustr.pop();
    ustr = ustr.map((v) => (v == ")" ? "(" : ")")).join("");
    str += ustr;
    return str;
  } else {
    ustr = ustr.join("");
    return ustr + recurv(vstr);
  }
}

function solution(p) {
  return recurv(p);
}
