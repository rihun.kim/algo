function solution(arrangement) {
  arrangement = arrangement.split("");

  let stk = [];
  for (let a of arrangement) {
    if (a == "(") stk.push("(");
    else if (a == ")") {
      if (stk[stk.length - 1] == "(") {
        stk.pop();
        stk.push(0);
      } else {
        stk.push(")");
      }
    }
  }

  let sum = 0;
  let leNum = 0;
  for (let a of stk) {
    if (a == "(") leNum++;
    else if (a == 0) {
      sum += leNum;
    } else if (a == ")") {
      ++sum;
      --leNum;
    }
  }

  return sum;
}
