function doOp(a, b, o) {
  if (o == "+") return +a + +b;
  else if (o == "-") return +a - +b;
  else return +a * +b;
}

function solution(expression) {
  let ops = [
    ["+", "-", "*"],
    ["+", "*", "-"],
    ["-", "+", "*"],
    ["-", "*", "+"],
    ["*", "+", "-"],
    ["*", "-", "+"],
  ];

  let ex = expression.split(/[^0-9]/);
  let op = expression.match(/[\s-,\s+,\s*]/g);
  let exp = [];
  ex.map((v, i) => exp.push(v, op[i]));
  exp.pop();

  let ans = 0;
  for (let op of ops) {
    let stk = [];
    let expp = [...exp];
    for (let o of op) {
      stk = [];
      for (let i = 0; i < expp.length; ) {
        if (expp[i] == o) {
          stk.push(doOp(stk.pop(), expp[i + 1], o));
          i += 2;
        } else {
          stk.push(expp[i]);
          ++i;
        }
      }
      expp = [...stk];
    }

    if (Math.abs(stk[0]) > ans) ans = Math.abs(stk[0]);
  }

  return ans;
}
