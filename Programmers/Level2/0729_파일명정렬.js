// 문자열 비교는
// 무조건 localeCompare 을 써라.
// 단순 stable sort 문제이지만
// 문자열 비교에서 localeCompare 을 쓰지 않으면, 핵 쓰레기 결과가 나오는 문제다...

function solution(files) {
  let arr = [];
  for (let i = 0; i < files.length; ++i) {
    let len = files[i].length;

    let head = "",
      h;
    for (h = 0; h < len; ++h) {
      if (isNaN(files[i][h]) || files[i][h] == " ") continue;
      else break;
    }
    head = files[i].substring(0, h);

    let num = "",
      n;
    for (n = h; n < h + 5; ++n) {
      if (isNaN(files[i][n]) || files[i][n] == " ") break;
    }
    num = files[i].substring(h, n);

    arr.push([head, num, files[i].substring(n), i]);
  }

  arr = arr.sort((a, b) => {
    if (a[0].toLowerCase() == b[0].toLowerCase()) {
      if (+a[1] == +b[1]) return a[3] - b[3];
      else return +a[1] - +b[1];
    } else return a[0].toLowerCase().localeCompare(b[0].toLowerCase());
  });

  return arr.map((v) => v[0] + v[1] + v[2]);
}

// 레퍼
function solution(files) {
  let answerWrap = files.map((file, index) => ({ file, index }));
  const compare = (a, b) => {
    const reg = /(\D*)(\d*)/i;
    const A = a.match(reg);
    const B = b.match(reg);
    const compareHead = A[1].toLowerCase().localeCompare(B[1].toLowerCase());
    const compareNumber = (a, b) => {
      return parseInt(a) > parseInt(b) ? 1 : parseInt(a) < parseInt(b) ? -1 : 0;
    };
    return compareHead === 0 ? compareNumber(A[2], B[2]) : compareHead;
  };
  answerWrap.sort((a, b) => {
    const result = compare(a.file, b.file);
    return result === 0 ? a.index - b.index : result;
  });
  return answerWrap.map((answer) => answer.file);
}
