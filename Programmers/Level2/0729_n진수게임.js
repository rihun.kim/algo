// 자기 차례마다 숫자를 뽑는것은
// 자신의 시작수에 인터벌만큼씩 더해주면서 구하는게 심플함

function solution(n, t, m, p) {
  let str = "";
  let len = m * (t - 1) + p;
  for (let i = 0; str.length < len; ++i) str += i.toString(n);
  str = str.substring(0, len).toUpperCase();

  let ans = "";
  for (let i = p - 1; i <= str.length; i += m) ans += str[i];

  return ans;
}
