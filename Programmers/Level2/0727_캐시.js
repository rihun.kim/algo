function solution(cacheSize, cities) {
  if (cacheSize == 0) return cities.length * 5;

  cities = cities.map((v) => v.toLowerCase());
  let cache = [];
  let ans = 0;
  for (let city of cities) {
    let idx = cache.indexOf(city);
    if (idx != -1) {
      ans += 1;
      cache.push(cache.splice(idx, 1)[0]);
    } else {
      ans += 5;
      if (cache.length < cacheSize) {
        cache.push(city);
      } else {
        cache.shift();
        cache.push(city);
      }
    }
  }

  return ans;
}
