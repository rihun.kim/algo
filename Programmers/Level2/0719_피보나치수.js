// 재귀로 풀면 느려서 반복문으로 품

function solution(n) {
  let a = 0;
  let b = 1;
  let c = 0;

  for (let i = 1; i < n; ++i) {
    c = a + b;
    a = b % 1234567;
    b = c % 1234567;
  }

  return c % 1234567;
}
