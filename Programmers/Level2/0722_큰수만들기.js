function solution(number, k) {
  number = number.split("");

  let stk = [-1];
  ++k;
  for (let n of number) {
    while (k > 0) {
      if (stk[stk.length - 1] < n) {
        stk.pop();
        --k;
        if (stk.length == 0) break;
      } else break;
    }
    stk.push(n);
  }

  while (k > 0) {
    stk.pop();
    --k;
  }

  return stk.join("");
}
