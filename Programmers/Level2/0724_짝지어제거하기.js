function solution(s) {
  let stk = [];

  s = s.split("");
  for (let ss of s) {
    if (stk[stk.length - 1] != ss) stk.push(ss);
    else stk.pop();
  }

  return stk.length == 0 ? 1 : 0;
}
