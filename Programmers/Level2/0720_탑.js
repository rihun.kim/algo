function solution(heights) {
  let ans = [];
  for (let i = heights.length - 1; i >= 0; --i) {
    let find = false;
    for (let j = i - 1; j >= 0; --j) {
      if (heights[i] < heights[j]) {
        ans.unshift(j + 1);
        find = true;
        break;
      }
    }
    if (!find) ans.unshift(0);
  }

  return ans;
}

// 레퍼런스용 코드
function solution(heights) {
  return heights.map((v, i) => {
    while (i) {
      i--;
      if (heights[i] > v) return i + 1;
    }
    return 0;
  });
}
