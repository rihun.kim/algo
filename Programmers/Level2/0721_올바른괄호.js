function solution(s) {
  let stk = [];
  s = s.split("");

  for (let ss of s) {
    if (ss == "(") stk.push("(");
    else {
      if (stk[stk.length - 1] == "(") stk.pop();
      else return false;
    }
  }

  return stk.length == 0 ? true : false;
}
