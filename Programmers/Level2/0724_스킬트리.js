function solution(skill, skill_trees) {
  let ans = 0;
  for (let st of skill_trees) {
    let skIndex = 0;
    let arr = st.split("");
    let err = false;
    for (let a of arr) {
      let resIndex = skill.indexOf(a);
      if (resIndex != -1) {
        if (skIndex == resIndex) ++skIndex;
        else {
          err = true;
          break;
        }
      }
    }

    if (!err) ++ans;
  }

  return ans;
}
