function makeString(arr) {
  return arr
    .map((v) => {
      if (v[0] == "1") return "" + v[1];
      else return "" + v[0] + v[1];
    })
    .join("");
}

function solution(s) {
  let ans = s.length;

  for (let i = 1; i <= s.length / 2; ++i) {
    let stk = [];
    for (let j = 0, cnt = 1; j < s.length; j += i) {
      let word = s.substr(j, i);
      if (stk.length != 0 && stk[stk.length - 1][1] == word) {
        ++stk[stk.length - 1][0];
      } else {
        cnt = 1;
        stk.push([cnt, word]);
      }
    }
    ans = Math.min(makeString(stk).length, ans);
  }

  return ans;
}
