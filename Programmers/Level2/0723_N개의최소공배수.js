function gcd(a, b) {
  while (b > 0) {
    let t = a % b;
    a = b;
    b = t;
  }
  return a;
}

function lcm(a, b) {
  return (a * b) / gcd(a, b);
}

function solution(arr) {
  let gg = arr[0];
  for (let i = 1; i < arr.length; ++i) {
    gg = lcm(gg, arr[i]);
  }

  return gg;
}

function solution(arr) {
  return arr.reduce((a, b) => (a * b) / gcd(a, b));
}

function gcd(a, b) {
  return a % b ? gcd(b, a % b) : b;
}
