// 아이디어
// 단순 수학문제
// 조합문제..
// 각 종류별로 곱해주고, 그 때 +1 해줘서
// 하나씩만 입는 것도 생각
// 최종적으로 안입는 것만 빼주면됨..
// 수학문제였음

function solution(clothes) {
  let map = new Map();
  for (let c of clothes) {
    if (map[c[1]] == undefined) map[c[1]] = 1;
    else ++map[c[1]];
  }

  let arr = [];
  for (let key in map) arr.push(map[key]);
  if (arr.length == 1) return arr[0];

  let ans = 1;
  arr.forEach((v) => {
    ans *= v + 1;
  });

  return ans - 1;
}

// 레퍼런스 코드
// 중요코드
function solution(clothes) {
  let answer = 1;
  const obj = {};
  for (let arr of clothes) {
    obj[arr[1]] = (obj[arr[1]] || 0) + 1;
    // 생성과 이미있는 것을 한번에 사용할 수 있음
    // 좋은 코드
  }

  for (let key in obj) {
    answer *= obj[key] + 1;
  }

  return answer - 1;
}

function solution(clothes) {
  let ans = {};
  for (let c of clothes) ans[c[1]] = (ans[c[1]] || 0) + 1;
  let sum = 1;
  for (let key in ans) sum *= ans[key] + 1;
  return --sum;
}
