// 슬라이딩 윈도우 문제

function solution(n) {
  let len = Math.ceil(n / 2);
  let ans = 1;
  for (let i = 1; i <= len; ++i) {
    let sum = 0;
    for (let j = i; j <= len; ++j) {
      sum += j;
      if (sum == n) {
        ++ans;
        break;
      } else if (sum > n) break;
    }
  }

  return ans;
}
