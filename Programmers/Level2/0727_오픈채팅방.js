function solution(record) {
  let map = new Map();

  let ans = [];
  for (let reco of record) {
    let arr = reco.split(" ");
    if (arr[0] == "Enter") {
      ans.push([arr[1], "님이 들어왔습니다."]);
      map[arr[1]] = arr[2];
    } else if (arr[0] == "Leave") {
      ans.push([arr[1], "님이 나갔습니다."]);
    } else {
      map[arr[1]] = arr[2];
    }
  }

  for (let i = 0; i < ans.length; ++i) {
    ans[i] = "" + map[ans[i][0]] + ans[i][1];
  }

  return ans;
}
