// 그리디적으로
// 매순간의 행은 가장 이전 값중 가장 최대의 값을 선택한다.
// 그래야 최종적으로 최적의 값이 도출된다.
// 그리고, 여기서 조건이 자기 윗행은 못 추가하므로
// 그 경우는 빼주고 한다.
// 이 문제의 핵심은 윗행과 아랫행 사이의 관계를
// 누적 연산합으로 풀 수 있는 지를 물어보고 있다.
// 그리고 거기에 그리디적으로 가장 최적의 나오는 경우를 물어보고 있다.
// 그리고 코딩 테크닉으로 그냥 사열해서 찾는 것도 오히려 간단한 로직 만듬
// 아래에서 위로 생각하는 차이를 발견하는 문제

function solution(land) {
  for (let i = 1; i < land.length; ++i) {
    land[i][0] += Math.max(land[i - 1][1], land[i - 1][2], land[i - 1][3]);
    land[i][1] += Math.max(land[i - 1][2], land[i - 1][3], land[i - 1][0]);
    land[i][2] += Math.max(land[i - 1][3], land[i - 1][0], land[i - 1][1]);
    land[i][3] += Math.max(land[i - 1][0], land[i - 1][1], land[i - 1][2]);
  }

  return Math.max(...land[land.length - 1]);
}
