// DP 로 푸는 문제
// 2차원 배열상에서 DP 로 계속 정사각형을 찾는 문제

function solution(board) {
  let max = Math.max(...board[0]);

  let R = board.length;
  let C = board[0].length;
  for (let r = 1; r < R; ++r) {
    for (let c = 1; c < C; ++c) {
      if (board[r][c] == 1) {
        board[r][c] = Math.min(board[r - 1][c - 1], board[r - 1][c], board[r][c - 1]) + 1;
        max = Math.max(board[r][c], max);
      }
    }
  }

  return max * max;
}
