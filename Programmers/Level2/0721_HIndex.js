// 그리디 문제에 가까움
// 내림차순해서 나아갈때,
// 갯수가 최대일때가 최적인 경우임
// 최대에서 감소하는 순간이 루프를 끝내야하는 경우이며
// 그 때가 최적의 값인 상황
// 인용의 권수가 키를 쥐고 있는 문제임

function solution(citations) {
  citations.sort((a, b) => b - a);
  let ans = { val: citations[0], cnt: 0 };
  citations.forEach((v) => {
    if (v == 0) {
    } else if (ans.val == v) {
      if (ans.cnt < v) ++ans.cnt;
    } else if (ans.val > v) {
      if (ans.cnt < v) ++ans.cnt;
      ans.val = v;
    }
  });

  return citations[0] == 0 ? 0 : ans.cnt;
}

function solution(citations) {
  citations = citations.sort((a, b) => b - a);
  let i = 0;
  while (i + 1 <= citations[i]) i++;
  return i;
}
