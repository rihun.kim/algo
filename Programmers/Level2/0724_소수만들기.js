function solution(nums) {
  let arr = [];
  for (let i = 0; i < nums.length - 2; ++i) {
    for (let j = i + 1; j < nums.length - 1; ++j) {
      for (let k = j + 1; k < nums.length; ++k) {
        arr.push(nums[i] + nums[j] + nums[k]);
      }
    }
  }

  let ans = 0;
  for (let ar of arr) {
    let flag = false;
    for (let i = 2; i < ar; ++i) {
      if (ar % i == 0) {
        flag = true;
        break;
      }
    }
    if (!flag) ++ans;
  }
  return ans;
}
