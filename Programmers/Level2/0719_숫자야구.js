// 완전탐색 문제
function test(num, ori) {
  num = "" + num;
  ori = "" + ori;

  let strike = 0;
  let ball = 0;
  num.split("").forEach((n, i) => {
    if (n == ori[i]) ++strike;
    else if (ori.includes(n)) ++ball;
  });

  return { strike: strike, ball: ball };
}

function solution(baseball) {
  let ans = 0;
  for (let num = 123; num <= 987; ++num) {
    let snum = "" + num;
    if (snum[0] != snum[1] && snum[0] != snum[2] && snum[1] != snum[2] && !snum.includes("0")) {
      let ok = true;
      for (let ele of baseball) {
        let res = test(num, ele[0]);
        if (res.strike != ele[1] || res.ball != ele[2]) {
          ok = false;
          break;
        }
      }
      if (ok) ++ans;
    }
  }

  return ans;
}
