// 완전탐색 문제

// 배열안에 있는지 체크할때, includes 을 주로 쓸것,
// Map 보다 쓰기 편함
function isPrime(num, ans) {
  if (num == 0 || num == 1) return false;
  if (ans.includes(num)) return false;

  for (let i = 2; i < num; ++i) if (num % i == 0) return false;

  return true;
}

// 중요 암기코드
// [시험유형]
// 순열을 만드는 재귀코드
// 아이디어
// 최초에 각 숫자들로 구분하여, 배열에 넣는다.
// 이 경우, 문자열로 처리하는 게 편함
// 그 다음, 새 배열을 만들어, 들어가있는 숫자중 하나를 빼서
// 재귀적으로 추진하고, 추진할때, 내가 뺀 숫자를 문자로 넘긴다.
// 그 문자가 완성되는 형식의 기반이 된다.
// 그렇게 재귀적으로 넘어오면,
// 넘어온 문자로 원래 수행해야 하는 로직을 수행하고,
// 그 다음에 헤애할 재귀의 경우를 수행한다.
// 그 재귀에서도 마찬가지로 또 하나의 숫자를 빼고 넘긴다.
// 이 모든 경우에서 반복문은 필수 이다.
// 즉, 완전탐색에서
// 재귀와 반복문의 조함을 하면 가능해진다.
function permu(arr, str, ans) {
  if (arr.length > 0) {
    for (let i = 0; i < arr.length; ++i) {
      let tmp = [...arr];
      tmp.splice(i, 1);
      permu(tmp, str + arr[i], ans);
    }
  }

  if (isPrime(+str, ans)) ans.push(+str);
}

function solution(numbers) {
  let ans = [];
  permu("" + numbers, "", ans);
  return ans.length;
}
