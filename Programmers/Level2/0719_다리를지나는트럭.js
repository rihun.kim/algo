// 아이디어
// [시험유형]
// 큐잉문제
// 문제자체에 대한 설명도 길고,
// 문제를 구성하는데도, 절차적인 로직이 많이 필요한 문제 유형
// 대부분 코테는 이런식의 문제가 많음
// 풀기 굉장히 귀찮은 문제 유형...
function solution(bridge_length, weight, truck_weights) {
  let queue = Array.from({ length: bridge_length }, () => 0);
  let bwSum = 0; // 다리가 현재 견디는 무게
  let blSum = 0; // 다리에 현재 있는 트럭수
  let time;
  for (time = 1; truck_weights.length != 0; ++time) {
    let front = queue.shift();
    if (front) {
      bwSum -= front;
      --blSum;
    }

    if (bwSum + truck_weights[0] <= weight && blSum + 1 <= bridge_length) {
      bwSum += truck_weights[0];
      queue.push(truck_weights.splice(0, 1)[0]);
      ++blSum;
    } else {
      queue.push(0);
      blSum = blSum > 0 ? --blSum : 0;
    }
  }

  return time + bridge_length - 1;
}

// 좋은 리팩토링
// 트럭이 다리에 올라올때, 끝날 시간을 표기
// 시간 부분 시퀀셜 이해가 어려움
const solution = (bridgeLength, weight, truckWeights) => {
  const progress = [];
  let i = 1;
  while (true) {
    progress.map((item, j) => {
      if (item.end === i) progress.splice(j, 1);
    });

    if (progress.reduce((p, c) => p + c.weight, 0) + truckWeights[0] <= weight) {
      progress.push({ end: i + bridgeLength, weight: truckWeights.shift() });
    }

    if (!progress.length && !truckWeights.length) break;
    i += 1;
  }

  return i;
};
