function solution(n) {
  n = ("0" + n.toString(2)).split("");

  let oneCnt = 0;
  for (let i = n.length - 1; i > 0; --i) {
    if (n[i - 1] + n[i] == "01") {
      n[i - 1] = "1";
      n[i] = "0";
      for (let j = n.length - 1; j > i; --j) {
        if (oneCnt > 0) {
          n[j] = "1";
          --oneCnt;
        } else {
          n[j] = "0";
        }
      }
      break;
    }
    if (n[i] == "1") ++oneCnt;
  }

  return parseInt(n.join(""), 2);
}

// 정규표현식으로 풀기
function solution(n) {
  var size = n.toString(2).match(/1/g).length;
  while (n++) {
    if (size === n.toString(2).match(/1/g).length) return n;
  }
}
