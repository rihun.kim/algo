function solution(s) {
  let used = [];
  let ans = [];

  s = s.replace(/{/gi, "[");
  s = s.replace(/}/gi, "]");
  s = JSON.parse(s);

  s.sort((a, b) => a.length - b.length).map((v) => {
    for (let vv of v) {
      if (used.indexOf(vv) == -1) {
        ans.push(vv);
        used.push(vv);
      }
    }
  });

  return ans;
}
