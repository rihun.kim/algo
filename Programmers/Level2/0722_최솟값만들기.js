function solution(A, B) {
  A.sort((a, b) => a - b);
  B.sort((a, b) => b - a);

  let sum = 0;
  A.map((v, i) => {
    sum += v * B[i];
  });
  return sum;
}

function solution(A, B) {
  A.sort((a, b) => a - b);
  B.sort((a, b) => b - a);

  return A.reduce((prev, curr, idx) => prev + curr * B[idx], 0);
}
