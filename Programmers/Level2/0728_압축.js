function solution(msg) {
  let map = new Map();
  for (let i = 0; i < 26; ++i) map[String.fromCharCode(65 + i)] = i + 1;

  let ans = [];
  for (let i = 0, key = 27; i < msg.length; ) {
    let str = msg[i];
    let j;
    for (j = 1; i + j < msg.length; ++j) {
      if (map[str + msg[i + j]] == undefined) {
        map[str + msg[i + j]] = key++;
        break;
      }
      str += msg[i + j];
    }

    i += j;
    ans.push(map[str]);
  }

  return ans;
}
