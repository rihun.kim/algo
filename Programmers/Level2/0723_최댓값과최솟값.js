function solution(s) {
  s = s.split(" ").map((v) => +v);
  return "" + Math.min.apply(null, s) + " " + Math.max.apply(null, s);
}

function solution(s) {
  s = s.split(" ");
  return Math.min(...s) + " " + Math.max(...s);
}
