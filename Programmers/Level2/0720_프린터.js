function solution(priorities, location) {
  let ans = 0;
  while (true) {
    let flg = false;
    for (let i = 1; i < priorities.length; ++i) {
      if (priorities[0] < priorities[i]) {
        flg = true;
        break;
      }
    }

    if (flg) {
      priorities.push(priorities.shift());
      if (--location < 0) location = priorities.length - 1;
    } else {
      priorities.shift();
      ++ans;
      if (--location < 0) break;
    }
  }

  return ans;
}

// 레퍼런스 코드
// some 을 써서 해당영역이 있는지 확인하는 코드가 좋음
function solution(priorities, location) {
  let list = priorities.map((v, i) => ({
    my: i == location,
    val: v,
  }));

  let cnt = 0;
  while (true) {
    let cur = list.shift();

    if (list.some((v) => v.val > cur.val)) list.push(cur);
    else {
      ++cnt;
      if (cur.my) break;
    }
  }

  return cnt;
}
