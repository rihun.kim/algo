function solution(n, computers) {
  let check = Array.from({ length: n }, () => false);
  let ans = 0;

  function DFS(index) {
    check[index] = true;

    for (let i = 0; i < n; ++i) {
      if (!check[i] && computers[index][i] == 1) {
        DFS(i);
      }
    }
  }

  for (let i = 0; i < n; ++i) {
    if (!check[i]) {
      DFS(i);
      ++ans;
    }
  }

  return ans;
}
