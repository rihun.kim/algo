// 일단 작업에 소요되는 시간이 적은일 부터 작업이 수행되도록 해야겠다고 생각했다.
// 우선순위 큐를 활용해 작업 수행시간을 기준으로 오름차순으로 정렬되도록 풀어보았다.
// 하지만 수행해보니 1개를 제외하고 모든 테스트케이스에서 실패했다.
// 제한사항 중에
// 하드디스크가 작업을 수행하고 있지 않을 때에는 [ 먼저 요청 ] 이 들어온 작업부터 처리합니다.
// 이 제한사항도 고려해야한다!!
// 따라서 정렬된 이후에 요청시간과 현재시간을 비교해 주어야하고
// 현재시간보다 요청시간이 이후라면 현재시간을 늘려준다.

function solution(jobs) {
  let n = jobs.length;
  let befJob = jobs.shift();
  jobs = jobs.map((v, i) => [v[0], v[1], i]);
  jobs.sort((a, b) => {
    if (a[0] == b[0]) return a[2] - b[2];
    else return a[1] - b[1];
  });

  let ans = befJob[1] - befJob[0];
  let finTime = befJob[0] + befJob[1];
  while (jobs.length != 0) {
    let i, nextJob;
    for (i = 0; i < jobs.length; ++i) if (finTime >= jobs[i][0]) break;

    if (i == jobs.length) nextJob = jobs.shift();
    else nextJob = jobs.splice(i, 1)[0];

    if (nextJob[0] < finTime) {
      finTime += nextJob[1];
    } else {
      finTime = finTime + nextJob[0] - finTime + nextJob[1];
    }

    ans = ans + finTime - nextJob[0];
    befJob = [...nextJob];
  }

  return Math.floor(ans / n);
}

// 다시 풀어본 방식
// 이번에는 우선순위를 정할 때,
// 특정시간을 기준으로 들어와 있는 녀석들을 선별한 후
// 그 녀석들에서 주어진 일의 양적 시간을 오름차순으로 풀었다.
// 이전에 풀었던 방식의 맹점은 현재 시간에 대한 고려가 없었고
// 무조건 짧은 시간으로 오름차순 시켜 둔채 풀었기 떄문에 문제였다.
// 그 방식의 문제는 아래와 같이 소팅 시켜두고 풀기 때문이다.
// [0,3] [7,1] [4,2]
// [0,3] 하고, 그 다음 [7,1] 을 풀기 때문에 문제가 되는 것이다.
// 시간이 더 길게 나오게 되는 것이다.
// 올바르게 하는 것은
// [0,3] [4,2] [7,1] 이런 식으로 풀어야 한다.
function prioritySort(jobs, time) {
  jobs.sort((a, b) => {
    if (a[0] <= time && b[0] <= time) {
      return a[1] - b[1];
    } else if (a[0] <= time) {
      return -1;
    } else {
      return 1;
    }
  });

  return jobs;
}

function solution(jobs) {
  let n = jobs.length;
  jobs = prioritySort(jobs, 0);

  let time = 0;
  let ans = 0;
  while (jobs.length != 0) {
    if (time < jobs[0][0]) {
      ++time;
    } else {
      let work = jobs.shift();
      ans = ans + time - work[0] + work[1];
      time = time + work[1];
    }
    jobs = prioritySort(jobs, time);
  }

  return parseInt(ans / n);
}
