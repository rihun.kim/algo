function solution(board) {
  const N = board.length;
  const DY = [1, 0, -1, 0];
  const DX = [0, 1, 0, -1];
  const Q = [];
  const costArr = new Array(N);

  for (let i = 0; i < N; ++i) {
    costArr[i] = new Array(N);
    for (let j = 0; j < N; ++j) {
      costArr[i][j] = -1;
    }
  }

  Q.push([0, 0, 0, 0], [0, 0, 0, 1]);
  costArr[0][0] = 0;

  while (Q.length > 0) {
    const [y, x, cost, way] = Q.shift();

    for (let k = 0; k < 4; ++k) {
      const nextY = y + DY[k];
      const nextX = x + DX[k];

      if (nextY < 0 || nextY >= N || nextX < 0 || nextX >= N) continue;
      if (board[nextY][nextX] == 1) continue;
      if (Math.abs(way - k) == 2) continue; // back way check

      const money = way == k ? 100 : 600; // 갱신되고자 하는 비용
      const prevCost = costArr[nextY][nextX];
      if (prevCost == -1 || prevCost >= cost + money) {
        costArr[nextY][nextX] = cost + money;
        Q.push([nextY, nextX, cost + money, k]);
      }
    }
  }

  return costArr[N - 1][N - 1];
}

function s(board) {
  const N = board.length;
  const DI = [1, 0, -1, 0];
  const DJ = [0, 1, 0, -1];
  const Q = [];
  const dist = new Array(N);
  for (let i = 0; i < N; i++) {
    dist[i] = new Array(N);
    for (let j = 0; j < N; j++) {
      dist[i][j] = -1;
    }
  }

  Q.push([0, 0, 0, 0], [0, 0, 0, 1]);
  dist[0][0] = 0;

  while (Q.length > 0) {
    const [i, j, c, d] = Q.shift();

    for (let k = 0; k < 4; k++) {
      const ni = i + DI[k];
      const nj = j + DJ[k];
      // 경계 체크
      if (ni < 0 || nj < 0 || ni >= N || nj >= N) continue;

      // 벽 체크
      if (board[ni][nj] === 1) continue;

      // 뒤로가기 체크
      if (Math.abs(d - k) === 2) continue;

      const cost = d === k ? 100 : 600;
      const target = dist[ni][nj];
      if (target === -1 || target >= c + cost) {
        dist[ni][nj] = c + cost;
        Q.push([ni, nj, c + cost, k]);
      }
    }
  }

  return dist[N - 1][N - 1];
}

solution([
  [0, 0, 0],
  [0, 0, 0],
  [0, 0, 0],
]);
