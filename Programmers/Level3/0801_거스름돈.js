// DP 문제
// DP 의 핵심은 규칙을 찾는 것!

// 단순 루프 두개는 시간초과
function solution(n, money) {
  let mod = 1000000007;
  let d = [Array.from({ length: n + 1 }, () => 0)];
  d[0][0] = 1;

  for (let cnt = 1; cnt <= money.length; ++cnt) {
    d[cnt] = [1];
    for (let cost = 1; cost <= n; ++cost) {
      if (cost < money[cnt - 1]) {
        d[cnt][cost] = d[cnt - 1][cost] % mod;
      } else {
        d[cnt][cost] = (d[cnt - 1][cost] % mod) + (d[cnt][cost - money[cnt - 1]] % mod);
      }
    }
  }

  return d[money.length].pop();
}

function solution(n, money) {
  let mod = 1000000007;
  let d = Array.from({ length: n + 1 }, () => 0);
  d[0] = 1;

  for (let cnt = 1; cnt <= money.length; ++cnt) {
    for (let cost = 1; cost <= n; ++cost) {
      if (cost < money[cnt - 1]) {
        d[cost] = d[cost] % mod;
      } else {
        d[cost] = (d[cost] % mod) + (d[cost - money[cnt - 1]] % mod);
      }
    }
  }

  return d[n];
}
