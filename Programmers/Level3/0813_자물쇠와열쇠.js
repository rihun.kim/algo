// https://ai-information.blogspot.com/2019/11/2020-kakao-blind-recruitment_6.html

function solution(key, lock) {
  // 2 차원 배열 회전 방법 잘 알아두기 makeM !!
  // 회전식에도 규칙이 존재
  let len = key.length;
  let rotates = [[...key]];
  for (let i = 1; i < 4; ++i) {
    rotates[i] = makeM(len);
    for (let y = 0; y < len; ++y) {
      for (let x = 0; x < len; ++x) {
        rotates[i][y][x] = rotates[i - 1][x][len - y - 1];
      }
    }
  }

  // 이런 문제는 패딩을 만들어 푼다.
  let board = [...makeBoard(lock, key.length, lock.length)];

  let M = key.length;
  let N = lock.length;
  let tag = true;

  // 비교할때, gap 이라는 것을 통한 센스
  for (let rotate of rotates) {
    for (let gap1 = 0; gap1 < M + N - 1; ++gap1) {
      for (let gap2 = 0; gap2 < M + N - 1; ++gap2) {
        tag = true;
        let tempBoard = JSON.stringify(board);
        tempBoard = JSON.parse(tempBoard);
        for (let y = 0; y < M; ++y) {
          for (let x = 0; x < M; ++x) {
            tempBoard[gap1 + y][gap2 + x] += rotate[y][x];
          }
        }

        for (let i = M - 1; i < M + N - 1; ++i) {
          for (let j = M - 1; j < M + N - 1; ++j) {
            if (tempBoard[i][j] != 1) {
              tag = false;
              break;
            }
          }
        }

        if (tag) break;
      }
      if (tag) break;
    }
    if (tag) break;
  }

  return tag;

  function makeM(M) {
    return Array.from({ length: M }, () => Array.from({ length: M }, () => 0));
  }

  function makeBoard(lock, M, N) {
    let len = 2 * M + N - 2;
    let board = Array.from({ length: len }, () => Array.from({ length: len }, () => 0));

    for (let y = 0; y < lock.length; ++y) {
      for (let x = 0; x < lock.length; ++x) {
        board[M - 1 + y][M - 1 + x] = lock[y][x];
      }
    }

    return board;
  }
}

solution(
  [
    [0, 0, 0],
    [1, 0, 0],
    [0, 1, 1],
  ],
  [
    [1, 1, 1],
    [1, 1, 0],
    [1, 0, 1],
  ]
);
