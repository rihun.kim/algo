// 백트래킹 문제
// 백트래킹은 재귀함수를 반드시 짜서 푼다
// 별 테크닉이 없다.
// 그냥 재귀함수 짠다.

function solution(n) {
  let ans = 0;
  for (let i = 0; i < n; ++i) {
    let cols = [i];
    queen(cols);
  }

  return ans;

  function isPromising(cols, i) {
    let tag = true;
    for (let j = cols.length - 1, t = 1; j >= 0; --j, ++t) {
      if (cols[j] + t == i || cols[j] - t == i) {
        tag = false;
        break;
      }
    }

    return tag;
  }

  function queen(cols) {
    if (cols.length == n) {
      ++ans;
    } else {
      for (let i = 0; i < n; ++i) {
        if (!cols.includes(i)) {
          if (isPromising(cols, i)) {
            cols.push(i);
            queen(cols);
            cols.pop();
          }
        }
      }
    }
  }
}
