// 갯수가 적으므로
// 완전탐색 문제
// 완전탐색은 DFS 를 주로 이용한다.
// 에러를 못찾겠다..

function solution(user_id, banned_id) {
  let map = new Map();
  let check = Array.from({ length: user_id.length }, () => false);

  dfs(user_id, banned_id, [...check], 0, []);
  console.log(Object.keys(map));

  function dfs(users, bans, check, bIdx, ans) {
    if (bIdx == bans.length) {
      map["" + ans.sort()] = true;
      return;
    }

    // console.log(ans, check);

    for (let i = 0; i < users.length; ++i) {
      if (check[i]) continue;
      if (bans[bIdx].length != users[i].length) continue;

      let j = 0;
      for (j = 0; j < users[i].length; ++j) {
        if (bans[bIdx][j] == "*") continue;
        if (bans[bIdx][j] != users[i][j]) break;
      }

      if (j == users[i].length) {
        // console.log(users[i], i, ans, check[3]);
        check[i] = true;

        ans.push(users[i]);
        dfs(users, bans, [...check], bIdx + 1, ans);
        ans.pop();
        // check[i] = false;
      }
    }
  }
}

// solution(["frodo", "fradi", "crodo", "abc123", "frodoc"], ["fr*d*", "abc1**"]);

// solution(["frodo", "fradi", "crodo", "abc123", "frodoc"], ["*rodo", "*rodo", "******"]);

solution(["frodo", "fradi", "crodo", "abc123", "frodoc"], ["fr*d*", "******", "*rodo", "******"]);
