function solution(genres, plays) {
  let map = new Map();
  let cnt = new Map();

  for (let i = 0; i < genres.length; ++i) {
    if (cnt[genres[i]] == undefined) cnt[genres[i]] = plays[i];
    else cnt[genres[i]] += plays[i];

    if (map[genres[i]] == undefined) map[genres[i]] = [];
    map[genres[i]].push([plays[i], i]);
  }

  let arr = [];
  for (let key in cnt) arr.push([key, cnt[key]]);
  arr.sort((a, b) => b[1] - a[1]);

  let ans = [];
  for (let song of arr) {
    map[song[0]].sort((a, b) => b[0] - a[0]);
    ans.push(map[song[0]][0][1]);
    if (map[song[0]][1] != undefined) ans.push(map[song[0]][1][1]);
  }

  return ans;
}
