function solution(user_id, banned_id) {
  let ansMap = new Map();
  dfs(user_id, [], banned_id);
  let ans = 0;
  for (let key in ansMap) {
    ++ans;
  }

  console.log(ans);
  return ans;

  function dfs(source, combi, ban) {
    if (ban.length == 0) {
      ansMap[JSON.stringify(combi.sort())] = true;
      return;
    }

    for (let idx = 0; idx < source.length; ++idx) {
      let k, i, flag;
      for (k = 0; k < ban.length; ++k) {
        if (source[idx].length != ban[k].length) continue;

        for (i = 0; i < ban[k].length; ++i) {
          if (ban[k][i] == "*") continue;
          if (ban[k][i] != source[idx][i]) {
            break;
          }
        }

        if (i == ban[k].length) {
          flag = true;
          break;
        }
      }

      if (flag) {
        let temp = [...source];
        let ans = temp.splice(idx, 1);

        let temp2 = [...ban];
        temp2.splice(k, 1);

        combi.push(...ans);
        dfs([...temp], [...combi], [...temp2]);
        combi.pop();
      }
    }
  }
}
