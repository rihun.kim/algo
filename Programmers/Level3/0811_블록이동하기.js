function solution(board) {
  let N = board.length - 1;
  go(board, 0, 0, 0, 1, board, 0);

  function go(board, y1, x1, y2, x2, visited, count) {
    console.log("[", y1, x1, "]", "[", y2, x2, "]");

    if ((y1 == N && x1 == N) || (y2 == N && x2 == N)) {
      console.log(count);
      return;
    }

    let normalY = [0, 0, -1, 1];
    let normalX = [-1, 1, 0, 0];
    let rotateY = [
      [y1, y1 + 1],
      [y1, y1 - 1],
      [y2 - 1, y2],
      [y2 + 1, y2],
    ];
    let rotateX = [
      [x1, x1],
      [x1, x1],
      [x2, x2],
      [x2, x2],
    ];
    let exam = [
      [y1 + 1, x2],
      [y1 - 1, x2],
      [y2 - 1, x1],
      [y2 + 1, x1],
    ];

    // normal
    for (let i = 0; i < 4; ++i) {
      let nY1 = y1 + normalY[i];
      let nY2 = y2 + normalY[i];
      let nX1 = x1 + normalX[i];
      let nX2 = x2 + normalX[i];

      if (0 > nY1 || nY1 > N || 0 > nY2 || nY2 > N || 0 > nX1 || nX1 > N || 0 > nX2 || nX2 > N)
        continue;
      if (visited[nY1][nX1] || visited[nY2][nX2]) continue;

      visited[nY1][nX1] = 1;
      visited[nY2][nX2] = 1;
      go(board, nY1, nX1, nY2, nX2, visited, count + 1);
      visited[nY1][nX1] = 0;
      visited[nY2][nX2] = 0;
    }

    // rotate
    for (let i = 0; i < 4; ++i) {
      let eY = exam[i][0];
      let eX = exam[i][1];
      if (0 > eY || eY > N || 0 > eX || eX > N) continue;
      if (board[exam[i][0]][exam[i][1]] == 1) continue;

      let nY1 = rotateY[i][0];
      let nY2 = rotateY[i][1];
      let nX1 = rotateX[i][0];
      let nX2 = rotateX[i][1];

      if (0 > nY1 || nY1 > N || 0 > nY2 || nY2 > N || 0 > nX1 || nX1 > N || 0 > nX2 || nX2 > N)
        continue;
      if (visited[nY1][nX1] || visited[nY2][nX2]) continue;

      visited[nY1][nX1] = 1;
      visited[nY2][nX2] = 1;
      go(board, nY1, nX1, nY2, nX2, visited, count + 1);
      visited[nY1][nX1] = 0;
      visited[nY2][nX2] = 0;
    }
  }
}

solution([
  [0, 0, 0, 1, 1],
  [0, 0, 0, 1, 0],
  [0, 1, 0, 1, 1],
  [1, 1, 0, 0, 1],
  [0, 0, 0, 0, 0],
]);
