function solution(n, works) {
  works.sort((a, b) => b - a);

  while (n > 0) {
    let temp = works.shift();

    --temp;
    --n;
    if (temp <= 0) {
      works.push(0);
      continue;
    }

    let tag = true;
    for (let i = 0; i < works.length; ++i) {
      if (temp > works[i]) {
        tag = false;
        works.splice(i, 0, temp);
        break;
      }
    }

    if (tag) works.push(temp);
  }

  let ans = 0;
  works.map((v) => {
    ans += v ** 2;
  });

  return ans;
}
