function solution(n, t, m, timetable) {
  let people = timetable.map((v) => {
    let temp = v.split(":");
    return [+temp[0], +temp[1]];
  });

  people.sort((a, b) => {
    if (a[0] == b[0]) {
      return a[1] - b[1];
    } else {
      return a[0] - b[0];
    }
  });

  let bus = [];
  for (let i = 0; i < n; ++i) {
    let base = t * i;
    let HH = 9 + parseInt(base / 60);
    let MM = base % 60;
    bus.push([HH, MM]);
  }

  let ans = [];
  for (let i = 0; i < n; ++i) {
    // bus
    let cnt = 0;
    let lastPerson;
    let take = true;
    while (people.length != 0) {
      let person = people.shift();
      lastPerson = person;

      if (person[0] < bus[i][0]) {
        console.log("버스탓어요. 버스시간:", bus[i], " 버스탄놈:", person[0], person[1]);
        ++cnt;
      } else if (person[0] == bus[i][0] && person[1] <= bus[i][1]) {
        console.log("버스탓어요. 버스시간:", bus[i], " 버스탄놈:", person[0], person[1]);
        ++cnt;
      } else {
        // 못타는 경우
        people.unshift(person);
        take = false;
        break;
      }

      if (cnt == m) break;
    }

    if (!take) {
      if (i == n - 1) {
        console.log("막차인데 사람들이 못타서 자리가 남은 상황, 콘은 버스시간에만 오면 됨");
        ans = [...bus[i]];
      }
    } else if (i == n - 1 && cnt == m) {
      console.log("막차까지 사람들이 꽉찬 경우");
      if (lastPerson[1] == 0) {
        ans = [lastPerson[0] - 1, 59];
      } else {
        ans = [lastPerson[0], lastPerson[1] - 1]; // 분이 00 일 경우 처리할 것
      }
    } else if (i == n - 1) {
      console.log("막차에 자리가 남은 경우");
      ans = [...bus[i]];
    }
  }

  let hour = "" + ans[0];
  let min = "" + ans[1];
  hour = hour.length == 1 ? "0" + hour : hour;
  min = min.length == 1 ? "0" + min : min;

  console.log(hour + ":" + min);
  return hour + ":" + m