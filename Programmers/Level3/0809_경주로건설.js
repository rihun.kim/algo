function solution(board) {
  const dCost = 100;
  const cCost = 500;
  const N = board.length;

  let check = [...board].map((v) => v.map((v) => false));
  check[0][0] = true;

  let cost = 0;
  let queue = [[0, 0, 0, [[0, 0]]]];
  let ans = Infinity;

  bfs(check, cost, queue);
  // console.log(ans);
  return ans;

  function bfs(check, cost, queue) {
    let moveX = [0, 1, -1];
    let moveY = [1, 0, 0];

    while (queue.length) {
      let point = queue.shift();

      if (point[0] == N - 1 && point[1] == N - 1) {
        ans = Math.min(point[2], ans);
      }

      for (let i = 0; i < 4; ++i) {
        let Y = point[0] + moveY[i];
        let X = point[1] + moveX[i];

        let flag = false;
        for (let po of point[3]) {
          if (Y == po[0] && X == po[1]) {
            flag = true;
            break;
          }
        }
        if (flag) continue;

        if (0 <= Y && Y < N && 0 <= X && X < N && board[Y][X] == 0) {
          let arr = [...point[3]];
          let price = point[2];
          if (arr.length >= 2) {
            let prev = arr[arr.length - 2];
            if (prev[0] != Y && prev[1] != X) {
              price += cCost;
            }
          }
          price += dCost;

          arr.push([Y, X]);
          let temp = [Y, X, price, [...arr]];

          queue.push(temp);
        }
      }
    }
  }
}

solution([
  [0, 0, 0, 0, 0, 0, 0, 1],
  [0, 0, 0, 0, 0, 0, 0, 0],
  [0, 0, 0, 0, 0, 1, 0, 0],
  [0, 0, 0, 0, 1, 0, 0, 0],
  [0, 0, 0, 1, 0, 0, 0, 1],
  [0, 0, 1, 0, 0, 0, 1, 0],
  [0, 1, 0, 0, 0, 1, 0, 0],
  [1, 0, 0, 0, 0, 0, 0, 0],
]);

solution([
  [0, 0, 1, 0],
  [0, 0, 0, 0],
  [0, 1, 0, 1],
  [1, 0, 0, 0],
]);

solution([
  [0, 0, 0, 0, 0, 0],
  [0, 1, 1, 1, 1, 0],
  [0, 0, 1, 0, 0, 0],
  [1, 0, 0, 1, 0, 1],
  [0, 1, 0, 0, 0, 1],
  [0, 0, 0, 0, 0, 0],
]);

solution([
  [0, 0, 0],
  [0, 0, 0],
  [0, 0, 0],
]);
