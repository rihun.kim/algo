// DFS 문제로써, DFS 테크닉을 잘 연마할 것

let tot = [];

function recurv(map, from, ans) {
  if (tot.length != 0) return;
  if (map[from] == undefined || map[from].length == 0) {
    let tag = true;
    for (let city in map) {
      if (map[city].length != 0) {
        tag = false;
        break;
      }
    }
    if (tag && tot.length == 0) tot = [...ans];
    return;
  }

  let len = from.length;
  for (let i = 0; i < len; ++i) {
    let city = map[from].shift();
    ans.push(city);
    recurv(map, city, ans);
    map[from].push(city);
    ans.pop(city);
  }
}

function solution(tickets) {
  let map = new Map();
  for (let ticket of tickets) {
    if (!map[ticket[0]]) map[ticket[0]] = [ticket[1]];
    else map[ticket[0]].push(ticket[1]);
  }

  for (let key in map) map[key].sort((a, b) => a.localeCompare(b));

  let ans = ["ICN"];
  recurv(map, "ICN", ans);

  return tot;
}

///
/// DFS 좋은 재귀적 문법 코드
function solution(tickets) {
  let answer = [];

  DFS(tickets, "ICN", ["ICN"]);
  return answer.sort()[0];

  function DFS(tickets, start, str) {
    if (tickets.length == 0) answer.push(str);

    // 재귀에서 핵심은 for 문을 먼저 짜는 것
    for (let i in tickets) {
      if (tickets[i][0] == start) {
        let temp = [...tickets]; // 복사본 제작
        temp.splice(i, 1); // 없애고
        let temp2 = [...str, tickets[i][1]]; // 정답 복사본 제작
        DFS(temp, tickets[i][1], temp2);
      }
    }
  }
}
