function solution(word, pages) {
  let keywordReg = new RegExp("[^A-Za-z]" + word + "[^A-Za-z]", "gi");

  let map = new Map();

  let index = 0;
  for (let page of pages) {
    const name = page.split('content="')[1].split('"/>')[0];
    const basic = page.match(keywordReg);
    map[name] = { basicPoint: basic == null ? 0 : basic.length };

    let arr = [];
    let linkSites = page.split('a href="');
    for (let i = 1; i < linkSites.length; ++i) {
      arr.push(linkSites[i].split('">')[0]);
    }

    map[name].index = index++;
    map[name].link = [...arr];
    map[name].linkCnt = arr.length;
    map[name].matchPoint = map[name].basicPoint;
  }

  for (let key in map) {
    if (map[key].link.length != 0) {
      for (let link of map[key].link) {
        if (map[link] != undefined) map[link].matchPoint += map[key].basicPoint / map[key].linkCnt;
      }
    }
  }

  let max = [0, 0];
  for (let key in map) {
    if (map[key].matchPoint > max[1]) {
      max = [map[key].index, map[key].matchPoint];
    }
  }

  console.log(max[0]);
  return max[0];
}

solution("Muzi", [
  '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://careers.kakao.com/interview/list"/>\n</head>  \n<body>\n<a href="https://programmers.co.kr/learn/courses/4673"></a>#!MuziMuzi!)jayg07con&&\n\n</body>\n</html>',
  '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://www.kakaocorp.com"/>\n</head>  \n<body>\ncon%\tmuzI92apeach&2<a href="https://hashcode.co.kr/tos"></a>\n\n\t^\n</body>\n</html>',
]);

// solution("blind", [
//   '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://a.com"/>\n</head>  \n<body>\nBlind Lorem Blind ipsum dolor Blind test sit amet, consectetur adipiscing elit. \n<a href="https://b.com"> Link to b </a>\n</body>\n</html>',
//   '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://b.com"/>\n</head>  \n<body>\nSuspendisse potenti. Vivamus venenatis tellus non turpis bibendum, \n<a href="https://a.com"> Link to a </a>\nblind sed congue urna varius. Suspendisse feugiat nisl ligula, quis malesuada felis hendrerit ut.\n<a href="https://c.com"> Link to c </a>\n</body>\n</html>',
//   '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://c.com"/>\n</head>  \n<body>\nUt condimentum urna at felis sodales rutrum. Sed dapibus cursus diam, non interdum nulla tempor nec. Phasellus rutrum enim at orci consectetu blind\n<a href="https://a.com"> Link to a </a>\n</body>\n</html>',
// ]);
