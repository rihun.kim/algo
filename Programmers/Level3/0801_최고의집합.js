// 쉬운 수학문제
// 집합에서 곱셈의 값이 커질려면
// 최대한 원소들의 값이 균등 분배되어야 한다는 특성을 이용해서 풀면됨

function solution(n, s) {
  if (n > s) return [-1];

  let ini = parseInt(s / n);
  let set = Array.from({ length: n }, () => ini);

  s -= ini * n;
  for (let i = 0; s > 0; --s, ++i) ++set[i % n];

  return set.sort((a, b) => a - b);
}
