// 배수문제
// 다시 풀어보자
// 가장 약한 문제 유형
// 진민견과 함꼐 다시 풀것

function fac(n) {
  if (n == 0) return 1;
  return n * fac(n - 1);
}

function solution(n, k) {
  let i = 1;
  let arr = Array.from({ length: n }, () => i++);

  let ans = [];
  --k;
  while (n > 0) {
    let index = parseInt(k / fac(n - 1));
    console.log("k=", k, "n=", n, "arr=", arr, "idx=", index, "val=", arr[index]);
    ans.push(arr[index]);
    arr.splice(index, 1);
    k = k % fac(n - 1);

    --n;
  }

  return ans;
}
