// 그래프 문제
// 그리디를 이용한 최소 스패닝 트리 찾는 문제
// 그리디 이므로, 오름차순 배치해서
// 가장 낮은 값만 시도 한다.
// 최소 스패닝 트리는 그리디 + n-1 의 선으로 모든 노드를 잇는 것
// 따라서 여기에는 크루스칼과 프림 알고리즘이 존재
// 프림을 쓰려면 unionFind 을 써야함
// unionFind 는 비교하는 노드들의 부모가 서로 같은 지 판단하는 로직
// 같은 부모면 패쓰해야함, 서로 연결하면 싸이클 발생(필요없는 간선 생김)
// 계속 자신의 부모노드를 변경시켜주면서 알고리즘 진행

//https://gmlwjd9405.github.io/2018/08/29/algorithm-kruskal-mst.html
//https://gmlwjd9405.github.io/2018/08/31/algorithm-union-find.html

function solution(n, costs) {
  costs.sort((a, b) => a[2] - b[2]);

  let i = 0;
  let d = Array.from({ length: n }, () => i++);
  let ans = 0;

  for (let cost of costs) {
    let start = unionFind(cost[0]);
    let end = unionFind(cost[1]);
    let val = cost[2];

    if (start != end) {
      d[start] = end;
      ans += val;
    }
  }

  return ans;

  function unionFind(node) {
    if (node == d[node]) return node;
    return (d[node] = unionFind(d[node]));
  }
}
