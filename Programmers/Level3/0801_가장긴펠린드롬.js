function solution(s) {
  let ans = 1;

  for (let i = 0; i < s.length - 2; ++i) {
    if (s[i] == s[i + 1]) {
      for (let l = i - 1, r = i + 2, len = 4; ; --l, ++r) {
        if (l < 0 || r >= s.length) {
          ans = Math.max((len -= 2), ans);
          break;
        }

        if (s[l] == s[r]) {
          len += 2;
          continue;
        } else {
          ans = Math.max((len -= 2), ans);
          break;
        }
      }
    }

    if (s[i] == s[i + 2]) {
      for (let l = i, r = i + 2, len = 1; ; --l, ++r, len += 2) {
        if (l < 0 || r >= s.length) {
          ans = Math.max(len, ans);
          break;
        }

        if (s[l] == s[r]) {
          continue;
        } else {
          ans = Math.max(len, ans);
          break;
        }
      }
    }
  }

  return ans;
}

// JS 니까 간단하게 푸는 법
// 회문이므로, 그냥 자른만큼 리버싱 시켜서 비교하는 로직이
// 세상 간단함 ===> 하지만...... 시간초과 나온다..ㅋㅋㅋㅋ
// 그러니 위에 방식으로 풀어야하거나..
// 리버스를 하나하나 비교하면 된다고 하는데... 띠용?
// 애시당초 이처럼 풀기를 원하지 않은 문제인듯.
function solution(s) {
  let ans = 1;

  for (let i = 0; i < s.length; ++i) {
    for (let j = i; j <= s.length; ++j) {
      let oriText = s.substring(i, j);
      let revText = oriText.split("").reverse().join("");
      if (oriText == revText) ans = Math.max(ans, oriText.length);
    }
  }

  return ans;
}
