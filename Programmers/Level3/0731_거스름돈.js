// 재귀로 푼 경우 => 시간초과
let ans = 0;
function recurv(remain, money, limit) {
  if (remain == 0) {
    ++ans;
    return;
  }

  for (let mon of money) {
    if (remain - mon >= 0 && limit >= mon) {
      recurv(remain - mon, money, mon);
    }
  }
}

function solution(n, money) {
  money.sort((a, b) => b - a);
  recurv(n, money, Infinity);

  return ans % 1000000007;
}
