function solution(board) {
  const N = board.length;
  let ans = N * N;
  let queue = [[0, 0, 0, 1, 0]];
  let visited = [];

  const checkVisited = (_y1, _x1, _y2, _x2) => {
    return visited.some(({ y1, x1, y2, x2 }) => x1 == _x1 && y1 == _y1 && x2 == _x2 && y2 == _y2);
  };

  const isSkip = (_y1, _x1, _y2, _x2) => {
    if (_y1 < 0 || _y1 >= N) return true;
    if (_x1 < 0 || _x1 >= N) return true;
    if (_y2 < 0 || _y2 >= N) return true;
    if (_x2 < 0 || _x2 >= N) return true;
    if (board[_y1][_x1] == 1 || board[_y2][_x2]) return true;
    if (checkVisited(_y1, _x1, _y2, _x2)) return true;
  };

  const isDone = (_y1, _x1, _y2, _x2) => {
    if (_x1 == N - 1 && _y1 == N - 1) return true;
    if (_x2 == N - 1 && _y2 == N - 1) return true;
    return false;
  };

  while (queue.length > 0) {
    const item = queue.shift();
    let [y1, x1, y2, x2, cnt] = item;

    if (isSkip(y1, x1, y2, x2)) continue;

    if (isDone(y1, x1, y2, x2)) {
      ans = Math.min(ans, cnt);
      continue;
    }

    visited.push({ y1, x1, y2, x2 });
    queue.push([y1, x1 + 1, y2, x2 + 1, cnt + 1]); // right
    queue.push([y1, x1 - 1, y2, x2 - 1, cnt + 1]); // left
    queue.push([y1 - 1, x1, y2 - 1, x2, cnt + 1]); // up
    queue.push([y1 + 1, x1, y2 + 1, x2, cnt + 1]); // down

    if (y1 == y2 && y1 < N - 1 && y2 < N - 1) {
      // horizon down
      if (board[y1 + 1][x1] == 0 && board[y2 + 1][x2] == 0) {
        queue.push([y1, x1, y1 + 1, x1, cnt + 1]);
        queue.push([y2, x2, y2 + 1, x2, cnt + 1]);
      }
    }
    if (y1 == y2 && y1 > 0 && y2 > 0) {
      // horizon up
      if (board[y1 - 1][x1] == 0 && board[y2 - 1][x2] == 0) {
        queue.push([y2 - 1, x2, y2, x2, cnt + 1]);
        queue.push([y1, x1, y1 - 1, x1, cnt + 1]);
      }
    }
    if (x1 == x2 && x1 < N - 1 && x2 < N - 1) {
      // vertical right
      if (board[y1][x1 + 1] == 0 && board[y2][x2 + 1] == 0) {
        queue.push([y1, x1, y1, x1 + 1, cnt + 1]);
        queue.push([y2, x1 + 1, y2, x2, cnt + 1]);
      }
    }
    if (x1 == x2 && x1 > 0 && x2 > 0) {
      // vertical left
      if (board[y1][x1 - 1] == 0 && board[y2][x2 - 1] == 0) {
        queue.push([y1, x1, y1, x1 - 1, cnt + 1]);
        queue.push([y2, x1 - 1, y2, x2, cnt + 1]);
      }
    }
  }

  return ans;
}
