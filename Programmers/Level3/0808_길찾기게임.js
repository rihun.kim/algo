function Node(i, x, y) {
  this.val = i;
  this.x = x;
  this.y = y;
  this.left = null;
  this.right = null;
}

function addNode(parent, child) {
  if (parent.x > child.x) {
    if (parent.left == null) parent.left = child;
    else addNode(parent.left, child);
  } else {
    if (parent.right == null) parent.right = child;
    else addNode(parent.right, child);
  }
}

function solution(nodeinfo) {
  let arr = [];
  for (let [index, node] of nodeinfo.entries()) arr.push(new Node(+index + 1, node[0], node[1]));
  arr.sort((a, b) => (a.y == b.y ? a.x - b.x : b.y - a.y));
  for (let i = 1; i < arr.length; ++i) addNode(arr[0], arr[i]);

  let pre = [];
  preOrder(arr[0]);
  let post = [];
  postOrder(arr[0]);
  let ans = [pre, post];

  return ans;

  function preOrder(root) {
    pre.push(root.val);
    root.left != undefined ? preOrder(root.left) : null;
    root.right != undefined ? preOrder(root.right) : null;
  }

  function postOrder(root) {
    root.left != undefined ? postOrder(root.left) : null;
    root.right != undefined ? postOrder(root.right) : null;
    post.push(root.val);
  }
}
