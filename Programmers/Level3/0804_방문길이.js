function solution(dirs) {
  let map = new Map();

  let ans = 0;
  let prev = [0, 0];
  let point = [0, 0]; // y, x
  for (let dir of dirs) {
    if (dir == "U") ++point[0] > 5 ? --point[0] : null;
    else if (dir == "D") --point[0] < -5 ? ++point[0] : null;
    else if (dir == "R") ++point[1] > 5 ? --point[1] : null;
    else --point[1] < -5 ? ++point[1] : null;

    if (prev[0] == point[0] && prev[1] == point[1]) continue;
    if (map[JSON.stringify(prev) + JSON.stringify(point)] == undefined) {
      map[JSON.stringify(prev) + JSON.stringify(point)] = true;
      map[JSON.stringify(point) + JSON.stringify(prev)] = true;
      ++ans;
    }

    prev = [...point];
  }

  return ans;
}
