function solution(N, road, K) {
  let map = new Map();

  for (let rod of road) {
    if (map[rod[0]]) map[rod[0]].push([rod[1], rod[2]]);
    else map[rod[0]] = [[rod[1], rod[2]]];

    if (map[rod[1]]) map[rod[1]].push([rod[0], rod[2]]);
    else map[rod[1]] = [[rod[0], rod[2]]];
  }

  for (let key in map) map[key].sort((a, b) => a[1] - b[1]);

  let dist = Array.from({ length: N + 1 }, () => 0);
  let check = Array.from({ length: N + 1 }, () => false);
  check[1] = true;

  bfs(1);
  let ans = 0;
  for (let i = 1; i < dist.length; ++i) {
    dist[i] <= K ? ans++ : null;
  }

  return ans;

  function bfs(start) {
    let queue = [start];

    while (queue.length != 0) {
      let temp = [];

      for (let qq of queue) {
        for (let edge of map[qq]) {
          if (!check[edge[0]] || dist[edge[0]] > dist[qq] + edge[1]) {
            check[edge[0]] = true;
            dist[edge[0]] = dist[qq] + edge[1];
            temp.push(edge[0]);
          }
        }
      }
      queue = [...temp];
    }
  }
}
