let ans = [];
function hanoi(n, from, temp, to) {
  if (n == 1) ans.push([from, to]);
  else {
    hanoi(n - 1, from, to, temp);
    ans.push([from, to]);
    hanoi(n - 1, temp, from, to);
  }
}

function solution(n) {
  hanoi(n, 1, 2, 3);

  return ans;
}
