function solution(begin, target, words) {
  let ans = Infinity;
  DFS(words, begin, []);

  function DFS(words, begin, str) {
    if (begin == target) {
      ans = ans > str.length ? str.length : ans;
    }

    for (let i = 0; i < words.length; ++i) {
      let cnt = 0;
      for (let j = 0; j < begin.length; ++j) {
        if (words[i][j] != begin[j]) {
          ++cnt;
          if (cnt >= 2) break;
        }
      }

      if (cnt == 1) {
        let temp = [...words];
        temp.splice(i, 1);
        let temp2 = [...str];
        temp2.push(words[i]);
        DFS(temp, words[i], temp2);
      }
    }
  }

  return ans == Infinity ? 0 : ans;
}
