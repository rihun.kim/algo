// 토크나이징 + 문자열 처리 + 센스있게 구간 잡는 법
// 에 관한 문제이고
// 한가지 방심한 것은 시분초가 나오면, 초로 나 환산시켜야 연산이 간편하다는 것이다.
// 그 다음에 풀땐, 그렇게 변환해서 하도록 하자.

function solution(lines) {
  let res = [];
  let ans = 0;

  for (let line of lines) {
    let data = line.split(" ");
    let [hh, mm, ss, sss] = data[1].split(new RegExp("[:.]"));
    let duration = data[2].split("s")[0];
    let tt, ttt;
    if (duration.includes(".")) [tt, ttt] = duration.split(".");
    else {
      tt = duration;
      ttt = "0";
    }

    res.push([getStartTime(+hh, +mm, +ss, +sss, +tt, +ttt), [+hh, +mm, +ss, +sss]]);
  }

  for (let i = 0; i < res.length; ++i) {
    let endTime = getEndTime(res[i][1][0], res[i][1][1], res[i][1][2], res[i][1][3]);

    let cnt = 1;
    for (let j = i + 1; j < res.length; ++j) {
      if (isEndTimeBigger(res[j][0], endTime)) ++cnt;
    }
    ans = Math.max(ans, cnt);
  }

  return ans;

  function isEndTimeBigger(a, end) {
    if (a[0] < end[0]) return true;
    if (a[0] == end[0]) {
      if (a[1] < end[1]) return true;
      if (a[1] == end[1]) {
        if (a[2] < end[2]) return true;
        if (a[2] == end[2]) {
          if (a[3] <= end[3]) return true;
        }
      }
    }
    return false;
  }

  function getEndTime(hh, mm, ss, sss) {
    --sss;
    if (sss < 0) {
      sss = 999;
      --ss;
    }

    ++ss;
    if (ss > 59) {
      ss = 0;
      ++mm;
      if (mm > 59) {
        mm = 0;
        ++hh;
        if (hh > 24) {
          hh = 24;
        }
      }
    }

    return [hh, mm, ss, sss];
  }

  function getStartTime(hh, mm, ss, sss, tt, ttt) {
    if (sss - ttt < 0) {
      --ss;
      sss = 1000 - ttt + sss + 1;
    } else {
      sss = sss - ttt + 1;
    }

    if (ss - tt < 0) {
      --mm;
      if (mm < 0) {
        mm = 59;
        --hh;
        if (hh < 0) {
          hh = 0;
        }
      }
      ss = 60 - tt + ss;
    } else {
      ss = ss - tt;
    }
    return [hh, mm, ss, sss];
  }
}

solution(["2016-09-15 01:00:04.001 2.0s", "2016-09-15 01:00:07.000 2s"]);

// solution(["2016-09-15 01:00:04.002 2.0s", "2016-09-15 01:00:07.000 2s"]);

// solution([
//   "2016-09-15 20:59:57.421 0.351s",
//   "2016-09-15 20:59:58.233 1.181s",
//   "2016-09-15 20:59:58.299 0.8s",
//   "2016-09-15 20:59:58.688 1.041s",
//   "2016-09-15 20:59:59.591 1.412s",
//   "2016-09-15 21:00:00.464 1.466s",
//   "2016-09-15 21:00:00.741 1.581s",
//   "2016-09-15 21:00:00.748 2.31s",
//   "2016-09-15 21:00:00.966 0.381s",
//   "2016-09-15 21:00:02.066 2.62s",
// ]);
