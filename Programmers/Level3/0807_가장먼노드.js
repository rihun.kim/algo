function solution(n, edge) {
  let map = new Map();

  for (let ed of edge) {
    if (map[ed[0]] == undefined) map[ed[0]] = [ed[1]];
    else map[ed[0]].push(ed[1]);

    if (map[ed[1]] == undefined) map[ed[1]] = [ed[0]];
    else map[ed[1]].push(ed[0]);
  }

  let check = Array.from({ length: n + 1 }, () => false);
  check[1] = true;
  let ans = 0;
  bfs(1);

  return ans;

  function bfs(start) {
    let queue = [start];
    while (queue.length != 0) {
      ans = queue.length;
      let temp = [];
      for (let q of queue) {
        for (let val of map[q]) {
          if (!check[val]) {
            check[val] = true;
            temp.push(val);
          }
        }
      }
      queue = [...temp];
    }
  }
}
