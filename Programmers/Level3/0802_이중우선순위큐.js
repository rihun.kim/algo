function solution(operations) {
  let queue = [];

  for (let op of operations) {
    let arr = op.split(" ");
    if (arr[0] == "I") queue.push(+arr[1]);
    else if (arr[1] == "1") queue.splice(queue.indexOf(Math.max(...queue)), 1);
    else if (arr[1] == "-1") queue.splice(queue.indexOf(Math.min(...queue)), 1);
  }

  if (queue.length == 0) return [0, 0];
  else return [Math.max(...queue), Math.min(...queue)];
}
