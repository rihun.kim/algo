// 이분탐색문제
// 이분탐색의 어려운 점은
// 알고리즘을 문제에 적용시킨다는 것이다.
// 딱 보면 이것이 이분탐색적 문제인지 알 수 가 없다.
// 그런데 대부분 힌트가 있다.
// 문제 자체는 무척 쉽게 보이지만, 연산 횟수가
// 겁내 많은 경우이다.
// 이 문제의 경우도 2억번까지의 숫자를 제시했다.
// 즉, 이분탐색을 암시하는 것이다.
// 따라서, 이분탐색을 해야한다.
// 이분탐색에서 중요한 것은 이분탐색의 대상을 선정하는 것이다.
// 무엇을 이분탐색화 할 것인가? 이다.
// 대부분 문제에서 풀어야 할 것을 제시한다.
// 그것이 이분탐색 대상이다.
// 따라서 1 부터 최대값 2억을 두고 이분탐색화 하면서
// 조건식을 잘 세우는 것으로 문제를 푼다.

function canGo(stones, md, k) {
  let no = 0;
  for (let i = 0; i < stones.length; ++i) {
    if (stones[i] < md) {
      no++;
    } else {
      no = 0;
    }

    if (no == k) {
      return false;
    }
  }
  return true;
}

function solution(stones, k) {
  let left = 1;
  let right = 200000000;

  while (left < right - 1) {
    let md = parseInt((left + right) / 2);

    if (canGo(stones, md, k)) {
      left = md;
    } else {
      right = md;
    }
  }

  return left;
}
