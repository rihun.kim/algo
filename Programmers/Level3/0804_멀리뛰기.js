function solution(n) {
  let d = [1, 1, 2];

  for (let i = 3; i <= n; ++i) {
    d[i] = (d[i - 1] + d[i - 2]) % 1234567;
  }

  return d[n] % 1234567;
}
