// 그리디
// 최대 승점을 얻어야함, 이기면 승점을 받음
// 이길 땐, 작은 폭으로 이기고, 질 땐, 큰폭으로 지면됨
// A 팀을 오름차순 소팅합니다. 1 3 5 7
// B 팀을 오름차순으로 소팅합니다. 2 2 6 8
// A 팀에게 B 팀을 큐잉합니다.
// B 팀의 멤버가 크면 A,B 팀 둘다 shift()
// B 팀의 멤버가 작으면, 어차피 이길 멤버가 없으므로 뺴서 버림
// 끝까지 도달하면, 그 뒤로부터는 계속 패함을 의미하므로
// break 해서 나옴

function solution(A, B) {
  A.sort((a, b) => a - b);
  B.sort((a, b) => a - b);

  let ans = 0;
  for (let a = 0, b = 0; a < A.length && b < B.length; ) {
    if (B[b] > A[a]) {
      ++b;
      ++a;
      ++ans;
    } else {
      ++b;
    }
  }

  return ans;
}
