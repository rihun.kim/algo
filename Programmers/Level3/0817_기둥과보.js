// 카카오가 얼마나 세상 악랄한 기업인지 앟 수 있는 문제ㅋㅋㅋ
// 블라인드로 뽑겠다면서, 정답률 1.9% 나오도록 문제난이도 조절 및 테스트 케이스 제공ㅋㅋㅋ

function solution(n, build_frame) {
  let answer = [];
  let bo = Array(n + 2)
    .fill(null)
    .map(() => Array());
  let pill = Array(n + 2)
    .fill(null)
    .map(() => Array());

  for (let [x, y, a, b] of build_frame) {
    if (b == 1) {
      // 설치
      if (a == 0) addPill(x, y, pill, bo);
      else addBo(x, y, pill, bo);
    } else if (b == 0) {
      // 삭제
      delFrame(x, y, a, pill, bo, n);
    }
  }

  for (let i = 0; i < n + 1; i++) {
    for (let j = 0; j < n + 1; j++) {
      if (pill[i][j]) answer.push([i, j, 0]);
      if (bo[i][j]) answer.push([i, j, 1]);
    }
  }

  return answer;

  ////////////
  function addPill(x, y, pill, bo) {
    // 지평선위에
    if (y == 0) {
      pill[x][y] = 1;
      return true;
    }

    // 다른 기둥위에
    if (y >= 1 && pill[x][y - 1]) {
      pill[x][y] = 1;
      return true;
    }
    // 왼쪽 보 위에
    if (x >= 1 && bo[x - 1][y]) {
      pill[x][y] = 1;
      return true;
    }
    // 현재 위치 보 위에
    if (bo[x][y]) {
      pill[x][y] = 1;
      return true;
    }
    return false;
  }

  ////////////
  function addBo(x, y, pill, bo) {
    // 밑에 기둥이 있는 경우
    if (y >= 1 && pill[x][y - 1]) {
      bo[x][y] = 1;
      return true;
    }
    // 오른쪽 밑에 기둥이 있는 경우
    if (y >= 1 && pill[x + 1][y - 1]) {
      bo[x][y] = 1;
      return true;
    }
    // 양 옆에 보가 있는 경우
    if (x >= 1 && bo[x - 1][y] && bo[x + 1][y]) {
      bo[x][y] = 1;
      return true;
    }
    return false;
  }

  ////////////
  function delFrame(x, y, frame, pill, bo, n) {
    let flag = true;
    if (frame == 0) pill[x][y] = 0;
    else bo[x][y] = 0;

    for (let x = 0; x < n + 1; ++x) {
      for (let y = 0; y < n + 1; ++y) {
        if (pill[x][y] == 1) {
          if (!addPill(x, y, pill, bo)) flag = false;
        }

        if (bo[x][y] == 1) {
          if (!addBo(x, y, pill, bo)) flag = false;
        }
      }
      if (!flag) break;
    }

    if (!flag) {
      if (frame == 0) pill[x][y] = 1;
      else bo[x][y] = 1;
    }

    return flag;
  }
}

///
function solution(n, build_frame) {
  let board = init(n);

  for (let k = 0; k < build_frame.length; ++k) {
    let build = build_frame[k];
    let x = build[0];
    let y = build[1];
    let type = build[2]; // 기둥(0), 보(1)
    let act = build[3]; // 설치(1), 삭제(0)

    if (act == 1) {
      if (type == 0) {
        board[y][x][2] += 1;
        if (!check(board, x, y)) board[y][x][2] -= 1;
      } else {
        board[y][x][3] += 1;
        if (!check(board, x, y)) board[y][x][3] -= 1;
      }
    } else {
      if (type == 0) {
        if (board[y][x][2] > 0) {
          board[y][x][2] -= 1;
          if (!check(board, x, y) || !check(board, x, y + 1) || !check(board, x - 1, y + 1))
            board[y][x][2] += 1;
        }
      } else {
        if (board[y][x][3] > 0) {
          board[y][x][3] -= 1;
          if (!check(board, x, y) || !check(board, x - 1, y) || !check(board, x + 1, y))
            board[y][x][3] += 1;
        }
      }
    }
  }

  let answer = [];
  for (let x = 0; x < board.length; ++x) {
    for (let y = 0; y < board.length; ++y) {
      if (board[y][x][2] > 0) {
        // 기둥 = 0
        answer.push([board[y][x][0], board[y][x][1], board[y][x][2] - 1]);
      }
      if (board[y][x][3] > 0) {
        // 보 = 1
        answer.push([board[y][x][0], board[y][x][1], board[y][x][3]]);
      }
    }
  }

  return answer;

  function check(board, x, y) {
    let flag = true;

    if (board[y][x][2] == 1) {
      // 기둥
      if (
        y == 0 ||
        board[y - 1][x][2] == 1 ||
        board[y][x][3] == 1 ||
        (x > 0 && board[y][x - 1][3] == 1)
      ) {
        flag = true;
      } else {
        return false;
      }
    }

    if (board[y][x][3] == 1) {
      // 보
      if (
        (y > 0 && board[y - 1][x][2] == 1) ||
        (y > 0 && x < n && board[y - 1][x + 1][2] == 1) ||
        (x > 0 && x < n && board[y][x - 1][3] == 1 && board[y][x + 1][3] == 1)
      ) {
        flag = true;
      } else {
        return false;
      }
    }

    return flag;
  }

  function init(n) {
    let board = [];
    for (let i = 0; i < n + 1; ++i) {
      let temp = [];
      for (let j = 0; j < n + 1; ++j) temp.push([j, i, 0, 0]);
      board.push([...temp]);
    }
    return board;
  }
}

solution(5, [
  [1, 0, 0, 1],
  [1, 1, 1, 1],
  [2, 1, 0, 1],
  [2, 2, 1, 1],
  [5, 0, 0, 1],
  [5, 1, 0, 1],
  [4, 2, 1, 1],
  [3, 2, 1, 1],
]);
