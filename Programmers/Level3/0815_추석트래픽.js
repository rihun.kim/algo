function solution(lines) {
  let logArr = [];
  let logPointArr = [];

  lines.forEach((log) => {
    let finishSec =
      Number(log.substring(11, 13)) * 3600 +
      Number(log.substring(14, 16)) * 60 +
      Number(log.substring(17, 23));
    let duration = Number(log.substring(24, log.length - 1));
    let startSec = Number((finishSec - duration + 0.001).toFixed(3));

    logArr.push([startSec, finishSec, duration]);
    logPointArr.push(finishSec, startSec);
  });

  let ans = 0;
  logPointArr.sort();
  logPointArr.forEach((log) => {
    let beginRange = log;
    let endRange = log + 1;
    let cnt = 0;

    logArr.forEach((logItem) => {
      let startSec = logItem[0];
      let endSec = logItem[1];
      if (
        (beginRange <= startSec && startSec < endRange) ||
        (beginRange <= endSec && endSec < endRange) ||
        (beginRange >= startSec && endSec >= endRange)
      ) {
        ++cnt;
      }
    });

    ans = Math.max(ans, cnt);
  });

  return ans;
}
