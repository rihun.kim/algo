// 이번 문제는 센스를 얼만큼 발휘할 수 있는가에 대한 것이다.
// Map 과 Set 을 얼마큼 잘 활용하는가에 대항 문제이다.
// 슬라이딩 윈도우 문제이다.
// 한칸식 지나가면서 값을 측정하는 형태이다.
// 슬라이딩 윈도우

function solution(gems) {
  let count = new Set(gems).size;
  let gemMap = new Map();
  let gemLength = [];

  gems.forEach((gem, i) => {
    gemMap.delete(gem);
    gemMap.set(gem, i);
    if (gemMap.size == count) {
      gemLength.push([gemMap.values().next().value + 1, i + 1]);
    }
  });

  gemLength.sort((a, b) => {
    if (a[1] - a[0] == b[1] - b[0]) return a[0] - b[0];
    else return a[1] - a[0] - b[1] - b[0];
  });

  return gemLength[0];
}
