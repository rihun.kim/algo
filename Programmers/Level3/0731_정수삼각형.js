// DP 로 푸는 쉬운 문제

function solution(triangle) {
  let dp = [[triangle[0][0]]];
  for (let r = 1; r < triangle.length; ++r) {
    dp[r] = [];
    dp[r][0] = triangle[r][0] + dp[r - 1][0];
    dp[r][triangle[r].length - 1] =
      triangle[r][triangle[r].length - 1] + dp[r - 1][triangle[r].length - 2];
    for (let c = 1; c < triangle[r].length - 1; ++c) {
      dp[r][c] = triangle[r][c] + Math.max(dp[r - 1][c - 1], dp[r - 1][c]);
    }
  }

  return Math.max(...dp[triangle.length - 1]);
}
