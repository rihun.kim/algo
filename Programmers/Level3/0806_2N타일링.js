// DP 문제
function solution(n) {
  let d = [0, 1, 2];

  for (let i = 3; i <= n; ++i) {
    d[i] = (d[i - 1] + d[i - 2]) % 1000000007;
  }

  return d[n] % 1000000007;
}
