// 완전탐색 문제
// 원순열일 경우 문제
// 순열 적용해야 하는 문제
// 한번 풀고말 문제는 아님

function solution(n, weak, dist) {
  let dists = [];
  dist.sort((a, b) => a - b);
  permu(dist, []);
  let ans = Infinity;

  for (let people of dists) {
    for (let i = 0; i < weak.length; ++i) {
      let weakList = makeWeakList(n, weak, i);

      let idx = 0;
      let cur = weakList[0] + people[idx];
      let flag = true;
      for (let i = 1; i < weakList.length; ++i) {
        if (cur < weakList[i]) {
          if (idx + 1 == people.length) {
            flag = false;
            break;
          }
          cur = weakList[i] + people[++idx];
        }
      }

      if (flag) ans = Math.min(ans, idx + 1);
    }
  }

  console.log(ans);
  return ans == Infinity ? -1 : ans;

  function makeWeakList(n, weak, startIdx) {
    let list = [];
    for (let i = startIdx; i < weak.length; ++i) list.push(weak[i]);
    for (let i = 0; i < startIdx; ++i) list.push(weak[i] + n);
    return list;
  }

  function permu(source, answer) {
    if (source.length == 0) dists.push([...answer]);

    for (let i = 0; i < source.length; ++i) {
      let temp = source.splice(i, 1);
      answer.push(...temp);
      permu(source, answer);
      answer.pop();
      source.splice(i, 0, ...temp);
    }
  }
}

solution(12, [1, 5, 6, 10], [1, 2, 3, 4]);
solution(12, [1, 3, 4, 9, 10], [3, 5, 7]);
