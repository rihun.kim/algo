function solution(word, pages) {
  word = word.toLowerCase();

  return pages
    .map((page, index) => {
      const [_, url] = page.match(/<meta property="og:url" content="([^"]+)"\/>/i);
      const tags = page.match(/<[^>]+>/g);
      const basicScore = tags
        .reduce((raw, tag) => raw.replace(tag, ""), page)
        .split(/[^a-zA-Z]/g)
        .filter((w) => w.toLowerCase() == word).length;
      const outUrls = tags.reduce((links, tag) => {
        const result = tag.match(/<a href="([^"]+)">/i);
        return result ? links.concat(result[1]) : links;
      }, []);

      return {
        index,
        url,
        outUrls,
        basicScore,
      };
    })
    .map((page, index, pages) => {
      page.linkScore = pages
        .filter((p) => p.outUrls.some((url) => url == page.url))
        .reduce((sum, p) => sum + p.basicScore / p.outUrls.length, 0);
      page.totalScore = page.linkScore + page.basicScore;
      return page;
    })
    .sort((a, b) =>
      a.totalScore == b.totalScore ? a.index - b.index : b.totalScore - a.totalScore
    )[0].index;
}

// solution("blind", [
//   '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://a.com"/>\n</head>  \n<body>\nBlind Lorem Blind ipsum dolor Blind test sit amet, consectetur adipiscing elit. \n<a href="https://b.com"> Link to b </a>\n</body>\n</html>',
//   '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://b.com"/>\n</head>  \n<body>\nSuspendisse potenti. Vivamus venenatis tellus non turpis bibendum, \n<a href="https://a.com"> Link to a </a>\nblind sed congue urna varius. Suspendisse feugiat nisl ligula, quis malesuada felis hendrerit ut.\n<a href="https://c.com"> Link to c </a>\n</body>\n</html>',
//   '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://c.com"/>\n</head>  \n<body>\nUt condimentum urna at felis sodales rutrum. Sed dapibus cursus diam, non interdum nulla tempor nec. Phasellus rutrum enim at orci consectetu blind\n<a href="https://a.com"> Link to a </a>\n</body>\n</html>',
// ]);

// console.log(
//   solution("Muzi", [
//     '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://careers.kakao.com/interview/list"/>\n</head>  \n<body>\n<a href="https://programmers.co.kr/learn/courses/4673"></a>#!MuziMuzi!)jayg07con&&\n\n</body>\n</html>',
//     '<html lang="ko" xml:lang="ko" xmlns="http://www.w3.org/1999/xhtml">\n<head>\n  <meta charset="utf-8">\n  <meta property="og:url" content="https://www.kakaocorp.com"/>\n</head>  \n<body>\ncon%\tmuzI92apeach&2<a href="https://hashcode.co.kr/tos"></a>\n\n\t^\n</body>\n</html>',
//   ])
// );
