function solution(dartResult) {
  let arr = dartResult.split("");
  let stack = [];

  for (let i = 0; i < arr.length; ) {
    let ch = arr[i];
    if (isNaN(ch)) {
      if (ch == "S") null;
      else if (ch == "D") stack[stack.length - 1] = Math.pow(stack[stack.length - 1], 2);
      else if (ch == "T") stack[stack.length - 1] = Math.pow(stack[stack.length - 1], 3);
      else if (ch == "*") {
        stack[stack.length - 1] *= 2;
        if (stack.length - 2 >= 0) stack[stack.length - 2] *= 2;
      } else if (ch == "#") stack[stack.length - 1] *= -1;
      ++i;
    } else {
      if (ch == 1 && arr[i + 1] == 0) {
        stack.push(10);
        i += 2;
      } else {
        stack.push(+ch);
        ++i;
      }
    }
  }

  return stack.reduce((prev, curr) => prev + curr);
}
