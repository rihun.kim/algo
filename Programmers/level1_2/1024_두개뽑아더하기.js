function solution(numbers) {
  let set = new Set;
  combi(numbers, [], numbers.length, 2, 0)

  let sol = [...set]
  sol.sort((a, b) => a - b);
  console.log(sol)
  return sol;

  function combi(src, ans, n, r, i) {
    if (r == 0) {
      set.add(ans[0] + ans[1])
    } else if (n < r || n == 0) return;
    else {
      ans.push(src[i]);
      combi(src, [...ans], n - 1, r - 1, i + 1);
      ans.pop();
      combi(src, [...ans], n - 1, r, i + 1)
    }
  }
}

solution([2, 1, 3, 4, 1])

