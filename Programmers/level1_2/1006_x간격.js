function solution(x, n) {
  return Array.from({ length: n }, () => x).map((val, i) => val + x * i);
}
