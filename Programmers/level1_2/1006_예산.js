function solution(d, budget) {
  let cnt = 0;
  d.sort((a, b) => a - b).forEach((money) => {
    if (budget >= money) {
      budget -= money;
      ++cnt;
    }
  });

  return cnt;
}
