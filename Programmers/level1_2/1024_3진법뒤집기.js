function solution(n) {
  let hex = n.toString(3).split("").reverse().join("")
  return +parseInt(hex, 3).toString(10)
}