function solution(N, stages) {
  let sum = Array.from({ length: N + 2 }, () => 0);
  let peopleNum = stages.length;

  stages.forEach((stage) => ++sum[stage]);
  let ans = [];
  for (let i = 1; i <= N; ++i) {
    if (sum[i] == 0) ans[i] = [0, i];
    else {
      ans[i] = [+(sum[i] / peopleNum).toFixed(2), i];
      peopleNum -= sum[i];
    }
  }

  ans.shift();
  let aans = ans
    .sort((a, b) => {
      if (a[0] === b[0]) return a[1] - b[1];
      else return b[0] - a[0];
    })
    .map((val) => val[1]);
  return aans;
}
