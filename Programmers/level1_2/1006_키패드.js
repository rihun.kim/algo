function solution(numbers, hand) {
  let leftState = "*";
  let rightState = "#";

  let ans = [];
  numbers.forEach((number) => {
    if (number == 1 || number == 4 || number == 7) {
      leftState = number;
      ans.push("L");
    } else if (number == 3 || number == 6 || number == 9) {
      rightState = number;
      ans.push("R");
    } else {
      let rightAns = diff(number, "right");
      let leftAns = diff(number, "left");

      if (rightAns < leftAns) {
        ans.push("R");
        rightState = number;
      } else if (rightAns > leftAns) {
        ans.push("L");
        leftState = number;
      } else {
        if (hand == "right") {
          ans.push("R");
          rightState = number;
        } else {
          ans.push("L");
          leftState = number;
        }
      }
    }
  });

  return ans.join("");

  function diff(number, hand) {
    const rightMap = {
      2: { 2: 0, 5: 1, 8: 2, 0: 3, 3: 1, 6: 2, 9: 3, "#": 4 },
      5: { 2: 1, 5: 0, 8: 1, 0: 2, 3: 2, 6: 1, 9: 2, "#": 3 },
      8: { 2: 2, 5: 1, 8: 0, 0: 1, 3: 3, 6: 2, 9: 1, "#": 2 },
      0: { 2: 3, 5: 2, 8: 1, 0: 0, 3: 4, 6: 3, 9: 2, "#": 1 },
    };
    const leftMap = {
      2: { 2: 0, 5: 1, 8: 2, 0: 3, 1: 1, 4: 2, 7: 3, "*": 4 },
      5: { 2: 1, 5: 0, 8: 1, 0: 2, 1: 2, 4: 1, 7: 2, "*": 3 },
      8: { 2: 2, 5: 1, 8: 0, 0: 1, 1: 3, 4: 2, 7: 1, "*": 2 },
      0: { 2: 3, 5: 2, 8: 1, 0: 0, 1: 4, 4: 3, 7: 2, "*": 1 },
    };

    if (hand == "right") return rightMap[number][rightState];
    else return leftMap[number][leftState];
  }
}

solution([1, 3, 4, 5, 8, 2, 1, 4, 5, 9, 5], "right");
// LRLLLRLLRRL
// LRLLLRLLRRL
