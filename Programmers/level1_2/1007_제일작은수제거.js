function solution(arr) {
  if (arr.length == 1) return [-1];

  let small = Infinity;
  let smallIndex = 0;
  arr.forEach((val, i) => {
    if (val < small) {
      small = val;
      smallIndex = i;
    }
  });

  arr.splice(smallIndex, 1);
  return arr;
}

function solution(arr) {
  if (arr.length == 1) return [-1];
  arr.splice(arr.indexOf(Math.min(...arr)), 1);
  return arr;
}
