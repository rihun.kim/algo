function solution(x) {
  let str = "" + x;
  return Number.isInteger(x / +str.split("").reduce((prev, curr) => +prev + +curr));
}
