function solution(n, m) {
  function gcf(a, b) {
    let tmp = a % b;
    a = b;
    b = tmp;

    return b == 0 ? a : gcf(a, b);
  }

  let g = gcf(n, m);
  return [g, (((g * n) / g) * m) / g];
}
