function solution(n, arr1, arr2) {
  function transfer(arr) {
    return arr.map((num) => {
      let temp = num.toString(2);
      if (temp.length < n) {
        return Array.from({ length: n - temp.length }, () => 0).join("") + temp;
      } else return temp;
    });
  }

  arr1 = transfer(arr1);
  arr2 = transfer(arr2);

  let ans = [];
  for (let i = 0; i < arr1.length; ++i) {
    let temp = [];
    for (let j = 0; j < arr1[i].length; ++j) {
      if (arr1[i].charAt(j) === "1" || arr2[i].charAt(j) === "1") {
        temp.push("#");
      } else {
        temp.push(" ");
      }
    }

    ans.push(temp.join(""));
  }

  return ans;
}
