function solution(s) {
  let ans = [];

  let front = [];
  let end = [];
  let i = 0;
  while (s[i] == " ") {
    front.push(" ");
    ++i;
  }
  i = 1;
  while (s[s.length - i] == " ") {
    end.push(" ");
    ++i;
  }

  s.split(" ").forEach((str) => {
    str = str.split("");
    for (let i = 0; i < str.length; ++i) {
      if (i % 2 == 0) {
        str[i] = str[i].toUpperCase();
      } else str[i] = str[i].toLowerCase();
    }
    ans.push(str.join(""));
  });

  return front.join("") + ans.join(" ").trim() + end.join("");
}

solution("   aaa  bbb cc  ");

function solution(s) {
  return s
    .split(" ")
    .map((word) => {
      let even = true;
      let str = "";
      for (let w of word) {
        str += even ? w.toUpperCase() : w.toLowerCase();
        even = !even;
      }
      return str;
    })
    .join(" ");
}
