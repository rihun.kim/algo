function solution(arr1, arr2) {
  const answer = [];

  arr1.forEach((row, r) => {
    let temp = [];
    row.forEach((col, c) => {
      temp.push(col + arr2[r][c]);
    });
    answer.push(temp);
  });

  return answer;
}
