function solution(s, n) {
  s = s.split("");

  for (let i = 0; i < s.length; ++i) {
    if (s[i] != " ") {
      if (s[i].charCodeAt() <= 90) {
        // 대문자
        let ascii = s[i].charCodeAt() + n;
        ascii = ascii > 90 ? 64 + (ascii % 90) : ascii;
        s[i] = String.fromCharCode(ascii);
      } else {
        // 소문자
        let ascii = s[i].charCodeAt() + n;
        ascii = ascii > 122 ? 96 + (ascii % 122) : ascii;
        s[i] = String.fromCharCode(ascii);
      }
    }
  }

  return s.join("");
}
