function solution(priorities, location) {
  let printCnt = 1;

  priorities = priorities.map((pr, idx) => {
    return [pr, idx == location]
  })

  while (true) {
    let nowPaper = priorities.shift();

    let flag = false;
    for (let pr of priorities) {
      if (nowPaper[0] < pr[0]) {
        flag = true;
        break;
      }
    }

    if (flag) {
      priorities.push(nowPaper);
    } else {
      if (nowPaper[1] == true) {
        console.log(printCnt)
        break;
      }
      ++printCnt
    }
  }
}

solution([1, 1, 9, 1, 1, 1], 0)