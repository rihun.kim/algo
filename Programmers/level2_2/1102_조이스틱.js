// 이 문제는 그리디 문제이다.
// 그리디는 매 순간 최적의 선택을 해야한다.
// 이 문제는 원순열과 함께 결합되어 있어서
// 풀기가 쉽진 않다.
// 그리디의 특성에 따라서, 항상 위치에서
// 오른쪽과 왼쪽의 거리값을 구하는 것이 포인트이다.
// 그래서 짧은 쪽으로 무조건 가면 된다.


function alphaMove(letter) {
  if (!letter) return;

  let index = letter.charCodeAt() - 64;
  if (index > 13) return 26 - index + 1
  else return index - 1;
}

//가장 가까운 a값을 찾는 것..
function leftRight(name, index) {

  let right = 1;
  let rightindex = index;
  while (true) {
    rightindex++;
    if (rightindex === name.length) rightindex = 0;

    if (name[rightindex] !== 'A') break;
    else right++;
  }

  let left = 1;
  let leftindex = index;
  while (true) {
    leftindex--;
    if (leftindex < 0) leftindex = name.length - 1;

    if (name[leftindex] !== 'A') break;
    else left++;
  }

  if (left === right) return [right, rightindex];
  return left > right ? [right, rightindex] : [left, leftindex];
}


function solution(name) {
  let basic = Array.from({ length: name.length }, () => "A");

  let upDownCnt = name.split("").map(ch => alphaMove(ch)).reduce((acc, cur) => (acc + cur), 0);

  let cnt = 0;
  let cpName = name;
  cpName = cpName.split("");
  cpName[0] = "A";
  let nowIndex = 0;

  // while 문의 조건문이 leftRight 의 루핑을 막아줌
  while (cpName.join("") != basic.join("")) {
    let [count, index] = leftRight(cpName, nowIndex);
    cnt += count;
    cpName[index] = "A";
    nowIndex = index;
  }

  return upDownCnt + cnt;
}

solution("ABAAAAAA")