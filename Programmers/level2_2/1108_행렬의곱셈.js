function solution(arr1, arr2) {
  let ans = [];
  for (let row1 = 0; row1 < arr1.length; ++row1) {
    let temp = [];
    for (let col2 = 0; col2 < arr2[0].length; ++col2) {
      let val = 0;
      for (let row2 = 0; row2 < arr2.length; ++row2) {
        val += (arr1[row1][row2] * arr2[row2][col2]);
      }
      temp.push(val);
    }
    ans.push([...temp]);
  }

  return ans;
}