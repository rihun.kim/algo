function solution(clothes) {
  let map = new Map();

  for (let clothe of clothes) {
    if (!map[clothe[1]]) map[clothe[1]] = [clothe[0]]
    else map[clothe[1]].push(clothe[0]);
  }

  let res = 1;
  for (let key in map) res *= (map[key].length + 1);

  return res - 1;
}

solution([["crow_mask", "face"], ["blue_sunglasses", "face"], ["smoky_makeup", "face"]])