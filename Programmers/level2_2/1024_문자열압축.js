function solution(s) {
  let min = Infinity;
  if (s.length == 1) return 1;

  for (let len = 1; len <= s.length / 2; ++len) {

    let temp = [];
    for (let i = 0; i < s.length; i += len) {
      temp.push(s.substr(i, len));
    }

    let ans = [];
    for (let i = 0; i < temp.length;) {
      if (temp[i] == temp[i + 1]) {
        let cnt = 1;
        let j = i + 1;
        while (true) {
          if (temp[i] == temp[j]) {
            ++cnt;
            ++j
            if (j >= temp.length) {
              ans.push("" + cnt + temp[i]);
              i = j;
              break;
            }
          } else {
            ans.push("" + cnt + temp[i]);
            i = j;
            break;
          }
        }
      } else {
        ans.push(temp[i]);
        ++i
      }
    }

    // console.log(ans)
    if (min > ans.join("").length) min = ans.join("").length
  }

  console.log(min)
  return min;
}

solution("a")