function solution(n) {
  let cnt = 0;
  for (let s = 1; s <= n; ++s) {
    let total = 0;
    for (let ss = s; ss <= n; ++ss) {
      total += ss;
      if (total === n) {
        ++cnt;
        break;
      } else if (total > n) break;
    }
  }

  return cnt;

}

console.log(solution(15));