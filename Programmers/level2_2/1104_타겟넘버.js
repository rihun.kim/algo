function solution(numbers, target) {
  let arr = [-1 * numbers[0], numbers[0]];

  for (let i = 1; i < numbers.length; ++i) {
    let temp = [];
    for (let num of arr) {
      temp.push(num + -1 * numbers[i])
      temp.push(num + numbers[i])
    }
    arr = [...temp];
  }

  let cnt = 0;
  arr.map((num) => num === target ? ++cnt : false);

  return cnt;
}

// solution([1, 1, 1, 1, 1], 3);
solution([1, 2], 2)


