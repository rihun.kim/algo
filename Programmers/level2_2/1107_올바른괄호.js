function solution(s) {
  if (s[0] === ")") return false;

  let stack = [];

  s.split("").map(v => {
    if (v === "(") stack.push("(");
    else stack.pop();
  })

  return stack.length === 0 ? true : false;
}

console.log(solution("(())()"));