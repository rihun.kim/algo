// 나머지 한 명 남은 것을 
// i 로 찾아내는 센스를 발휘

function solution(people, limit) {
  people = people.sort((a, b) => a - b);

  let i, j;
  for (i = 0, j = people.length - 1; i < j; --j) {
    if (people[i] + people[j] <= limit) ++i;
  }

  return people.length - i;
}