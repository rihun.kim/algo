function solution(bridge_length, weight, truck_weights) {
  let nowWeight = 0;
  let moving = [];
  let time;
  for (time = 1; ; time++) {
    for (let truck of moving) ++truck[0];

    if (moving.length != 0 && moving[0][0] > bridge_length) {
      nowWeight -= moving[0][1];
      moving.shift();
    }

    if (truck_weights[0] + nowWeight <= weight) {
      nowWeight += truck_weights[0];
      moving.push([1, truck_weights.shift()])
    }

    // console.log(time, moving, truck_weights)
    if (truck_weights.length == 0 && moving.length == 0) break;

  }

  return time
}