function solution(n) {
  let nums = [];

  for (let i = 1; i <= 1000000; ++i) {
    nums.push(i)
  }

  let ans = Array.from({ length: n }, () => Array.from({ length: n }, () => false));

  let numIdx = 0;
  let state = 0;
  let y = 0, x = 0;
  let period = n;
  for (let N = 1; N <= n; ++N) {
    for (let i = period; i >= 1; --i) {
      if (state == 0) {
        ans[y++][x] = nums[numIdx++];
      } else if (state == 1) {
        ans[y][x++] = nums[numIdx++];
      } else {
        ans[y--][x--] = nums[numIdx++];
      }
    }

    --period;

    if (state == 0) {
      state = 1;
      --y;
      ++x;
    } else if (state == 1) {
      state = 2;
      --y;
      x -= 2;
    } else {
      state = 0;
      y += 2;
      ++x;
    }
  }

  let val = [];
  ans.forEach((arr) => {
    arr.forEach((num) => {
      if (Number.isInteger(num)) {
        val.push(num)
      }
    })
  })

  return val;
}