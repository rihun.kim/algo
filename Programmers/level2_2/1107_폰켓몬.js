function solution(nums) {
  let len = nums.length / 2;
  nums = new Set(nums)

  return len <= nums.size ? len : nums.size;
}

console.log(solution([3, 1, 2, 3]))