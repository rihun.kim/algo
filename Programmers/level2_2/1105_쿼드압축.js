function solution(arr) {
  let zeroCnt = 0;
  let oneCnt = 0;

  recurv(arr);
  console.log(zeroCnt, oneCnt)
  return [zeroCnt, oneCnt];

  function recurv(matrix) {
    if (matrix.length === 1 || isPossible(matrix)) {
      if (matrix[0][0] === 0) ++zeroCnt;
      else ++oneCnt;

      return;
    }

    let res = divide(matrix);
    for (let mat of res) {
      recurv(mat);
    }
  }

  function divide(matrix) {
    let arr1 = [];
    let arr2 = [];
    for (let y = 0; y < matrix.length / 2; ++y) {
      let temp = [];
      for (let x = 0; x < matrix.length / 2; ++x) {
        temp.push(matrix[y][x])
      }
      arr1.push([...temp])

      temp = [];
      for (let x = matrix.length / 2; x < matrix.length; ++x) {
        temp.push(matrix[y][x])
      }
      arr2.push([...temp])
    }

    let arr3 = [];
    let arr4 = [];
    for (let y = matrix.length / 2; y < matrix.length; ++y) {
      let temp = [];
      for (let x = 0; x < matrix.length / 2; ++x) {
        temp.push(matrix[y][x]);
      }
      arr3.push([...temp])

      temp = [];
      for (let x = matrix.length / 2; x < matrix.length; ++x) {
        temp.push(matrix[y][x])
      }
      arr4.push([...temp])
    }

    return [[...arr1], [...arr2], [...arr3], [...arr4]]
  }

  function isPossible(matrix) {
    let startNum = matrix[0][0];

    for (let y = 0; y < matrix.length; ++y) {
      for (let x = 0; x < matrix[0].length; ++x) {
        if (startNum !== matrix[y][x]) return false;
      }
    }

    return true;
  }
}

solution([[1, 1, 1, 1, 1, 1, 1, 1], [0, 1, 1, 1, 1, 1, 1, 1], [0, 0, 0, 0, 1, 1, 1, 1], [0, 1, 0, 0, 1, 1, 1, 1], [0, 0, 0, 0, 0, 0, 1, 1], [0, 0, 0, 0, 0, 0, 0, 1], [0, 0, 0, 0, 1, 0, 0, 1], [0, 0, 0, 0, 1, 1, 1, 1]]);