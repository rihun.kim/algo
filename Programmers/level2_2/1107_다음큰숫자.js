function solution(n) {
  let oneCnt = 0;
  n.toString(2).split("").map(ch => ch === "1" ? ++oneCnt : false);

  for (let num = n + 1; num <= 1000000; ++num) {
    let tempCnt = 0;
    num.toString(2).split("").map(ch => ch === "1" ? ++tempCnt : false);

    if (oneCnt === tempCnt) return num;
  }
}