function solution(numbers) {
  let ans = numbers.map((num) => "" + num).sort((a, b) => {
    if (+a[0] === +b[0]) {
      return +(a + b) < +(b + a) ? 1 : -1;
    } else {
      return +b[0] - +a[0];
    }
  }).join("");

  if (ans[0] === "0") return "0";
  return "" + ans;
}

solution([0, 0, 1, 0]);