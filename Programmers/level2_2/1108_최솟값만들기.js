function solution(A, B) {
  A = A.sort((a, b) => a - b);
  B = B.sort((a, b) => b - a);

  let total = 0;
  A.map((a, i) => total += a * B[i]);

  return total;
}