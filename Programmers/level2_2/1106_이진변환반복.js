function solution(s) {

  let transCnt = 0;
  let totalZCnt = 0;

  while (s != 1) {
    let zCnt = getZeroCnt(s);
    totalZCnt += zCnt;
    s = translate(s.length - zCnt);
    ++transCnt;
  }

  return [transCnt, totalZCnt]

  function getZeroCnt(str) {
    let cnt = 0;
    for (let i = 0; i < str.length; ++i) str[i] == 0 ? ++cnt : false;

    return cnt;
  }

  function translate(num) {
    return num.toString(2);
  }
}

solution("110010101001");