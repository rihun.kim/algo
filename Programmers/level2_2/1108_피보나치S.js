function solution(n) {

  let a = 0;
  let b = 1;
  let c = 0;

  for (let i = 1; i <= n; ++i) {
    c = a + b;
    b = a % 1234567;
    a = c % 1234567;
  }

  return c % 1234567;
}