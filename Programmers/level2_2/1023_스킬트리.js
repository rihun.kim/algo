function solution(skill, skill_trees) {

  skill = skill.split("");
  skill_trees = skill_trees;

  let ans = 0;
  for (let st of skill_trees) {
    let tmp = st.split("")
    let idx = 0;
    let flag = true;
    for (let ch of tmp) {
      if (skill[idx] == ch) {
        ++idx;
      } else {
        if (skill.includes(ch)) {
          flag = false
          break;
        }
      }
    }

    if (flag) ++ans;
  }

  console.log(ans)
  return ans;
}


solution("CBD", ["BACDE", "CBADF", "AECB", "BDA"]);