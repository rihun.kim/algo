// 균형잡인 문자열 : ( ) 의 갯수가 맞음
// 올바른 문자열 : ( ) 짝도 맞음
// 균형잡인 => 올바른 으로 변경

// u : 균형잡힌 최소단위
// v : 빈 문자열 가능

// u 가 올바르기까지 하면, v 를 1 단계(빈문자열 반환, u,v 로 나누기) 부터 수행
// v 과정이 끝나면, u 에 붙여서 반환

// u 과정은
// 올바른지 판단, 맞으면 그대로 v 와 합쳐서 반환

// !!! 아니라면
// 빈문자열 생성 후 "(" + 이전에 만든 v 값 + ")"
// u 의 처음과 마지막 기호 삭제, 나머지의 괄호를 뒤집고 (+v) 뒤에 붙이기!

function solution(p) {

  return recurv(p, "");

  function recurv(u) {
    if (u === "") return "";

    const arr = balancing(u);

    if (correcting(arr[0])) {
      return arr[0] + recurv(arr[1]);
    } else {
      let temp = "(" + recurv(arr[1]) + ")";
      arr[0] = arr[0].slice(0, -1).slice(1, arr[0].length); //// error
      return temp += reversing(arr[0])
    }
  }

  function balancing(str) {
    let leftCnt = 0;
    let rightCnt = 0;
    for (let i = 0; i < str.length; ++i) {
      if (str[i] === "(") ++leftCnt;
      else ++rightCnt;

      if (leftCnt === rightCnt) {
        return [str.slice(0, i + 1), str.slice(i + 1, str.length)]
      }
    }
  }

  function correcting(str) {
    let stack = [];

    for (let i = 0; i < str.length; ++i) {
      if (str[i] === "(") {
        stack.push(str[i]);
      } else {
        if (stack[stack.length - 1] === "(") stack.pop();
        else return false;
      }
    }

    return true;
  }

  function reversing(str) {
    let arr = str.split("").map(s => s === "(" ? ")" : "(").join("")
    return arr;
  }

}

// solution(")(");
// solution("()))((()")
solution("(()())()")
