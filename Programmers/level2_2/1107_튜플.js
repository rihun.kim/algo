function solution(s) {
  s = JSON.parse(s.replace(/{/gi, "[").replace(/}/gi, "]"));
  s.sort((a, b) => a.length - b.length);

  let total = [...s[0]];
  for (let i = 1; i < s.length; ++i) {
    for (let j = 0; j < s[i].length; ++j) {
      if (!total.includes(s[i][j])) {
        total.push(s[i][j]);
        break;
      }
    }
  }

  return total;
}



console.log(solution("{{1,2,3},{2,1},{1,2,4,3},{2}}"));