function solution(number, k) {
  let nums = number.split("");

  let i = 0;
  while (true) {
    if (nums[i] >= nums[i + 1]) {
      ++i;
    } else {
      nums.splice(i, 1);    // splice 가 느려서 못 통과함...

      i = i > 0 ? --i : 0;
      --k;
    }

    if (k === 0 || i === nums.length - 1) {
      break;
    }
  }


  if (k != 0) {
    nums.pop();
    --k;
  }

  return nums.join("")
}

console.log(solution("4177252841", 4))

// function solution(number, k) {
//   number = number.split("");

//   let stk = [-1];
//   ++k;
//   for (let n of number) {
//     while (k > 0) {
//       if (stk[stk.length - 1] < n) {
//         stk.pop();
//         --k;
//         if (stk.length == 0) break;
//       } else break;
//     }
//     stk.push(n);
//   }

//   while (k > 0) {
//     stk.pop();
//     --k;
//   }

//   return stk.join("");
// }
