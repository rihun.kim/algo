function solution(progresses, speeds) {
  let ans = [];

  for (let day = 1; day <= 100; ++day) {
    for (let i = 0; i < progresses.length; ++i) progresses[i] += speeds[i];

    let deployCnt = 0;
    while (progresses.length != 0) {
      if (progresses[0] >= 100) {
        ++deployCnt;
        progresses.shift();
        speeds.shift();
      } else break;
    }

    if (deployCnt != 0) ans.push(deployCnt);
    if (progresses.length == 0) break;
  }

  console.log(ans)
}

