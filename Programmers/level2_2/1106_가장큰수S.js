function solution(board) {
  let max = Math.max(...board[0]);

  const R = board.length;
  const C = board[0].length;

  for (let r = 1; r < R; ++r) {
    for (let c = 1; c < C; ++c) {
      if (board[r][c] >= 1 && board[r - 1][c - 1] >= 1 && board[r - 1][c] >= 1 && board[r][c - 1] >= 1) {
        board[r][c] = Math.min(board[r - 1][c - 1], board[r - 1][c], board[r][c - 1]) + 1;
        max = Math.max(board[r][c], max);
      }
    }
  }

  return max * max;
}