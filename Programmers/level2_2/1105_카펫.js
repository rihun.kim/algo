function solution(brown, yellow) {
  for (let i = 1; i <= yellow; ++i) {
    if (Number.isInteger(yellow / i)) {
      let width = (yellow / i + 2) * 2;
      let height = i * 2;
      if (width + height === brown) return [yellow / i + 2, i + 2]
    }
  }
}

console.log(solution(8, 1));    // 8, 6