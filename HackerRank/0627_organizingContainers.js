// 아이디어
// 각 컨테이너마다 갖고 있는 번호를 자신이 모아야하는 볼이라고 생각
// 그래서 각 컨테이너의 요소 수를 계산
// 다른 컨테이너에서 그 컨테이너 만큼의 번호를 갖고 있는지 판단
// 이 떄, 각 컨테이너가 기존에 자신이 갖고 있는 볼을 빼고 세야 함

// 1. 각 컨테이너마다 엘리먼트 수 구하기
// 2. 컨테이너 마다 자신의 번호 알기 및 몇개 있는지 구하기
// 3. 자신 이외에 컨테이너에 자신의 번호가 몇개 있는지 구하기
// 4. 같으면 계속, 다르면 바로 Impossible 출력
// 중요한 점은 0번 컨테이너가 0번을 다 갖고 있을 필요는 없다.
// 총량만 서로 같으면 해결됨...

const test = () => {
  // 정답은 Possible
  let container = [
    [997612619, 934920795, 998879231, 999926463],
    [960369681, 997828120, 999792735, 979622676],
    [999013654, 998634077, 997988323, 958769423],
    [997409523, 999301350, 940952923, 993020546],
  ];

  let capacity = [];
  let count = [];
  for (let i = 0; i < container.length; i++) {
    capacity.push(container[i].reduce((a, x) => a + x)); // 배열 합 구하는 방법
    count.push(container.map((x) => x[i]).reduce((a, x) => a + x)); // 각항목별 합 구하기
  }

  let capacityString = JSON.stringify(capacity.sort((a, b) => a - b));
  let countString = JSON.stringify(count.sort((a, b) => a - b));

  return capacityString == countString ? "Possible" : "Impossible";
};

console.log("ans", test());
