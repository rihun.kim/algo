function formingMagicSquare(s) {
  const ms = [
    [8, 3, 4, 1, 5, 9, 6, 7, 2],
    [8, 1, 6, 3, 5, 7, 4, 9, 2],
    [4, 3, 8, 9, 5, 1, 2, 7, 6],
    [6, 1, 8, 7, 5, 3, 2, 9, 4],
    [2, 7, 6, 9, 5, 1, 4, 3, 8],
    [2, 9, 4, 7, 5, 3, 6, 1, 8],
    [6, 7, 2, 1, 5, 9, 8, 3, 4],
    [4, 9, 2, 3, 5, 7, 8, 1, 6],
  ];

  let arr = [];
  for (let i = 0; i < s.length; ++i) {
    arr.push(...s[i]);
  }

  let minNum = Number.MAX_SAFE_INTEGER;
  for (let i = 0; i < ms.length; ++i) {
    let minCost = 0;
    for (let j = 0; j < ms[i].length; ++j) {
      minCost += Math.abs(ms[i][j] - arr[j]);
    }
    if (minNum > minCost) {
      minNum = minCost;
    }
  }
  return minNum;
}
