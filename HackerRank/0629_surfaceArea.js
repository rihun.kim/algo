// 아이디어
// 순열이다. 순열
// 알골 중급이상에는 순열을 무작정 구하라고 하진 않을 것이다!!
// 그러므로 수학적인 규칙을 찾는 것이 핵심!!!
// 규칙을 구할 때는 작은 경우의 수부터 구해본다!!!
// 단언컨대, 중급이상 알골 문제는 노가다로 풀면 폭망
// 반드시 규칙을 찾아야 한다.
// 예를 들면, k = 3, n = 6 이라고 하면
// i = 1    i + 3(k) = 4
// i = 2    i + 3(k) = 5
// i = 3    i + 3(k) = 6
// i = 4    i - 3(k) = 1
// i = 5    i - 3(k) = 2
// i = 6    i - 3(k) = 3
// 정답은 456123 - 123456 이 333333 이므로...
// 만약 n = 7 이면
// i = 7    i - 3(k) = 4 가 되어 순열이 안된다..
// 따라서 n == k*2 가 되면, 순열 조건이 된다.
// 근데 위의 조건은 부족한 부분이 있다.
// n % (k * 2) == 0 이 되어야 한다.
// 즉 n == k*2 가 안되면 무조건 [-1] 을 내보내고
// 아니면 위의 규칙에 따른 순열을 만들어서 배포ㅋ

function surfaceArea(A) {
  let R = A.length;
  let C = A[0].length;

  // 상단, 하단 넓이
  let surface = R * C * 2;

  let dr = [-1, 1, 0, 0];
  let dc = [0, 0, -1, 1];
  for (let row = 0; row < R; ++row) {
    for (let col = 0; col < C; ++col) {
      let newRow, newCol;
      for (let i = 0; i < 4; ++i) {
        newRow = row + dr[i];
        newCol = col + dc[i];
        if (newRow < 0 || newRow >= R || newCol < 0 || newCol >= C) {
          surface += A[row][col];
        } else {
          if (A[row][col] > A[newRow][newCol]) {
            surface += Math.abs(A[row][col] - A[newRow][newCol]);
          }
        }
      }
    }
  }

  return surface;
}
