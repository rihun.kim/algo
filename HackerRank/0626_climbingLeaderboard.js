function climbingLeaderboard(scores, alice) {
  let arr = [...new Set(scores)];

  let res = [];
  let searchKey = arr.length - 1;
  for (let i = 0; i < alice.length; ++i) {
    for (let j = searchKey; j >= 0; --j) {
      if (alice[i] == arr[j]) {
        res.push(j + 1);
        searchKey = j;
        break;
      } else if (alice[i] < arr[j]) {
        res.push(j + 2);
        arr.splice(j + 1, 0, alice[i]);
        break;
      } else if (alice[i] > arr[0]) {
        res.push(1);
        arr.unshift(alice[i]);
        searchKey = j;
        break;
      }
    }
  }
  return res;
}
