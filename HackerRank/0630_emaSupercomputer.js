function get(gridMatrix) {
  let R = gridMatrix.length;
  let C = gridMatrix[0].length;
  let grid = gridMatrix;
  let maxVal = 0;

  for (let row = 0; row < R; ++row) {
    for (let col = 0; col < C; ++col) {
      if (grid[row][col] == "G") {
        let plus = 1;
        let rd = 1;
        let cd = 1;
        let cont = true;

        while (cont) {
          if (row + rd >= R || row - rd < 0 || col + cd >= C || col - cd < 0) {
            cont = false;
          } else if (
            grid[row + rd][col] == "G" &&
            grid[row - rd][col] == "G" &&
            grid[row][col + cd] == "G" &&
            grid[row][col - cd] == "G"
          ) {
            ++rd;
            ++cd;
            plus += 4;
          } else {
            cont = false;
          }
        }

        maxVal = maxVal < plus ? plus : maxVal;
      }
    }
  }

  return maxVal;
}

function twoPluses(gridMatrix) {
  // grid 을 배열화
  let R = gridMatrix.length;
  let C = gridMatrix[0].length;
  let grid = [];
  for (let row of gridMatrix) grid.push(row.split(""));

  let ans = 0;
  for (let row = 0; row < R; ++row) {
    for (let col = 0; col < C; ++col) {
      if (grid[row][col] == "G") {
        let plus = 1;
        let rd = 1;
        let cd = 1;
        let cont = true;

        // console.log(row, col);

        // 가능한 경우 찾아내기
        while (cont) {
          if (row + rd >= R || row - rd < 0 || col + cd >= C || col - cd < 0) break;
          if (
            grid[row + rd][col] == "G" &&
            grid[row - rd][col] == "G" &&
            grid[row][col + cd] == "G" &&
            grid[row][col - cd] == "G"
          ) {
            ++rd;
            ++cd;
            plus += 4;
          } else {
            cont = false;
          }
        }

        // 위에서 찾아낸 경우의 수로 결과 구하기
        if (plus > 1) {
          for (let i = 2; i <= rd; ++i) {
            grid[row][col] = "B";
            let plusVal = 1 + 4 * (i - 1);

            for (let j = 1; j < i; ++j) {
              grid[row + j][col] = "B";
              grid[row - j][col] = "B";
              grid[row][col + j] = "B";
              grid[row][col - j] = "B";
              // console.log("BB ", row + j, row - j, col + j, col - j);
            } // 최소단위 십자가부터 생성으로 메꾸기

            let val = get(grid);
            // console.log(row, col, "[[[ ", i, "]]]", val);

            // console.log("val", val);
            ans = ans < val * plusVal ? val * plusVal : ans;

            // 메꿨던 부분 풀기
            grid[row][col] = "G";
            for (let k = 1; k < i; ++k) {
              grid[row + k][col] = "G";
              grid[row - k][col] = "G";
              grid[row][col + k] = "G";
              grid[row][col - k] = "G";
              // console.log("GG ", row + k, row - k, col + k, col - k);
            }
          }
        }
      }
    }
  }

  return ans == 0 ? 1 : ans;
}
