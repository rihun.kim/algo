// 아이디어
// 순열이 아닌 소팅임!
// 가장 최소값 찾기 그리고 그것의 인덱스와 맞는지 파악 V
// 그 최소값의 위치와 최소값이 가야할 위치에서
// 1번 swap 으로 sort 가 되었는지
// 2번 reverse 으로 sort 가 되었는지
// 둘다 안되면 바로 NO
// 1/2번 의 결과가 참인지 보려면
// 각 인덱스와 그 값이 맞는지 파악

function isSort(arr) {
  for (let i = 0; i < arr.length - 1; ++i) {
    if (arr[i] - arr[i + 1] <= 0) continue;
    else return false;
  }
  return true;
}

function almostSorted(arr) {
  let start = 0;
  let num;
  let numNowIndex;
  let numFutuIndex;
  let cont = true;
  while (cont) {
    let min = arr[start];
    for (let i = start + 1; i < arr.length; ++i) {
      if (min > arr[i]) {
        // arr[i] 가 바로 start 에 있어야할 녀석
        min = arr[i];
        num = arr[i];
        numNowIndex = i;
        numFutuIndex = start;
        cont = false;
      }
    }
    start++;
  }

  // console.log(num, numNowIndex, numFutuIndex);
  // swap
  let temp = arr[numFutuIndex];
  arr[numFutuIndex] = arr[numNowIndex];
  arr[numNowIndex] = temp;
  if (isSort(arr)) {
    console.log("yes\nswap " + (numFutuIndex + 1) + " " + (numNowIndex + 1));
    return;
  }

  // reverse
  temp = arr[numFutuIndex];
  arr[numFutuIndex] = arr[numNowIndex];
  arr[numNowIndex] = temp;

  arr.splice(numFutuIndex, numNowIndex, ...arr.slice(numFutuIndex, numNowIndex + 1).reverse());
  if (isSort(arr)) {
    console.log("yes\nreverse " + (numFutuIndex + 1) + " " + (numNowIndex + 1));
    return;
  }

  console.log("no");
  return;
}
