// 아이디어

// 항상 아이디어 노트 부터 적기
// 1. P 의 문자열이 G 에 있는지를 찾기
//    G 을 루프로 하나씩 돌면서 찾기
//    찾을 때, 인덱스를 하나씩 증가시키면서 모든 인덱스를 찾아 배열로 리턴
//    indexOf 는 없으면 -1 을 리턴
// 2. 배열을 기반으로, G 에서 row 와 col 부분까지 매칭 검사
//    매칭은 equal 로 하면 됨

function gridSearch(G, P) {
  const GHeight = G.length;
  const PHeight = P.length;
  const PWidth = P[0].length;

  for (let GIndex = 0; GIndex < G.length; GIndex++) {
    let GString = G[GIndex];
    if (GString.indexOf(P[0]) != -1) {
      // 찾아야할 시작점 배열 만들기
      let findArr = [];
      for (let i = 0; i <= GString.length - PWidth; ++i)
        if (GString.slice(i, i + PWidth).indexOf(P[0]) != -1) findArr.push(i);

      // 나머지 부분 매칭 검사
      for (let findIndex of findArr) {
        for (let i = GIndex + 1, prow = 1; i < GIndex + PHeight && i < GHeight; ++i, ++prow) {
          if (G[i].slice(findIndex, findIndex + PWidth) != P[prow]) break;
          else if (prow == PHeight - 1) return "YES";
        }
      }
    }
  }
  return "NO";
}

const shortTest = () => {
  const G = [
    "111111111111111",
    "111111111111111",
    "111111011111111",
    "111111111111111",
    "111111111111111",
  ];
  const P = ["11111", "11111", "11110"];

  console.log("ans : ", gridSearch(G, P));
};

const test = () => {
  // for (let i = 0; i < input.input.length; ++i) {
  //   let myAns = biggerIsGreater(input.input[i]);
  //   if (answer.answer[i] != myAns) {
  //     console.log(input.input[i], answer.answer[i], myAns);
  //   }
  // }
};

shortTest();
// test();
