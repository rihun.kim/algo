function queensAttack(n, k, r_q, c_q, obstacles) {
  let map = new Map();
  for (let ele of obstacles) map["" + ele[0] + "-" + ele[1]] = true;

  // 있는지 없는지 확인하는 가장 빠른 방법은 바로 Map 이용
  // 키를 만들때 중간에 - 넣기
  let upDown = [1, 0, -1];
  let leftRight = [1, 0, -1];
  // 상/하: +1,0,-1
  // 좌/우: -1,0,+1
  // 0,0 은 제외
  let sum = 0;
  for (let ud of upDown) {
    for (let lr of leftRight) {
      // console.log(ud, lr);
      let row = r_q;
      let col = c_q;
      // console.log(row, col, "시작");
      if (ud == 0 && lr == 0) continue;
      while (true) {
        row = row + ud;
        col = col + lr;

        if (map["" + row + "-" + col]) break;
        if (row < 1 || row > n || col < 1 || col > n) break;
        sum++;
        // console.log("sum:", sum, ",", row, col);
      }
    }
  }

  return sum;
}
