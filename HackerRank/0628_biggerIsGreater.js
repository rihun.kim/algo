// 아이디어

// 1. 맨 끝에서 부터 시작해야 한다.
// 2. 바로 앞에 있는 것이 자신의 위치의 값 보다 클 때까지 찾음
// 3. 자신보다 더 큰 놈을 찾으면, 큰 놈의 위치를 기준으로 나머지 문자들을 재정렬
// 4. 자신이 더 작으면, 꽉 찬 값이므로, 재정렬
//    재 정렬은 작아지는 부분을 찾은 뒤, 거기를 그 다음 작은 값으로 바꾸고,
//    그 뒤부터 오름차순 으로 나열

// dhck --> dhkc
// 2314 --> 2341
// dkhc --> hcdk
// [2431] --> 3124 // 조작 범위가 전체임 2 다음인 3을 넣고 그다음 오름차순
// 2413 --> 24[31]
// 52431 --> 53124
// 51324 --> 513[42] 자신이 바꿀 수 있는 위치부터 숫자들을 조작할 수 있음

const input = require("./Temp/input");
const answer = require("./Temp/answer");

const biggerIsGreater = (w) => {
  let arr = w.split("");
  for (let i = arr.length - 1; i > 0; --i) {
    if (arr[i - 1] < arr[i]) {
      // console.log(arr[i - 1], arr[i]);
      // i-1 부터 문자 재정렬 시작, i-1, arr.length-1 까지
      let temp = arr.slice(i - 1, arr.length).sort();
      for (let k = 0; k < temp.length; ++k) {
        if (temp[k] > arr[i - 1]) {
          // console.log("dddd", temp[k], arr[i - 1], temp[k + 1]);
          let x = temp[k];
          temp.splice(k, 1);
          temp.unshift(x);
          // console.log(temp);
          break;
        }
      }

      // console.log(arr[i]);
      // console.log(temp.join(""), arr.splice(i - 1, temp.length).join(""));
      // console.log(temp.join("") < arr.splice(i - 1, temp.length).join(""));

      arr.splice(i - 1, temp.length).join("");
      arr = arr.concat(temp);
      return arr.join("");
    }
  }
  return "no answer";
};

const shortTest = () => {
  console.log("zalqxykemvzzgaka", "zalqxykemvzzgkaa" == biggerIsGreater("zalqxykemvzzgaka"));
};

const test = () => {
  for (let i = 0; i < input.input.length; ++i) {
    let myAns = biggerIsGreater(input.input[i]);
    if (answer.answer[i] != myAns) {
      console.log(input.input[i], answer.answer[i], myAns);
    }
  }
};

// shortTest();
test();
