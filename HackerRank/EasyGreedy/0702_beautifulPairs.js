// 아이디어
// 그리디인 이유
// 그리디가 아니라면, 모든 조합을 다 찾아야 한다.
// 예를 들어, A에서 1개 항, B에서 1개항 씩 조합하고
// 2개, 3개 이런식으로 조합하면서 찾아야한다.
// 하지만, 그리디의 특징적으로
// 특정한 조건을 만족시키면 그것이 최적의 해가 된다.
// 이 문제처럼, 같은 값의 페어를 찾는 것은
// 그 값이 A, B 에서 몇개 인지만 알면 끝이다.
// 즉, 숫자 세기로 바뀌게 된다. 조합 -> 숫자세기로
// 이처럼, 그리디는 문제의 형태를 바꾸게 만드는 특징이 있다.
// 숫자세기를 하고
// 마지막에 부가적인 조건만 생각해주면 끝이다.
// 그리디는 무한대의 경우의 수를 단순하게 만들어준다.

function beautifulPairs(A, B) {
  let mapA = new Map();
  for (let ele of A) {
    if (mapA[ele] == undefined) mapA[ele] = 1;
    else mapA[ele]++;
  }

  let mapB = new Map();
  for (let ele of B) {
    if (mapB[ele] == undefined) mapB[ele] = 1;
    else mapB[ele]++;
  }

  let sum = 0;
  let cnt = 0;
  for (let key in mapA) {
    if (mapA[key] && mapB[key]) {
      sum += mapA[key] > mapB[key] ? mapB[key] : mapA[key];
      ++cnt;
    }
  }

  if (sum < A.length) {
    return ++sum;
  } else {
    return --sum;
  }
}
