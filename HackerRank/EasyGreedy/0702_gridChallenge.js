// Complete the gridChallenge function below.
// 아이디어
// 왜 그리디인가?
// 문제에서 가로행을 재정렬하여,
// 세로열이 오름차순임을 보장하라고 한다.
// 이 경우,
// 첫번째 가로행을 오름차순화 시켰을때,
// 두번째 가로행은 자기 앞에 엘리먼트와 비교 과정을
// 거치게 된다.
// 이 때, 두번째 가로행도 오름차순이 된 상태로 비교를 해야한다.
// 첫 가로행은 a1 < a2 < a3 < a4 (오름차순화)
// 두번째 가로행은 b1 < b2 < b3 < b4 (오름차순화) 으로 비교해야 한다.
// 이는 무조건 자기 a1 뒤에 b1, b2, b3, b4 중 큰 것 아무거나를 배치하지 않아야 함을 의미한다.
// 왜냐하면, a2 이후부터 아래와 비교할 때, 문제가 생기기 때문이다.
// 예를 들면, a1 -> b2, a2 -> b3, a3 -> b4, a4 -> b1 하면, b 행이 오름차순이 안되기 때문이다.
// 따라서, 각 가로행이 모두 오름차순이면,
// 각 세로열도 무조건 모두 오름차순이 성립하고 있어야 한다.
// 재배열하는 순간 망한다. 문제에서 페이크침
// 그리디인 이유는 각각을 정렬하고
// 위에서 부터 하나씩 해보고 안되면 끝!
// 모든 경우를 체크하지 않는다.

function gridChallenge(grid) {
  for (let row = 0; row < grid.length; ++row) {
    grid[row] = grid[row].split("").sort().join("");
  }

  for (let col = 0; col < grid[0].length; ++col) {
    for (let row = 0; row < grid.length - 1; ++row) {
      if (grid[row][col] > grid[row + 1][col]) return "NO";
    }
  }
  return "YES";
}
