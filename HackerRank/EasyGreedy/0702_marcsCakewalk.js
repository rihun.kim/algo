// Complete the marcsCakewalk function below.
// 아이디어
// 이것도 그리디 문제이다.
// 최소한의 칼로리를 구하기 위해서
// 모든 경우를 다 구하고 계산하면
// 시간이 너무 걸림
// 가장 큰 것을 가장 작은 것이랑 곱해서 최소화 시키면서
// 계산 하면, 1번의 계산으로 끝냄
// 따라서 그리디 문제이다.
// sort 한다고 해서, 그리디가 아닌 것이 아니다.
// 그리디에서 sort 는 디폴트로 해야될 수 도 있음을 명심
// 접근 방식이 그리디 적이라는 것을 명심

function marcsCakewalk(calorie) {
  calorie = calorie.sort((a, b) => b - a);

  let sum = 0;
  for (let i = 0; i < calorie.length; ++i) {
    sum += Math.pow(2, i) * calorie[i];
  }

  return sum;
}
