// Complete the luckBalance function below.
// 아이디어
// 럭키의 최대값을 구하라.
// 중요하지 않은 문제는 무조건 틀려야 럭키가 올라감
// 문제는 중요한 문제중 풀것을 고르는 것
// 그리디 측면에서 바라보면,
// 당연히 럭키가 적은 순서대로 풀면됨
// sort 시켜서 고르면 됨
function luckBalance(k, contests) {
  let sum = 0;
  let arr = [];
  for (let ele of contests) {
    if (ele[1] == 1) {
      arr.push(ele[0]);
    } else {
      sum += ele[0];
    }
  }

  arr = arr.sort((a, b) => a - b);
  for (let i = 0; i < arr.length - k; ++i) {
    sum -= arr[i];
  }
  arr.splice(0, arr.length - k);
  arr.map((a) => (sum += a));
  return sum;
}
