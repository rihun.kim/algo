// 그리디인 이유
// 최종적인 결과물을 위해 도출할 때,
// 각 인덱스 별로 최대값을 계속 찾아서 넣는 것이 아니라
// 위 방식은 브루트포스 방식임
// 그리디적인 경우는
// 가장 최적값을 미리 알고 있는 상태여야 한다.
// 따라서 거기 위치에 무엇이 올지 알고 있고,
// 문제의 요건상 스왑하라는 갯수를 제공하였으니,
// 스왑하면서 해결해야 한다.
// 최적의 상태의 결과값과 현재의 상태를 1대1로
// 비교하면서 풀이하게 되므로,
// 브루트 포스 방식이 아니게 된다.

// 보고 배울 코드
function largestPermutation(k, arr) {
  // 새 정렬 배열 만들기
  const sorted = [...arr].sort((a, b) => b - a);
  // swap 함수
  // [a,b] = [b,a] 이런식으로 써서, 서로 바꿈
  const swap = (a, b) => ([arr[a], arr[b]] = [arr[b], arr[a]]) && k--;

  for (var i = 0; i < arr.length && k > 0; i++) {
    if (arr[i] === sorted[i]) continue;

    // indexOf 가 처음부터 찾는 것을 막기위해
    // 두번째 파라미터로 찾기 시작할 위치 보냄
    swap(i, arr.indexOf(sorted[i], i));
  }

  return arr;
}

// 좀더 빠른 코드
function largestPermutation(k, arr) {
  let sortedArr = [];
  for (let i = arr.length; i > 0; --i) sortedArr.push(i);

  if (k > arr.length) return sortedArr;

  for (let i = 0; i < k && i < arr.length; ++i) {
    if (sortedArr[i] != arr[i]) {
      //swap
      let index = arr.indexOf(sortedArr[i], i);
      let temp = arr[i];
      arr[i] = arr[index];
      arr[index] = temp;
    } else if (sortedArr[i] == arr[i]) {
      ++k;
    }
  }

  return arr;
}
