function jimOrders(orders) {
  for (let i = 0; i < orders.length; ++i) {
    orders[i].push(orders[i][0] + orders[i][1], i + 1);
  }
  orders.sort((a, b) => a[2] - b[2]);

  return orders.map((a) => a[3]);
}
