// 아이디어
// 그리디 문제
// 이것도 동일하게 정렬해서
// 하나씩 가져와서 계산하면
// 최적의 결과를 만듬.

function maximumToys(prices, k) {
  prices = prices.sort((a, b) => a - b);

  let cnt = 0;
  for (let ele of prices) {
    if (k < ele) break;
    k -= ele;
    cnt++;
  }

  return cnt;
}
