// 아이디어
// 그리디 문제
// 모든 경우를 다 계산할 필요가 없음
// 정렬해서 앞부분부터 잘라도, 최적의 값 결과가 나옴

function toys(w) {
  w = w.sort((a, b) => a - b);

  let startWegith = w[0];
  let cont = 1;
  for (let i = 0; i < w.length; ++i) {
    if (w[i] > startWegith + 4) {
      startWegith = w[i];
      console.log(startWegith);
      ++cont;
    }
  }

  return cont;
}
