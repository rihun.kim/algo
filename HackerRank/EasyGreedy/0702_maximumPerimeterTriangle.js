// Complete the maximumPerimeterTriangle function below.
// 그리디
// 모든 경우를 체크하는 것은 너무 오래 걸림
// 정렬해서
// 큰 거 3개씩 묶어서 하면 최적의 해가 나옴
// 세변의 합이 가장 큰 것이 답이므로,
// 이처럼 그리디는
// 구현은 쉽지만, 논리적으로 최적의 합이 될 수 있음을
// 증명하는 것이 중요
function maximumPerimeterTriangle(sticks) {
  sticks.sort((a, b) => b - a);
  for (let i = 0; i < sticks.length - 2; ++i) {
    if (sticks[i] < sticks[i + 1] + sticks[i + 2]) {
      return [sticks[i + 2], sticks[i + 1], sticks[i]];
    }
  }
  return [-1];
}
