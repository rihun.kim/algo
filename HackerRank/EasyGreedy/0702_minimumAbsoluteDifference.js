// Complete the minimumAbsoluteDifference function below.
// 아이디어
// 그리디적으로 생각하자
// 값이 차이가 절대값으로 표현했을 때, 가장 작은 걸 찾아야한다.
// 그리디적으로
// 양수와 양수의 관계에선, 비등비등한 놈끼리 빼야 된다. (V)
// 음수와 음수의 관계에선, 비등비등한 놈끼리 빼야 한다. (V)
// 음수와 양수의 관계에선, 앞에 있건, 뒤에 있건 무조건 합이 이루어진다.
// 배열이 있을 때
// 정렬하고,
// 양수끼리 계산
// 음수끼리 계산
// 최소값 찾기
// 서로 앞뒤로만 해주면됨
// 0은 모든 공간에 포함
// 그것이 그리디
// DP 로 하였다면, 모든 요소에 대한 n * n 이 이루어져
// 오래걸림
// 그리디로 통하므로, 정렬만 해놓고, 위 아래 값만 비교해서
// 최소값이 정답이 되므로, 모든 경우를 안하고,
// 현재의 것만 최대를 된 것을 고르므로 그리디.

function minimumAbsoluteDifference(arr) {
  arr = arr.sort((a, b) => a - b);

  let min = Infinity;
  for (let i = 0; i < arr.length - 1; ++i) {
    let val = Math.abs(arr[i] - arr[i + 1]);
    if (min > val) min = val;
  }
  return min;
}
