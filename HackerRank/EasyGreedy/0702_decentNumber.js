// Complete the decentNumber function below.
// 아이디어
// 그리디 알고리즘 문제이며, 잔돈 구하기 유형이다.
// 먼저 수를 3으로 완전히 나누던가
// 5의 배수가 남도록 나눈다
// 3보다 적은 경우는 -1
// 3으로 완전 안나눠지면, 그 다음에 5로 나눠봄..
// 위에 처럼 하면 안된다!
// 그리디는 문제 푸는 방식을 바꾼다..
// 즉, 3으로 다 채우고, 그 다음에 5을 뒤에서 부터
// 삽입하는 방식으로 해결할 것!

function decentNumber(n) {
  if (n < 3) {
    console.log(-1);
    return;
  }

  let num = "";
  let fiveCnt = n;
  for (let i = 1; i <= n; ++i) num += "5";

  if (n % 3 == 0) {
    console.log(num);
    return;
  }

  let threeCnt = 0;
  while (fiveCnt > 0) {
    fiveCnt -= 5;
    ++threeCnt;
    if (fiveCnt >= 0 && fiveCnt % 3 == 0) {
      let res = "";
      for (let i = 1; i <= fiveCnt; ++i) {
        res += "5";
      }

      for (let i = 1; i <= threeCnt * 5; ++i) {
        res += "3";
      }
      console.log(res);
      return;
    }
  }

  console.log(-1);
  return;
}
