function getMinimumCost(k, c) {
  let sum = 0;
  let n = k;
  c = c.sort((a, b) => b - a);
  for (let i = 0; i < c.length; ++i) {
    if (k != 0) {
      --k;
      sum += c[i];
    } else {
      sum += (parseInt(i / n) + 1) * c[i];
    }
  }

  return sum;
}
