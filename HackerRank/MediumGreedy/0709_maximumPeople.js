function maximumPeople(p, x, y, r) {
  // Return the maximum number of people that will be in a sunny town after removing exactly one cloud.
  // p CityPeople, x CityPosition, y CloudPosition, r Cloud Range
  // CityMap[CityPosition] = [Cloud Count which covers this city, People Count]
  let cityMap = new Map();
  for (let i = 0; i < x.length; ++i) {
    if (!cityMap[x[i]]) cityMap[x[i]] = [0, p[i]];
    else cityMap[x[i]][1] += p[i];
  }

  // Make Cloud Range
  let cloudRanges = [];
  for (let i = 0; i < y.length; ++i) {
    let cloudLeft = y[i] - r[i] < 0 ? 0 : y[i] - r[i];
    cloudRanges.push([cloudLeft, y[i] + r[i]]);
  }

  // CloudMap[CLoudRange] = [CityPosition whis is covered by this cloud]
  let cloudMap = new Map();
  x = [...new Set(x)];
  for (let i = 0; i < x.length; ++i) {
    for (let cloudRange of cloudRanges) {
      let cloudLeft = cloudRange[0] < 0 ? 0 : cloudRange[0];
      if (cloudLeft <= x[i] && x[i] <= cloudRange[1]) {
        cityMap[x[i]].push([cloudLeft, cloudRange[1]]);
        if (!cloudMap[JSON.stringify([cloudLeft, cloudRange[1]])])
          cloudMap[JSON.stringify([cloudLeft, cloudRange[1]])] = [x[i]];
        else cloudMap[JSON.stringify([cloudLeft, cloudRange[1]])].push(x[i]);
        ++cityMap[x[i]][0];
      }
    }
  }

  let sum = 0;
  // COunting city peoplr which is not covered by cloud
  for (let place in cityMap) if (cityMap[place][0] == 0) sum += cityMap[place][1];

  let maxSum = 0;
  for (let place in cityMap) {
    let temp = 0;
    if (cityMap[place][0] == 1) {
      // counting city of people which is covered by only one cloud
      for (let city of cloudMap[JSON.stringify(cityMap[place][2])]) {
        if (cityMap[city][0] == 1) temp += cityMap[city][1];
      }
    }
    maxSum = maxSum < temp ? temp : maxSum;
  }

  return (sum += maxSum);
}
