function maxMin(k, arr) {
  arr = arr.sort((a, b) => a - b);

  let min = arr[k - 1] - arr[0];
  for (let i = 0; i + k - 1 < arr.length; ++i) {
    if (min > arr[i + k - 1] - arr[i]) {
      min = arr[i + k - 1] - arr[i];
    }
  }

  return min;
}
