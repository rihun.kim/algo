// 아이디어
// 새로 접한 문제의 컨셉
// 한 위치에서 양쪽을 봐야한다는 조건이라면
// 앞에서부터 쭉 보고, 뒤에서부터 쭉 보는 방식으로
// 그리디적으로 풀어야함
// 한 위치에서 양쪽을 보는 방식으론 풀 수 없음
// 컨텍스트가 축적되지 않은 상태에서 계산을 하므로..

function candies(n, arr) {
  let a = 1,
    c = [a],
    a1 = 1,
    cost = 0;

  for (let i = 1; i < arr.length; ++i) {
    if (arr[i - 1] < arr[i]) {
      a++;
    } else {
      a = 1;
    }
    c.push(a);
  }

  for (let i = arr.length - 1; i >= 0; --i) {
    if (a1 > c[i]) {
      c.splice(i, 1, a1);
    }

    if (arr[i - 1] > arr[i]) {
      a1++;
    } else {
      a1 = 1;
    }
  }

  return c.reduce((prev, curr) => prev + curr);
}
