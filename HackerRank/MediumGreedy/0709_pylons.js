// 아이디어
// 좋은 그리디 문제
// 처음 접하면 못풀 수 있는 문제이며,
// 그리디적인 접근으로 해야 풀기가 수월
// 모든 경우를 조합하는 완전탐색을 실패하기 좋음
// 그리디 문제의 특징인 새로운 배열을 만들어서
// 값을 기록하는 특징
// 값의 경우는 최적화된 경우들을 기억하는 배열
// 그리고 이 배열을 잘 활용하는 로직 필요

function pylons(k, arr) {
  let prev = [];
  let last = -1;
  for (let i = 0; i < arr.length; ++i) {
    if (arr[i] == 1) last = i;
    prev[i] = last;
  }

  let ans = 0;
  let take;
  for (let i = 0; i < arr.length; ) {
    take = prev[Math.min(i + k - 1, arr.length - 1)];
    // k 를 그냥 쓸 때와 -1 해서 쓸때가 다름임을 기억
    // k 를 그냥 쓰면 파워플랜트의 다음 위치를 의미하고
    // k -1 하면 파워플랜트가 적용해주는 범위를 의미
    if (take == -1 || take + k <= i) return -1;
    i = take + k;
    ++ans;
  }
  return ans;
}
