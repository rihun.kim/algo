// 아이디어

function match(g, p) {
  const len = g.length - p.length;

  // console.log(g, p);
  // console.log("lgen", g.length);
  // console.log("len", len);

  let pcol = 0;
  let arr = [];
  for (let k = 0; k <= g.length; ++k) {
    pcol = 0;
    if (g[k] == p[pcol]) {
      // console.log(k);
      ++pcol;
      for (let i = k + 1; i <= g.length; ++i) {
        if (g[i] == p[pcol]) {
          ++pcol;
          if (pcol == p.length) {
            // console.log("pco", pcol);
            arr.push(k);
          }
        } else {
          pcol = 0;
        }

        if (pcol == 0 && g[i] == p[pcol]) {
          ++pcol;
        }
      }
    }
  }

  if (arr.length != 0) return [...new Set(arr)];
  return false;
}

function afterMatch(G, gStart, gEnd, start, P) {
  // console.log("st", start);
  let prow = 1;
  // console.log(gStart, gEnd);
  for (let i = gStart; i < gStart + gEnd; ++i) {
    // console.log(G[i].substr(start, P[0].length));
    if (G[i].substr(start, P[0].length) != P[prow]) {
      // console.log(G[i].substr(start, P[0].length), P[prow]);
      // console.log("ddd");

      return false;
    }
    prow++;
  }
  return true;
}

function gridSearch(G, P) {
  const Glen = G.length;
  const Plen = P.length;

  let prow = 0;
  for (let row = 0; row <= Glen - Plen; ++row) {
    let start = match(G[row], P[prow]);

    if (start) {
      // console.log(start);
      for (let ele of start) {
        // console.log("hi");
        if (Plen == 1) return "YES";
        if (afterMatch(G, row + 1, Plen - 1, ele, P)) {
          return "YES";
        }
      }
    }
  }
  return "NO";
}

const shortTest = () => {
  const G = [
    "111111111111111",
    "111111111111111",
    "111111011111111",
    "111111111111111",
    "111111111111111",
  ];
  const P = [
    // "11111",
    "11111",
    "11110",
  ];

  console.log(gridSearch(G, P));
};

const test = () => {
  // for (let i = 0; i < input.input.length; ++i) {
  //   let myAns = biggerIsGreater(input.input[i]);
  //   if (answer.answer[i] != myAns) {
  //     console.log(input.input[i], answer.answer[i], myAns);
  //   }
  // }
};

shortTest();
// test();
