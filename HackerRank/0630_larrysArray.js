// 아이디어
// 1부터 앞으로 앞으로 보내기
// 각 숫자별로 위치를 잡고 세 숫자를 잡은뒤, 앞으로 보내기
// 종료조건이 중요
// 1 부터 시작하기
// 전진전략은 2개
// 1번 자신의 위치에서 두개앞을 잡기
// 2번 자신의 위치에서 1개 앞과 1개 뒤 잡기
// 위치 옮길 때마다 위치 비교 수행
// 이거 모두 못하면 반복문 나간뒤, 위치값 확인후 false 여부 결정

function larrysArray(A) {
  for (let index = 1; index <= A.length; ++index) {
    if (A.indexOf(index) + 1 == index) {
      continue;
    } else {
      // console.log(index, A.indexOf(index));
      while (A.indexOf(index) + 1 != index) {
        // console.log("hi");
        // 두칸잡기
        // 잡을때, 범위를 생각
        let fix = A.indexOf(index);
        if (index - 1 <= fix - 2) {
          // console.log("2칸");
          let temp = A[fix - 2];
          A[fix - 2] = A[fix - 1];
          A[fix - 1] = A[fix];
          A[fix] = temp;
        } else if (index - 1 <= fix - 1 && fix + 1 < A.length) {
          // 한칸앞뒤로 잡기
          // console.log("1칸");
          let temp = A[fix - 1];
          A[fix - 1] = A[fix];
          A[fix] = A[fix + 1];
          A[fix + 1] = temp;
        } else {
          break;
        }
      }
      // console.log(A);
      if (A.indexOf(index) + 1 != index) {
        return "NO";
      }
    }
  }
  return "YES";
}
