// 주어진 K 에 대하여, N 을 K 로 나눈 나머지들의 갯수를 나열하기 위한 배열 F 을 생성
// N 각각을 K 로 나눈 나머지들의 갯수로 F 배열을 생성
// 이 과정에서 N 이 K 로 완전히 나눠지는 경우가 여러개인 경우는(나머지가 0인 경우) 1 로, 없으면 0 으로
// 왜 1로 표현했나면, 최종적인 답을 제출하는 배열에서 N 이 K 로 완전히 나눠지는 경우는 한 개 밖에 들어갈 수 밖에 없기 때문
// 두 개 이상이 들어가면, K 로 나눠지는 경우가 발생하기 떄문
// 그 다음 중요한 경우는 바로 K 가 짝수인 경우
// 이 경우도 마찬가지로 F[K/2] 의 경우는 무조건 하나밖에 못들어가기 때문
// 왜냐하면, 나중에 조합상으로 F[K/2] 의 원소 하나와 F[K/2] 의 원소가 만나면 무조건 K 로 나눠질 수 밖에 없는 상황이므로...
// 마지막은 Max(F[i==1], F[K-i]) 이런식으로 찾아야함.
// 이는 공존할 수 없는 놈끼리 최종적인 배열에 같이 들어가면 안되기 때문
// 공존 불가, 예를 들면, K = 6 이면, Max(K[2], K[4]), Max(K[1], K[5]) 2와4, 1과5, 3과3은 공존불가 왜냐하면,
// 둘의 함이 K 로 나눠지기 때문임
function nonDivisibleSubset(k, s) {
  // Write your code here
  let map = new Map();
  let ans = 0;

  for (let i = 0; i < k; ++i) map[i] = 0;
  for (let i = 0; i < s.length; ++i) map[s[i] % k]++;

  map[0] = map[0] >= 1 ? 1 : 0;
  ans += map[0];

  if (k % 2 == 0) map[k / 2] = map[k / 2] >= 1 ? 1 : 0;
  for (let i = 1; i <= k / 2; ++i) ans += Math.max(map[i], map[k - i]);

  return ans;
}
