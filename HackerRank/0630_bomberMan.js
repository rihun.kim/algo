function bomberMan(n, gridMatrix) {
  let R = gridMatrix.length;
  let C = gridMatrix[0].length;

  // 계산을 위한 배열화
  let grid = [];
  for (let row of gridMatrix) grid.push(row.split(""));

  // 초기 세팅
  for (let y = 0; y < R; ++y) {
    for (let x = 0; x < C; ++x) {
      if (grid[y][x] == "O") grid[y][x] = 2;
    }
  }

  n--;
  if (n > 0) {
    n %= 4;
    if (n == 0) n = n + 4;
  }

  let moveY = [0, 0, 1, -1];
  let moveX = [1, -1, 0, 0];
  let isActive = true;
  while (n-- > 0) {
    // 터뜨릴 폭탄 준비 및 기존 폭탄 시간 감소
    if (isActive) {
      for (let y = 0; y < R; ++y) {
        for (let x = 0; x < C; ++x) {
          if (grid[y][x] > 0) {
            --grid[y][x];
          } else {
            grid[y][x] = 3;
          }
        }
      }
      isActive = !isActive;
    } else {
      // 폭탄 구경 및 기존 폭탄들 시간 감소
      // 폭탄 준비 및 기존 폭탄들 시간 감소
      let bombs = [];
      for (let y = 0; y < R; ++y) {
        for (let x = 0; x < C; ++x) {
          if (--grid[y][x] == 0) {
            bombs.push([y, x]);
          }
        }
      }
      // 폭탄 터트리기
      for (let bomb of bombs) {
        for (let i = 0; i < 4; ++i) {
          let newY = bomb[0] + moveY[i];
          let newX = bomb[1] + moveX[i];
          if (0 <= newY && newY < R && 0 <= newX && newX < C) {
            grid[newY][newX] = 0;
          }
        }
      }
      isActive = !isActive;
    }
  }

  for (let y = 0; y < R; ++y) {
    for (let x = 0; x < C; ++x) {
      grid[y][x] = grid[y][x] > 0 ? "O" : ".";
    }
  }

  let ans = [];
  for (let row = 0; row < grid.length; ++row) {
    ans.push(grid[row].join(""));
  }

  return ans;
}
