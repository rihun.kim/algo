function bonAppetit(bill, k, b) {
  let sum = 0;
  for (let i = 0; i < bill.length; ++i) {
    if (i != k) {
      sum += bill[i];
    }
  }

  console.log(sum / 2 == b ? "Bon Appetit" : b - sum / 2);
}
