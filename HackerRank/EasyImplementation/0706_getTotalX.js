function gcd(a, b) {
  while (b != 0) {
    let temp = a % b;
    a = b;
    b = temp;
  }
  return a;
}

function lcm(a, b) {
  return (a * b) / gcd(a, b);
}

function getTotalX(a, b) {
  let aGcd;
  if (a.length != 1) {
    aGcd = gcd(a[0], a[1]);
  } else {
    aGcd = a[0];
  }

  let num;
  if (a.length != 1) {
    num = lcm(a[0], a[1]);
    console.log(a[0], a[1], num);
    for (let i = 2; i < a.length; ++i) {
      num = lcm(num, a[i]);
    }
  } else {
    num = a[0];
  }

  let arr = [];
  for (let i = 1; i * num <= b[0]; ++i) {
    arr.push(i * num);
  }

  let sum = 0;
  for (let divisor of arr) {
    let is = true;
    for (let bb of b) {
      if (bb % divisor != 0) {
        is = false;
      }
    }
    if (is) ++sum;
  }

  return sum;
}
