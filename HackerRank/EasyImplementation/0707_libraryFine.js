function libraryFine(d1, m1, y1, d2, m2, y2) {
  // d1 returned
  // d2 due
  let y3 = y1 - y2;
  let m3 = m1 - m2;
  let d3 = d1 - d2;

  if (y3 < 0 || (y3 == 0 && m3 < 0) || (y3 == 0 && m3 == 0 && d3 <= 0)) {
    return 0;
  } else if (y3 == 0 && m3 == 0) {
    return d3 * 15;
  } else if (y3 == 0) {
    return m3 * 500;
  } else {
    return 10000;
  }
}
