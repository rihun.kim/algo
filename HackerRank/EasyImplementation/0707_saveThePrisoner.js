function saveThePrisoner(n, m, s) {
  let res = (s + m - 1) % n;
  return res == 0 ? n : res;
}
