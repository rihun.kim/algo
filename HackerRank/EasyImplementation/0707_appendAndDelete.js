function appendAndDelete(s, t, k) {
  // 만들어야 하는 갯수
  let makeNum = 0;
  for (let i = 0; i < t.length; ++i) {
    if (s[i] != t[i]) {
      makeNum = t.length - i;
      break;
    }
  }

  // 지워야할 갯수
  let delNum = 0;
  for (let i = 0; i < s.length; ++i) {
    if (s[i] != t[i]) {
      delNum = s.length - i;
      break;
    }
  }

  if (makeNum + delNum == k) return "Yes";
  if (delNum > k) return "No";

  k -= delNum;
  delNum = 0;
  if (makeNum == k) return "Yes";
  if (makeNum > k) return "No";
  while (true) {
    if (makeNum == t.length) null;
    else if (makeNum != 0) ++makeNum;
    --k;
    if (makeNum == k) return "Yes";
    if (k == 0) return "No";
  }
}

// 좋은 풀이
// 루프를 이용해서 풀어야 될 것 같은 문제를
// 로직으로 풀게함
function appendAndDelete(s, t, k) {
  let i = 0;
  // 작은 놈이 기준이 되므로,
  while (i < Math.min(s.length, t.length)) {
    if (s[i] != t[i]) break;
    i += 1;
  }

  // 공통이 글자만 빼내는 과정
  // 지우고, 추가해야할 문자들의 갯수를 구하는 과정
  let move = s.length + t.length - 2 * i;

  // 주어진 갯수 k 와 필요한 갯수 빼는 과정
  let remain = k - move;

  console.log(remain, move, i);

  // remain 0보다 작으면, 더 필요한대, 없는 상황이므로 false
  // 추가적으로 움직여야 할 경우를 고려
  // 홀수개가 남을 경우가 고려 필요,
  // 홀수개가 남아도 싹다 지우고, 계속 지운다음(0에서 지워도 0이므로, 횟수를 그냥 없애는 거 가능: 문제규칙)
  // 에 회복할 수 있다는 것을
  // 대신 remain 이 싹다 지우고 회복할 만큼 있어야 하므로,
  // 그것이 없다면 No 결과!
  // remain 이 짝수면 무조건 yes
  if (remain < 0 || (remain % 2 == 1 && remain < 2 * i)) return "No";
  return "Yes";
}
