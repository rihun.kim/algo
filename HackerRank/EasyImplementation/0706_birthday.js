function birthday(s, d, m) {
  // m 은 조각
  // d 는 합산

  let cnt = 0;
  let sum = 0;
  for (let i = 0; i < s.length - m + 1; ++i) {
    for (let j = i; j < i + m; ++j) {
      sum += s[j];
    }
    if (sum == d) ++cnt;
    sum = 0;
  }
  return cnt;
}
