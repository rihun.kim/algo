function dayOfProgrammer(year) {
  // leap year 인지 판단
  // 1700 - 1917 인지 판단
  // 1918 인지 판단
  // 1919 - 판단

  let months = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  if (1700 <= year && year <= 1917) {
    if (year % 4 == 0) ++months[1];
    let sumDays = 0;
    for (let month of months) {
      if (sumDays + month > 256) {
        break;
      } else {
        sumDays += month;
      }
    }
    let date = 256 - sumDays;
    return date + "." + "09." + year;
  } else if (year == 1918) {
    return "26.09.1918";
  } else {
    if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
      ++months[1];
    }
    let sumDays = 0;
    for (let month of months) {
      if (sumDays + month > 256) {
        break;
      } else {
        sumDays += month;
      }
    }
    let date = 256 - sumDays;
    return date + "." + "09." + year;
  }
}
