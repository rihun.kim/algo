function strangeCounter(t) {
  let curTime;
  let nextTime;
  for (let i = 1; ; ++i) {
    curTime = 3 * Math.pow(2, i - 1) - 2;
    nextTime = 3 * Math.pow(2, i) - 2;
    if (curTime <= t && t < nextTime) {
      break;
    }
  }

  return 2 * curTime + 2 - t;
}
