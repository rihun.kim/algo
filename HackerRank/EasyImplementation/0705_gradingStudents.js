function gradingStudents(grades) {
  for (let i = 0; i < grades.length; ++i) {
    if (grades[i] >= 38) {
      let startNum = 40;
      while (true) {
        if (grades[i] < startNum) {
          if (startNum - grades[i] < 3) {
            grades[i] = startNum;
          }
          break;
        }
        startNum += 5;
      }
    }
  }
  return grades;
}

function gradingStudents(grades) {
  return grades.map((grade) => {
    let diff = 5 - (grade % 5);
    if (diff < 3 && grade >= 38) {
      grade += diff;
    }
    return grade;
  });
}
