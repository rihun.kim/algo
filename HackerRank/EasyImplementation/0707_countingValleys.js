function countingValleys(n, s) {
  let level = 0;
  let cnt = 0;
  let valley = false;
  for (let i = 0; i < s.length; ++i) {
    if (s[i] == "U") {
      ++level;
      if (level == 0) valley = true;
    } else {
      if (level > 0) {
        valley = false;
      } else if (level == 0) {
        if (valley == false) valley = false;
        else valley = true;
      } else {
        valley = true;
      }
      --level;
    }

    if (level == 0 && valley) {
      valley = false;
      ++cnt;
    }
  }
  return cnt;
}
