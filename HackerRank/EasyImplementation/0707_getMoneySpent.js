function getMoneySpent(keyboards, drives, b) {
  let max = 0;
  for (let keyboard of keyboards) {
    for (let drive of drives) {
      let total = keyboard + drive;
      if (total == b) {
        return b;
      } else if (total < b && total > max) {
        max = keyboard + drive;
      }
    }
  }

  return max == 0 ? -1 : max;
}
