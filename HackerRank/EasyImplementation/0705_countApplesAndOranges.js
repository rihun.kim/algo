function countApplesAndOranges(s, t, a, b, apples, oranges) {
  let applesCnt = 0;
  apples.map((apple) => {
    if (s <= a + apple && a + apple <= t) ++applesCnt;
  });

  let orangesCnt = 0;
  oranges.map((orange) => {
    if (s <= b + orange && b + orange <= t) ++orangesCnt;
  });

  console.log(applesCnt);
  console.log(orangesCnt);
}
