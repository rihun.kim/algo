function breakingRecords(scores) {
  let maxScore = scores[0];
  let minScore = scores[0];

  let maxCnt = 0;
  let minCnt = 0;
  for (let ele of scores) {
    if (ele > maxScore) {
      ++maxCnt;
      maxScore = ele;
    } else if (ele < minScore) {
      ++minCnt;
      minScore = ele;
    }
  }

  return [maxCnt, minCnt];
}
