function migratoryBirds(arr) {
  let map = new Map();

  for (let ele of arr) {
    if (!map[ele]) {
      map[ele] = 1;
    } else {
      ++map[ele];
    }
  }

  let max = 0;
  let maxIndex = 0;
  for (let key in map) {
    if (max < map[key]) {
      maxIndex = key;
      max = map[key];
    } else if (max == map[key]) {
      if (maxIndex > key) {
        maxIndex = key;
      }
    }
  }

  return maxIndex;
}
