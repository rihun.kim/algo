function sockMerchant(n, ar) {
  let map = new Map();

  let sum = 0;
  for (let ele of ar) {
    if (map[ele] == 0 || !map[ele]) {
      map[ele] = 1;
    } else if (map[ele] == 1) {
      map[ele] = 0;
      ++sum;
    }
  }

  return sum;
}
