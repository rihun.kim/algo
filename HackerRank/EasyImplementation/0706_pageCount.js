function pageCount(n, p) {
  return parseInt(p / 2) <= parseInt((n - p) / 2)
    ? parseInt(p / 2)
    : n % 2 == 0 && n - p == 1
    ? parseInt((n - p) / 2) + 1
    : parseInt((n - p) / 2);
}
