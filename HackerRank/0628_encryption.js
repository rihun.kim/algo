function encryption(s) {
  let row = Math.floor(Math.sqrt(s.length));
  let col = Math.ceil(Math.sqrt(s.length));

  let arr = [];
  for (let i = 0; i < s.length; ) {
    arr.push(s.slice(i, i + col));
    i += col;
  }

  let ans = "";
  for (let i = 0; i <= row; ++i) {
    ans += arr.map((row) => row[i]).join("") + " ";
  }

  return ans.slice(0, -1);
}
