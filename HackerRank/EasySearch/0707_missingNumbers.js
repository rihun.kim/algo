function missingNumbers(arr, brr) {
  let Amap = new Map();
  let Bmap = new Map();

  for (let ele of arr) {
    if (!Amap[ele]) Amap[ele] = 1;
    else ++Amap[ele];
  }

  for (let ele of brr) {
    if (!Bmap[ele]) Bmap[ele] = 1;
    else ++Bmap[ele];
  }

  let ans = [];
  for (let key in Bmap) {
    if (Bmap[key] != Amap[key]) ans.push(key);
  }

  return ans;
}
