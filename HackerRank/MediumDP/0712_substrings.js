// 아이디어
// DP 보다, 수학적 특징이 더 들어간 문제
// 굉장히 큰 수에 대한 접근하는 방법에 대한 고민 필요 (BigInt, n)
// 최적화하면, DP 특성은 거의 사라짐....

function substrings(n) {
  n = "" + n;
  let mod = 1000000007n;

  let sum = 0n;
  let ones = 1n;
  let len = n.length;
  for (let i = 0; i < len; ++i) {
    sum = (sum + BigInt(n[len - i - 1]) * BigInt(len - i) * BigInt(ones)) % mod;
    ones = (ones * 10n + 1n) % mod;
  }

  return sum % mod;
}
