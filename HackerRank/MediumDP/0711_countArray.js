// 아이디어
// dp 을 위한 배열을 두개를 만들어서
// 각 각의 관계를 통한 DP 식을 완성함
// a 는 x 로 끝나는 경우의 수로 이루어진 배열
// b 는 x 로 안 끝나는 경우의 수로 이루어진 배열
// (k-1) * a[i-1] 은 x 가 아닌 경우로 현재의 위치를 채울 수 있는 갯수 * 앞의 수가 X 인 경우의 갯수
// (k-2) * b[i-1] 은 x, 현재 자신의 수 가(2개) 아닌 경우로 채울 수 있는 갯수  * 앞의 수가 X 가 아닌 경우의 갯수
// DP 는 수학식을 잘 세우면, 코드는 단순헤짐

function countArray(n, k, x) {
  // Return the number of ways to fill in the array.
  // n 은 길이, k 는 나올 수 있는 최대 수, 마지막은 x
  let a = [x == 1 ? 1 : 0];
  let b = [x == 1 ? 0 : 1];
  let mod = 1000000007;

  for (let i = 1; i < n; ++i) {
    a[i] = b[i - 1] % mod;
    b[i] = (((k - 1) * a[i - 1]) % mod) + (((k - 2) * b[i - 1]) % mod);
  }

  return a[n - 1];
}
