// 아이디어
// 재미있는 문제였다
// 생각의 틀을 약간 바꾼 문제였다.
// 문제의 핵심은 뒤에서 부터 생각하는 것이였다.
// 앞에서 부터 생각해서 풀면, 뒤의 경우의 수가 너무 다양해질 뿐더러
// dp 의 최초의 최적의 값을 설정하는데 문제가 많아진다.
// 따라서 뒤에서 부터 최초의 최적의 값을 설정하고
// 그 다음 DP 로직을 구성한다.
// 재미난 점은 문제에서 두 플레이어 모두
// 최적의 경우로 움직인다고 했다는 점이다.
// 때문에 DP 가 가능해지는 특징이 생겼다.

function bricksGame(arr) {
  let sum = [arr[arr.length - 1]];
  let dp = [[]];
  for (let i = arr.length - 2; i >= 0; --i) {
    sum.unshift(arr[i] + sum[0]);
    dp.push([]);
  }

  for (let i = arr.length - 1; i >= 0; --i) {
    if (i + 3 >= arr.length) {
      let num = !dp[i + 1] ? 0 : dp[i + 1][0];
      dp[i][0] = arr[i] + num;
      dp[i][1] = dp[i][0];
      continue;
    }

    dp[i][0] = sum[i] - Math.min(dp[i + 1][1], dp[i + 2][1], dp[i + 3][1]);
    dp[i][1] = dp[i][0];
  }

  return dp[0][0];
}
