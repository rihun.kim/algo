// 계속 봐야할 코드
// 재귀적으로도 풀어볼것

function getWays(n, c) {
  c = c.sort((a, b) => a - b);
  let dp = [];

  for (let i = 0; i <= c.length; ++i) dp[i] = Array.from({ length: n + 1 }, () => 0);
  for (let i = 0; i <= c.length; ++i) dp[i][0] = 1;

  for (let types = 1; types <= c.length; ++types) {
    for (let cost = 1; cost <= n; ++cost) {
      dp[types][cost] =
        (cost - c[types - 1] < 0 ? 0 : dp[types][cost - c[types - 1]]) + dp[types - 1][cost];
    }
  }

  // console.log(dp)
  return dp[c.length][n];
}
