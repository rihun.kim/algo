// 아이디어
// 리트코드에서 풀었던 문제 유형이다.
// 차례대로 더할 때, 총합이 음수가 나오면...
// 0부터 다시 시작하는 것이 캐이득이다.

function maxSubarray(arr) {
  let seqSum = 0;
  let subSum = 0;
  let tempSum = 0;
  let isAllNeg = true;
  for (let i = 0; i < arr.length; ++i) {
    if (arr[i] > 0) {
      seqSum += arr[i];
      isAllNeg = false;
    }
    tempSum += arr[i];
    if (tempSum < 0) tempSum = 0;
    if (tempSum > subSum) subSum = tempSum;
  }

  if (isAllNeg) {
    let temp = Math.max(...arr);
    return [temp, temp];
  }

  return [subSum, seqSum];
}
