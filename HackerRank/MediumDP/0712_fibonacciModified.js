function fibonacciModified(t1, t2, n) {
  let dp = [];
  dp[1] = BigInt(t1);
  dp[2] = BigInt(t2);

  for (let i = 3; i <= n; ++i) {
    dp[i] = dp[i - 2] + dp[i - 1] ** 2n;
  }

  return dp[n];
}
