// 아이디어
// DP 중급이상은 2차원 배열을 써야 거의 풀린다.
// 식 자체는 어렵지 않음
// 문제의 요건상 해당하는 인덱스에서는 해당 인덱스 값이나
// 1이 써야된다는 아이디어가 필요함
// dp 식은
// dp[i][0] = i번째 인덱스까지 계산했을 때의 최대값 이고, 0은 i번째 인덱스에서
// 선택한 값이 자신의 B[i] 값임을 의미
// dp[i][1] 도 마찬가지지만 i번째 인덱스에서 1을 선택함을 의미

function cost(B) {
  let dp = [[]];
  let BB = [B[0]];
  dp[0][0] = B[0];
  dp[0][1] = 1;

  for (let i = 1; i < B.length; ++i) {
    BB[i] = B[i];

    dp[i] = [];
    if (i == 1) {
      dp[i][0] = Math.max(Math.abs(BB[i] - BB[i - 1]), Math.abs(BB[i] - 1));
      dp[i][1] = Math.abs(1 - BB[i - 1]);
    } else {
      dp[i][0] = Math.max(
        Math.abs(BB[i] - BB[i - 1]) + dp[i - 1][0],
        Math.abs(BB[i] - 1) + dp[i - 1][1]
      );
      dp[i][1] = Math.max(Math.abs(1 - BB[i - 1]) + dp[i - 1][0], dp[i - 1][1]);
    }

    // dp[i][0] = Math.max(
    //     Math.abs(BB[i]-BB[i-1]) + (i==1 ? 0 : dp[i-1][0]),
    //     Math.abs(BB[i]-1) + (i==1 ? 0 : dp[i-1][1])
    // );
    // dp[i][1] = Math.max(
    //     Math.abs(1-BB[i-1]) + (i==1 ? 0 : dp[i-1][0]),
    //     Math.abs(1-1) + (i==1 ? 0 : dp[i-1][1])
    // )
    // dp[i][0] = BB[i]-BB[i-1] + dp[i-1][0]
    // dp[i][0] = BB[i]-1 + dp[i-1][1]
    // dp[i][1] = 1-BB[i-1] + dp[i-1][0]
    // dp[i][1] = 1-1 + dp[i-1][1]
  }
  return Math.max(dp[B.length - 1][0], dp[B.length - 1][1]);
}
