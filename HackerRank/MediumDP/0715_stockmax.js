// 아이디어
// 뒤로 가면서 봐야 되는 문제이다.
// max_price 의 특성상, 순서대로 하면, 계속 비교와 들고 있어야되는 값이 많아짐
// 하지만 뒤에서 부터 시작하면, max_price 는 계속 유지 되기 때문에
// 자신보다 더 큰놈을 만나기 전까지는 유효하게 되고
// 유효한 모든 놈들과의 이익을 얻을 수 있다.
function stockmax(prices) {
  let max = prices[prices.length - 1];
  let sum = 0;
  for (let i = prices.length - 1; i >= 0; --i) {
    if (max < prices[i]) max = prices[i];
    sum += max - prices[i];
  }

  return sum;
}
