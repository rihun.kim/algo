// 아이디어
// 배낭문제의 응용문제..

function unboundedKnapsack(k, arr) {
  arr = arr.sort((a, b) => a - b);
  let dp = [Array.from({ length: k + 1 }, () => 0)];

  for (let i = 1; i <= arr.length; ++i) {
    dp[i] = [0];
    for (let j = 1; j <= k; ++j) {
      if (arr[i - 1] <= j) {
        dp[i][j] = Math.max(arr[i - 1] + dp[i][j - arr[i - 1]], dp[i - 1][j]);
      } else {
        dp[i][j] = dp[i - 1][j];
      }
    }
  }

  return dp[arr.length][k];
}
