let dp = [];
let map = new Map();
let visited = new Map();
function dfs(num) {
  visited[num] = true;
  let count = 0;
  for (let ele of map[num]) {
    if (!visited[ele]) {
      count += dfs(map, visited, ele);
      ++count;
    }
  }
  // console.log("num=",num,", count=",count)
  return count;
}

// Complete the kingdomDivision function below.
function kingdomDivision(n, roads) {
  for (let road of roads) {
    if (!map[road[0]]) map[road[0]] = [road[1]];
    else map[road[0]].push(road[1]);

    if (!map[road[1]]) map[road[1]] = [road[0]];
    else map[road[1]].push(road[0]);

    visited[road[0]] = false;
    visited[road[1]] = false;
  }

  return dfs(1);
}
