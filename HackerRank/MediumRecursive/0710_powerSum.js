// 아이디어
// 재귀문제이고
// 예를 들어
// (10, 1)  이라하면, bot 은 밑이라 생각하면 됨
// (10, 1) => (9, 2) & (10, 2)
// (9, 2) => (5, 3) & (9, 3)
// (10, 2) => (6, 3) & (10, 3)
// 이런식으로 나눠진다고 보면 된다.
// 주안점은 return 에 recurv 두방을 해서 원본을 그대로 넘기는 방법을 캐치!

function recurv(X, N, bot = 1) {
  let remainNum = X - Math.pow(bot, N);

  // 가지치기 하는 부분
  if (remainNum < 0) return 0;
  else if (remainNum == 0) return 1;
  else return recurv(remainNum, N, bot + 1) + recurv(X, N, bot + 1);
}

// Complete the powerSum function below.
function powerSum(X, N) {
  return recurv(X, N);
}
