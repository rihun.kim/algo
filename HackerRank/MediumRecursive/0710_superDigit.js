// 아이디어
// 문제는 쉬었으나,
// 기교가 필요함

function resursive(n) {
  if (n.length == 1) return Number(n);

  let temp = "" + n.split("").reduce((prev, curr) => Number(prev) + Number(curr));
  return resursive(temp);
}

// Complete the digitSum function below.
function superDigit(n, k) {
  let sum = resursive("" + n);
  sum *= k;

  return resursive("" + sum);
}
