// 111. Minimum Depth of Binary Tree

function dfs(root, i, stack) {
  // left node 조건을 잘 파악할 것
  if (root.left == null && root.right == null) {
    if (stack[0] == 0) stack[0] = i;
    else stack[0] > i ? (stack[0] = i) : false;

    return;
  }

  if (root.left != null) dfs(root.left, i + 1, stack);
  if (root.right != null) dfs(root.right, i + 1, stack);
}

var minDepth = function (root) {
  let i = 1;
  let stack = [0];

  if (root == null) return 0;
  dfs(root, i, stack);

  return stack[0];
};
