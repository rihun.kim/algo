// 13. Roman to Integer

var romanToInt = function (s) {
  let getIntFromRoman = {
    I: 1,
    V: 5,
    X: 10,
    L: 50,
    C: 100,
    D: 500,
    M: 1000,
  };

  let sum = 0;
  let curr, next;

  for (let i = 0; i < s.length - 1; ++i) {
    curr = getIntFromRoman[s[i]];
    next = getIntFromRoman[s[i + 1]];
    if (curr < next) {
      sum -= curr;
    } else {
      sum += curr;
    }
    // 규칙은 간단하다. 현재 위치에서 다음 위치와 비교해서 자신이 작으면 빼고, 크면 더한다
  }

  return sum + getIntFromRoman[s[s.length - 1]];
};
