// 아이디어
// 그리디인 이유
// 조합을 찾는 것이 아닌
// 1대1 비교를 통해서 결과 도출
// 전형적인 그리디 문제

var isSubsequence = function (s, t) {
  if (s.length == 0) return true;

  for (let i = 0, j = 0; i < t.length; ++i) {
    if (s[j] == t[i]) {
      ++j;
      if (j == s.length) return true;
    }
  }

  return false;
};
