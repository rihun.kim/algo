// 아이디어
// 조합을 찾지 않고
// 두 배열의 정렬을 통해
// 만족자의 수를 늘린다.
// 1대1 매칭을 이용함
// 여기도 maxmization 나옴

var findContentChildren = function (g, s) {
  g.sort((a, b) => a - b);
  s.sort((a, b) => a - b);

  let cnt = 0;
  for (let i = 0, j = 0; i < s.length; ++i) {
    if (g[j] <= s[i]) {
      ++j;
      ++cnt;
    }
  }

  return cnt;
};
