// 아이디어
// 문제를 해석하는 데 시간이 너무 걸렸음
// 문제를 설명을 잘 못했음
// 핵심은 홀수끼리의 이동은 코스트가 0 이고
// 짝수끼리의 이동은 코스트가 0 이다.
// 그리고 최종적으론 짝수와 홀수간의 이동이 필요하다
// 작은놈이 이동하는 것이 최적의 결과
// 홀수끼리의 이동은 매 순간의 선택이 최적임을 보장 (코스트가 0 이므로.)

var minCostToMoveChips = function (chips) {
  let oddCnt = 0;
  let evenCnt = 0;
  for (let i = 0; i < chips.length; ++i) {
    if (chips[i] % 2 == 0) ++evenCnt;
    else ++oddCnt;
  }

  return oddCnt > evenCnt ? evenCnt : oddCnt;
};
