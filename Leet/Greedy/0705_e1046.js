// 아이디어
// 그리디인 이유
// 내림차순으로 정렬해서
// 순서대로 빼면서 연산하면 최적의 결과가 나옴
// 위 행위가 매 순간의 선택이 최적의 결과를 발생
// 계속 최대의 값 두개를 일일히 찾아서 넣고
// 반복하는 과정은 비효율적

var lastStoneWeight = function (stones) {
  stones.sort((a, b) => b - a);

  while (stones.length > 1) {
    let y = stones.shift();
    let x = stones.shift();
    let num = y - x;

    if (stones.length == 0) {
      stones.push(num);
      break;
    }

    if (y != x) {
      for (let i = 0; i < stones.length; ++i) {
        if (num >= stones[i]) {
          stones.splice(i, 0, num);
          break;
        } else if (i == stones.length - 1) {
          stones.push(num);
          break;
        }
      }
    }
  }

  return stones[0];
};

// 빠른 코드
// sort 을 계속 하는게 더 빠르다...
var lastStoneWeight = function (stones) {
  while (stones.length > 1) {
    stones.sort((a, b) => b - a);
    let max1 = stones.shift();
    let max2 = stones.shift();
    if (max1 != max2) stones.push(max1 - max2);
  }

  return stones[0] || 0;
};

var lastStoneWeight = function (stones) {
  while (stones.length > 1) {
    let max1 = Math.max(...stones);
    stones.splice(stones.indexOf(max1), 1);
    let max2 = Math.max(...stones);
    stones.splice(stones.indexOf(max2), 1);
    if (max1 != max2) stones.push(max1 - max2);
  }
  return stones[0] || 0;
};
