// 왜 그리디인지 잘 이해안됨...
var robotSim = function (commands, obstacles) {
  let way = [
    [0, 1],
    [-1, 0],
    [0, -1],
    [1, 0],
  ];
  let nowWay = 3;
  let x = 0;
  let y = 0;
  let max = 0;
  let map = new Map();
  for (let ele of obstacles) {
    map["" + ele[1] + "_" + ele[0]] = true;
  }

  for (let cmd of commands) {
    if (cmd == -1) {
      // 오른쪽으로 90도
      nowWay = (nowWay + 1) % 4;
    } else if (cmd == -2) {
      // 왼쪽으로 90도
      nowWay = (nowWay - 1 + 4) % 4;
    } else {
      for (let i = 1; i <= cmd; ++i) {
        let newY = y + way[nowWay][0];
        let newX = x + way[nowWay][1];
        if (map["" + newY + "_" + newX]) {
          break;
        } else {
          y = newY;
          x = newX;
        }
      }
    }

    if (max < y * y + x * x) {
      max = y * y + x * x;
    }
  }

  return max;
};
