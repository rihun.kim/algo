// 아이디어
// 좋은 그리디 문제이다.
// 순간의 선택이 최적인 경우를 생각하도록 만든 문제
// 그냥 단순한 방법으론 그리디한 결과가 안나오도록
// 꼬은 문제이다.
// 낙폭을 이용하는 것이 핵심이였던 문제

var twoCitySchedCost = function (costs) {
  let sum = 0;

  let aCnt = costs.length / 2;
  let bCnt = costs.length / 2;
  costs.sort((a, b) => Math.abs(b[0] - b[1]) - Math.abs(a[0] - a[1]));
  for (let i = 0; i < costs.length; ++i) {
    if ((costs[i][0] < costs[i][1] || bCnt == 0) && aCnt != 0) {
      --aCnt;
      sum += costs[i][0];
    } else {
      --bCnt;
      sum += costs[i][1];
    }
  }

  return sum;
};
