// 아이디어
// 그리디 문제인데
// 아래와 같이 하나라도 안되는 컬럼은 최적의 답이 될 수 없다.
// If a column isn't sorted, it can't be part of the final answer.
// 그리디적으로 최적인 안되는 경우는 버림
// 최적의 답만 도출하는 경우만 챙김

// "매 선택에서 지금 이 순간 당장 최적인 답을 선택하여 적합한 최종 결과를 도출하자"
// 라는 모토를 갖고 있다.
// 그리디 알고리즘을 한마디로 설명한다면 그 유명한 마시멜로 실험에 비유할 수 있겠다.
// 그리디 알고리즘을 사용한다는 것은 지금 당장 눈 앞에 있는 마시멜로를 먹는 것이다.
// 하지만 이 방법을 사용하는 것은 "기다렸다가 2개를 먹는다"라는 최적해를 보장해주지 못한다.

// 그리디는
// greedy choice property, optimal substructure
// 을 선택하는 데 강점인 알고리즘이다.
// 즉, 한번의 선택이 다음 선택에는 전혀 무관한 값이어야 하며
// 매 순간의 최적의 해가 문제에 대한 최적의 해여야 한다.

// 그리디는 굉장히 직관적인 알고리즘으로
// 인간이 대부분 생각하는 방식이다.

var minDeletionSize = function (A) {
  let delNum = 0;
  for (let col = 0; col < A[0].length; ++col) {
    for (let row = 0; row < A.length - 1; ++row) {
      if (A[row][col] > A[row + 1][col]) {
        ++delNum;
        break;
      }
    }
  }

  return delNum;
};
