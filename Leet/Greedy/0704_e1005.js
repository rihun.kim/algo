// 아이디어
// 그리디 문제
// 최소값의 부호를 바꾸게 되면 최종적으로 가장 최적의 해가 나오게 됨
// 모든 경우의 합을 계속 구할 필요가 없음

var largestSumAfterKNegations = function (A, K) {
  A = A.sort((a, b) => a - b);

  for (let i = 0; i < K; ++i) {
    let num = A.shift();

    num = num * -1;
    for (let j = 0; j < A.length; ++j) {
      if (A[j] > num) {
        A.splice(j, 0, num);
        break;
      } else if (j == A.length - 1) {
        A.push(num);
        break;
      }
    }
  }

  let sum = 0;
  A.map((a) => (sum += a));
  return sum;
};
