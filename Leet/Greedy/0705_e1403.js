var minSubsequence = function (nums) {
  nums.sort((a, b) => b - a);
  let total = nums.reduce((prev, val) => prev + val);

  let ans = [];
  let sum = 0;
  for (let num of nums) {
    sum += num;
    ans.push(num);
    total -= num;
    if (sum > total) {
      break;
    }
  }

  return ans;
};
