// 아이디어
// 그리디는 항상 풀이 자체는 쉽다
// 하지만, 이를 DP 접근하는 순간
// 엄청 어려운 문제가 된다.
// 대부분 정렬이나, 갯수를 세는 것과 같은 것으로
// 문제를 해결하는 방식을 바꾼다.
// 항상 매 순간의 선택이 최적의 결과를 이끌어내는 것이
// 자명하다고 느끼게 만든다.

var balancedStringSplit = function (s) {
  let sum = 0;
  let rStack = [];
  let lStack = [];

  for (let ele of s) {
    if (ele == "L") lStack.push(ele);
    else rStack.push(ele);

    if (rStack.length == lStack.length) {
      ++sum;
      rStack = [];
      lStack = [];
    }
  }

  return sum;
};
