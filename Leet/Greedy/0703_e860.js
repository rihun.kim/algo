// 그리디 문제
// 전형적인 동전 나눠주기 문제인 그리디 문제
// 큰 돈이면 작은 돈으로 주는 것이 아닌,
// 최대한 높은 잔돈을 줘서
// 잔돈의 양을 최대로 유지하는 것
// 그리디적으로 현실에 최대의 효과를 보려고 하고
// 그것이 최적의 결과를 만듬

var lemonadeChange = function (bills) {
  let fiveCnt = 0;
  let tenCnt = 0;

  for (let ele of bills) {
    if (ele == 5) {
      ++fiveCnt;
    } else if (ele == 10) {
      if (fiveCnt == 0) return false;
      --fiveCnt;
      ++tenCnt;
    } else {
      if (tenCnt != 0) {
        --tenCnt;
        if (fiveCnt != 0) {
          --fiveCnt;
        } else {
          return false;
        }
      } else {
        fiveCnt -= 3;
        if (fiveCnt < 0) return false;
      }
    }
  }

  return true;
};
