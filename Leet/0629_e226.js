/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {TreeNode}
 */

// 아이디어
// 재귀를 쓰자.
// 최상단 루트 노드를 넣으면
// 루트 노드의 좌와 우를 바꿈
// 그다음 다시 각각 좌노드를 루트로 해서 돌림, 우도돌림
// 센티널은 좌와 우가 없으면 그냥 리턴

function recurv(root) {
  if (root == null) return;

  let temp = root.left;
  root.left = root.right;
  root.right = temp;

  recurv(root.left);
  recurv(root.right);
}

var invertTree = function (root) {
  // console.log(root);
  recurv(root);
  return root;
};
