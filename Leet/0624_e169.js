// 169. Majority Element

/**
 * @param {number[]} nums
 * @return {number}
 */
var majorityElement = function (nums) {
  let map = new Map();

  for (let num of nums) {
    if (map[num] == null) {
      map[num] = 1;
    } else {
      ++map[num];
    }
  }

  let maxIndex = nums[0];
  for (let key in map) {
    if (map[maxIndex] < map[key]) {
      maxIndex = key;
    }
  }

  return maxIndex;
};
