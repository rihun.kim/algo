// 28. Implement strStr()

var strStr = function (haystack, needle) {
  if (needle.length === 0) return 0;

  for (let i = 0; i < haystack.length; i++) {
    let k = i;
    for (let j = 0; j < needle.length; ++j) {
      if (haystack[k] === needle[j]) {
        ++k;
        if (j === needle.length - 1) return i;
      } else {
        break;
      }
    }
  }

  return -1;
};
