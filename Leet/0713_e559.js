/**
 * // Definition for a Node.
 * function Node(val,children) {
 *    this.val = val;
 *    this.children = children;
 * };
 */

/**
 * @param {Node} root
 * @return {number}
 */

const recurv = (node, depth) => {
  if (node.children == null) return depth;

  let maxDepth = depth;
  for (let child of node.children) {
    let dep = recurv(child, depth + 1);
    if (maxDepth < dep) {
      maxDepth = dep;
    }
  }

  return maxDepth;
};

var maxDepth = function (root) {
  if (root == null) return 0;
  return recurv(root, 1);
};
