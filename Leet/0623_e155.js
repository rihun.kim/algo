/**
 * initialize your data structure here.
 */

function Node() {
  this.data;
  this.prev;
  this.next;
}

var MinStack = function () {
  this.head = null;
  this.last = null;
  this.count = 0;
};

MinStack.prototype.push = function (x) {
  if (this.count == 0) {
    const newLtNode = new Node();
    this.head = newLtNode;
    newLtNode.data = x;
    newLtNode.next = null;
    this.last = this.head;
    this.last.prev = this.head;
    this.count++;
  } else {
    this.count++;
    const newLtNode = new Node();
    newLtNode.data = x;
    newLtNode.prev = this.last;
    newLtNode.last = null;
    this.last = newLtNode;
  }
};

/**
 * @return {void}
 */
MinStack.prototype.pop = function () {
  const val = this.last.data;
  this.last = this.last.prev;
  this.count--;
};

/**
 * @return {number}
 */
MinStack.prototype.top = function () {
  return this.last.data;
};

/**
 * @return {number}
 */
MinStack.prototype.getMin = function () {
  let lastNode = this.last;
  let minVal = this.head.data;

  while (lastNode != this.head) {
    if (minVal >= lastNode.data) {
      minVal = lastNode.data;
    }
    lastNode = lastNode.prev;
  }
  return minVal;
};
