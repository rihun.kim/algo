/**
 * Definition for a binary tree node.
 * function TreeNode(val) {
 *     this.val = val;
 *     this.left = this.right = null;
 * }
 */

/**
 * @param {TreeNode} root
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {TreeNode}
 */

// 아이디어
// BST 트리이므로, 어떠한 값을 넣으면 자동 정렬된다.
// 반복문을 만들어 따라서 주어진 수를 넣으면서 찾아가는 길을 저장한다.
// 찾으면 멈추고 나온다.
// 각각 넣은 것을 비교할때, lowest 이므로, 값이 달라 브랜치를 타는 곳을 선정한다.
// 한쪽이 null 이면 그대로 비교하면 된다.
// 브랜치를 일으키는 바로 그 지점이 lowest 이다.

function travel(start, num) {
  let stack = [];
  while (start != null) {
    if (start.val === num) {
      stack.push(start);
      break;
    }
    stack.push(start);
    if (num > start.val) start = start.right;
    else start = start.left;
  }
  return stack;
}

var lowestCommonAncestor = function (root, p, q) {
  let start = root;

  let pArr = travel(start, p.val);
  let pArrLen = pArr.length;
  let qArr = travel(start, q.val);
  let qArrLen = qArr.length;
  for (let i = 0; i < pArrLen && i < qArrLen; i++)
    if (pArr[i].val != qArr[i].val) return pArr[i - 1] || qArr[i - 1];

  return pArrLen > qArrLen ? qArr[qArrLen - 1] : pArr[pArrLen - 1];
};
