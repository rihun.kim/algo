/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */

// 아이디어
// t 가 s 의 애나그램인지 알아야함
// 애너그램은 서로 글자수가 크기가 같다.
// 그리고 들어있는 알파벳 종류가 같아야한다.
// 해쉬맵으로 풀어라.

var isAnagram = function (s, t) {
  if (s.length != t.length) return false;

  let arrS = s.split("");
  let arrT = t.split("");
  let mapS = new Map();
  let mapT = new Map();

  for (let i = 0; i < s.length; ++i) {
    if (!mapS[arrS[i]]) mapS[arrS[i]] = 1;
    else ++mapS[arrS[i]];

    if (!mapT[arrT[i]]) mapT[arrT[i]] = 1;
    else ++mapT[arrT[i]];
  }

  for (let key in mapS) {
    if (mapT[key] != mapS[key]) return false;
  }

  return true;
};
