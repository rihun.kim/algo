/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */
/**
 * @param {ListNode} node
 * @return {void} Do not return anything, modify node in-place instead.
 */

// 아이디어
// 헤당 노드의 앞과 뒤 노드를 서로 이어준다. 이렇게 하면 안됨
// next 로 가면서, 자기 위치까지 올 때까지 찾아야한다. 이렇게도 안됨
// 하나씩 값을 복사하면서 해야함, 마지막 노드를 잘라서 없애야함.

var deleteNode = function (node) {
  let nowNode = node;
  let prevNode;

  while (nowNode.next != null) {
    nowNode.val = nowNode.next.val;
    prevNode = nowNode;
    nowNode = nowNode.next;
  }
  prevNode.next = null;
};
