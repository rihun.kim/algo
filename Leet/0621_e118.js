// 118. Pascal's Triangle

function pascal(numRows) {
  let row = [0, 1, 0];
  let ans = [[1]];

  for (let k = 1; k < numRows; ++k) {
    let temp = [];

    for (let i = 1; i < row.length; ++i) temp.push(row[i - 1] + row[i]);

    ans.push([...temp]);
    row = [0, ...temp, 0];
  }

  return ans;
}

var generate = function (numRows) {
  if (numRows == 0) return [];

  return pascal(numRows);
};
