/**
 * @param {number[]} nums
 * @param {number} k
 * @return {boolean}
 */

// 아이디어
// 반복문을 준비하여 nums 을 돌린다.
// map 에 nums 을 키값으로 하고 밸류는 index 로 한다.
// 반복문이 끝나면, map 을 반복문 돈다.
// map 내부에 밸류 값들중 K 이하가 있으면 true
// 이 반복문 내에서 절대값의 차가 K 이상이면 돌지 않음
// 반복문 끝나면 false

var containsNearbyDuplicate = function (nums, k) {
  let map = new Map();

  for (let i = 0; i < nums.length; i++) {
    if (map[nums[i]] === undefined) {
      map[nums[i]] = [i];
    } else {
      map[nums[i]].push(i);
    }
  }

  for (let key in map) {
    for (let i = 0; i < map[key].length; ++i) {
      for (let j = i + 1; j < map[key].length; ++j) {
        if (Math.abs(map[key][i] - map[key][j]) <= k) {
          return true;
        }
      }
    }
  }

  return false;
};
