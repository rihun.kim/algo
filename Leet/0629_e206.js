/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {ListNode}
 */
// 아이디어
// stack 을 이용한다.
// 처음에 루프를 돌면서 하나씩 넣는다.
// 다 채우면, 뒤에서 부터 하나씩 빼면서, 앞에꺼와 연결한다.
// 앞에꺼와 연결은 나온놈의 next를 stack 안에 있는 놈으로 가리키면 된다.
// 마지막꺼는 자동으로 next 를 null 로 할 것.

var reverseList = function (head) {
  if (head == null) return null;

  let stack = [];
  while (head != null) {
    stack.push(head);
    head = head.next;
  }

  let start = stack[stack.length - 1];
  let node;
  let prevNode;
  while (stack.length != 0) {
    node = stack.pop();
    prevNode = stack[stack.length - 1];
    node.next = prevNode;
  }

  return start;
};
