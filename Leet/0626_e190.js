// 190. Reverse Bits

var reverseBits = function (n) {
  // 10진수를 2진수로 표현하는 법
  let str = "" + n.toString(2);

  // 1 을 001 처럼 표현하는 법
  str = str.split("").reverse().join("") + new Array(32 - str.length + 1).join("0");

  return parseInt(str, 2);
};

var reverseBits = function (n) {
  let str = n.toString(2);
  let arr = str.split("");
  while (arr.length < 32) arr.unshift(0);
  arr.reverse();
  str = arr.join("");

  return parseInt(str, 2);
};
