// 38. Count and Say

// 현재와 다음꺼 간의 관계를 통한 로직 전개 문제
// current 와 sum 을 잘 해나아가야함
var countAndSay = function (n) {
  let result = "1";

  while (n-- > 1) {
    let next = "";
    let current = result[0];
    let count = 1;

    for (let i = 1; i < result.length + 1; i++) {
      if (result[i] === current) {
        count += 1;
      } else {
        next += `${count}${current}`;
        count = 1;
        current = result[i];
      }
    }

    result = next;
  }

  return result;
};
