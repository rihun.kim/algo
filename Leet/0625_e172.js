// 172. Factorial Trailing Zeroes

var trailingZeroes = function (n) {
  let twoCnt = 0;
  let fiveCnt = 0;

  for (let i = 0; i <= n; i += 5) {
    if (Number.isInteger(i / 5)) {
      let temp = i;
      while (temp >= 5 && temp % 5 == 0) {
        temp = temp / 5;
        ++fiveCnt;
      }
    }
  }

  return twoCnt >= fiveCnt ? fiveCnt : twoCnt;
};

// 신박한 풀이 방법...
// 팩토리얼이기에 5 로 나눈 몫의 값이 나머지 5 의 잔존물들로 구성됨을 이용!
// 2 는 생각하지 않음. 왜냐하면 짝수이므로 반드시 5보다 남음
var trailingZeroes = function (n) {
  if (n == 0) return 0;

  return parseInt(n / 5) + trailingZeroes(parseInt(n / 5));
};
