// 167. Two Sum II - Input array is sorted

// 앞에서 뒤에서 하나씩 포인터를 잡고 서로 맞다아 가는 로직 구현!
// O(n) 만큼 걸림!
var twoSum = function (numbers, target) {
  for (let f = 0, l = numbers.length - 1; f <= l; ) {
    let sum = numbers[f] + numbers[l];
    if (sum < target) {
      ++f;
    } else if (sum > target) {
      --l;
    } else {
      return [f + 1, l + 1];
    }
  }
};

var twoSum = function (numbers, target) {
  const newNums = numbers.filter((num) => {
    if (target <= 0) return true;
    return num <= target;
  });

  console.log(newNums);

  for (let i = 0; i < newNums.length; ++i) {
    let first = newNums[i];
    for (let j = i + 1; j < newNums.length; ++j) {
      if (newNums[j] + first == target) {
        return [i + 1, j + 1];
      }
    }
  }
};
