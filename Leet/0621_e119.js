// 119. Pascal's Triangle II

function pascal(rowIndex) {
  let row = [0, 1, 0];
  let ans = [[1]];

  for (let k = 0; k < rowIndex; ++k) {
    let temp = [];

    for (let i = 1; i < row.length; ++i) temp.push(row[i - 1] + row[i]);

    if (k == rowIndex - 1) return [...temp];
    row = [0, ...temp, 0];
  }

  return ans;
}

var getRow = function (rowIndex) {
  return pascal(rowIndex);
};
