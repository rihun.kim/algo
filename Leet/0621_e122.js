// 122. Best Time to Buy and Sell Stock II

// 수학적으로 C >= A + B 이며, Greedy 적으로 생각해서 푸는 문제
function cal(prices, start) {
  let maxProfit = 0;
  for (let i = 0; i < prices.length - 1; ++i) {
    if (prices[i + 1] - prices[i] > 0) {
      maxProfit += prices[i + 1] - prices[i];
    }
  }

  return maxProfit;
}

var maxProfit = function (prices) {
  return cal(prices, 0);
};
