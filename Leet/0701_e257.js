/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {string[]}
 */
function travel(root, sen, stack) {
  if (root.left == null && root.right == null) {
    stack.push(sen + root.val);
    return;
  }

  if (root.left != null) travel(root.left, sen + root.val + "->", stack);
  if (root.right != null) travel(root.right, sen + root.val + "->", stack);
}

var binaryTreePaths = function (root) {
  if (root == null) return [];
  let sen = "";
  let stack = [];
  travel(root, sen, stack);
  return stack;
};
