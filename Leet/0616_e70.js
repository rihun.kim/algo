// 70. Climbing Stairs

// n1 은 한 개의 계단을 올라가는 방법의 수
// n1 = 1 ( 0 -> 1 )
// n2 = 2 ( n1 -> 1 , 0 -> 2)
// n3 = 3 ( n2 -> 1, n1 -> 2 )
// n4 = 5 ( n3 -> 1, n2 -> 2 )
// n5 = 8 ( n4 -> 1, n3 -> 2)
// 전형적인 DP 문제

var climbStairs = function (n) {
  if (n == 1 || n == 2) return n;
  let ans;
  for (let i = 3, a = 1, b = 2; i <= n; ++i) {
    ans = a + b;
    a = b;
    b = ans;
  }

  return ans;
};
