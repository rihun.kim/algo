var hammingWeight = function (n) {
  let str = n.toString(2);
  let arr = str.split("");
  let sum = 0;

  for (let i = 0; i < arr.length; ++i) {
    sum += Number(arr[i]);
  }
  return sum;
};
