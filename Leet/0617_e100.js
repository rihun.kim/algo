// 100. Same Tree

function TreeNode(val, left, right) {
  this.val = val === undefined ? 0 : val;
  this.left = left === undefined ? null : left;
  this.right = right === undefined ? null : right;
}

const p = new TreeNode(1);
p.left = new TreeNode(2);
p.right = null;
const q = new TreeNode(1);
q.left = null;
q.right = new TreeNode(2);

// 재귀 함수 트리
const test = (p, q) => {
  if (p == null && q == null) return true;
  if (q == null || p == null) return false;
  if (p.val != q.val) return false;
  return test(p.right, q.right) && test(p.left, q.left);
};

console.log("ans: ", test());
