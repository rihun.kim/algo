// 168. Excel Sheet Column Title

// 세부적인 값으로 로직이 안 만들어질떄는 로직을 새로 구현해라.
function title(num) {
  if (num <= 26) return String.fromCharCode(65 + num - 1);

  let share = Math.floor(num / 26);
  let remainder = Math.floor(num % 26);

  if (remainder == 0) {
    return title(--share) + String.fromCharCode(65 + 25);
  } else {
    return title(share) + title(remainder);
  }
}

var convertToTitle = function (n) {
  return title(n);
};
