/**
 * @param {number} n
 * @return {number}
 */

// 아이디어
// 짝수면, 2 제외하고 다 제외
// for 문 두방
// 바깥 for 문은 주어진 숫자까지 홀수를 봐주기
// 내부 for 문은 그 수가 다른 수로 나눠지는지 반복문
// 소수는 1과 자기 자신으로만 나눠지는 수
// % == 0 을 써서 판별
// 위 방법을 쓰면 타임 아웃걸림
// 나눠진 숫자들의 배수는 제거
// 에라토스테네스의 체 방식을 이용할 것!

var countPrimes = function (n) {
  if (n <= 2) return 0;

  let arr = [];
  for (let i = 3; i < n; i += 2) arr.push(i);

  let map = new Map();
  let newArr = [];
  for (let num of arr) {
    if (map[num]) continue;
    for (let i = 3; i * num < n; ++i) map[i * num] = true;
    newArr.push(num);
  }

  return newArr.length + 1;
};
