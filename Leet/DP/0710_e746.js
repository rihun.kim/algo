/**
 * @param {number[]} cost
 * @return {number}
 */
var minCostClimbingStairs = function (cost) {
  let d = [];

  d[0] = cost[0];
  d[1] = cost[1];

  for (let i = 2; ; ++i) {
    if (i >= cost.length) break;
    d[i] = Math.min(d[i - 2] + cost[i], d[i - 1] + cost[i]);
  }

  return Math.min(d[cost.length - 1], d[cost.length - 2]);
};
