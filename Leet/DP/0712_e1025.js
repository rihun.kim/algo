// 아이디어
// d[1] : 숫자 1일 때, 이긴 사람은 true 면 앨리스, false 면 밥
// d[3] = 1, d[2] 이므로 d[2] 값이 바껴서 출력하면됨

var divisorGame = function (N) {
  let d = [];
  d[1] = false;
  d[2] = true;

  for (let n = 3; n <= N; ++n) {
    for (let x = 1; x < N && n % x == 0; ++x) {
      if (!d[n - x]) {
        d[n] = true;
        break;
      }
    }
    if (!d[n]) d[n] = false;
  }

  return d[N];
};
