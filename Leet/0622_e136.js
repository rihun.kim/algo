// 136. Single Number

// HashTable 이용하는 문제
var singleNumber = function (nums) {
  let map = new Map();

  for (let num of nums) {
    if (map[num]) {
      map[num] = 2;
    } else {
      map[num] = 1;
    }
  }

  for (let num in map) {
    if (map[num] != 2) return num;
  }
};
