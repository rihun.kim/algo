/**
 * @param {string} s
 * @param {string} t
 * @return {boolean}
 */

// 아이디어
// 1. 문자열을 각각 쪼갠뒤
// 2. 문자열을 숫자로 노말 표준화 진행  (핵신 펑션)
// 3. 비교 진행
// 3. 맵에 각각의 문자(숫자)를 키 값으로 밸류는 해당 문자열이 나타난 인덱스값 넣기 (배열로)
//

const makeString = (str) => {
  let map = new Map();
  let newS = "";
  let indexNum = 0;

  for (let i = 0; i < str.length; ++i) {
    if (map[str[i]] != undefined) {
      newS += map[str[i]];
    } else {
      map[str[i]] = indexNum;
      newS += indexNum;
      indexNum++;
    }
  }

  return newS;
};

var isIsomorphic = function (s, t) {
  // console.log(makeString(s))
  // console.log(makeString(t))
  return makeString(s) == makeString(t) ? true : false;
};
