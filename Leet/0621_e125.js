// 125. Valid Palindrome

var isPalindrome = function (s) {
  let stack = [];
  s = s.toLowerCase();
  for (let i = 0; i < s.length; ++i) {
    if (
      ("A" <= s[i] && s[i] <= "Z") ||
      ("a" <= s[i] && s[i] <= "z") ||
      ("0" <= s[i] && s[i] <= "9")
    ) {
      stack.push(s[i]);
    }
  }

  for (let j = 0; j < stack.length / 2; ++j) {
    if (stack[j] != stack[stack.length - j - 1]) {
      return false;
    }
  }

  return true;
};
