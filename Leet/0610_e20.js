// 20. Valid Parentheses

var isValid = function (s) {
  if (s.length === 1) return false;

  let arr = [];
  for (let i = 0; i < s.length; ++i) {
    if (s[i] === "(" || s[i] === "{" || s[i] === "[") {
      arr.push(s[i]);
    } else {
      let popVal = arr.pop();
      if (popVal === undefined) {
        return false;
      } else if (popVal === "(" && s[i] !== ")") {
        return false;
      } else if (popVal === "{" && s[i] !== "}") {
        return false;
      } else if (popVal === "[" && s[i] !== "]") {
        return false;
      }
    }
  }

  return arr.length === 0 ? true : false;
};

//
var isValid = function (s) {
  const temp = {
    "(": ")",
    "{": "}",
    "[": "]",
  };

  const stack = [];
  const keys = Object.keys(temp);
  for (let i = 0; i < s.length; ++i) {
    if (keys.includes(s[i])) {
      stack.push(s[i]);
    } else {
      if (temp[stack.pop()] !== s[i]) {
        return false;
      }
    }
  }
  return !stack.length;
};
