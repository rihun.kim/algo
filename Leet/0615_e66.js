// 66. Plus One

var plusOne = function (digits) {
  let next = digits[digits.length - 1] + 1;
  for (let i = digits.length - 1; i >= 0; --i) {
    if (next === 10) {
      digits[i] = 0;
      if (digits[i - 1] === undefined) {
        digits.unshift(1);
        break;
      }
      next = digits[i - 1] + 1;
    } else {
      digits[i] += 1;
      break;
    }
  }

  return digits;
};
