// 83. Remove Duplicates from Sorted List

var deleteDuplicates = function (head) {
  if (!head) return null;

  let curr = head;
  let preVal;
  let arr = [];
  while (curr !== null) {
    if (curr.val != preVal) {
      arr.push(curr.val);
      preVal = curr.val;
    }
    curr = curr.next;
  }

  const root = new ListNode(arr.shift());
  curr = root;
  for (let val of arr) {
    curr.next = new ListNode(val);
    curr = curr.next;
  }

  return root;
};
