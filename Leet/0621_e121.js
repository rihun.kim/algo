// 121. Best Time to Buy and Sell Stock

var maxProfit = function (prices) {
  let minPrice = Number.MAX_SAFE_INTEGER;
  let maxProfit = 0;

  for (let i = 0; i < prices.length; ++i) {
    if (prices[i] < minPrice) minPrice = prices[i];
    else if (prices[i] - minPrice > maxProfit) maxProfit = prices[i] - minPrice;
  }
  return maxProfit;
};

var maxProfit = function (prices) {
  let maxProfit = 0;
  for (let i = 0; i < prices.length; ++i) {
    for (let j = i + 1; j < prices.length; ++j) {
      if (maxProfit < prices[j] - prices[i]) {
        maxProfit = prices[j] - prices[i];
      }
    }
  }
  return maxProfit;
};
