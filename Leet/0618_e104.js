// 104. Maximum Depth of Binary Tree

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number}
 */

// depth first search
function dfs(root, i, stack) {
  if (root == null) return;
  if (root.left != null) dfs(root.left, i + 1, stack);
  if (root.right != null) dfs(root.right, i + 1, stack);
  if (root.val != null) if (i > stack[0]) stack[0] = i;
}

var maxDepth = function (root) {
  if (root == null) return 0;

  let i = 1,
    stack = [1];
  dfs(root, i, stack);
  return stack[0];
};
