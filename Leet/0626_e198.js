// 198. House Robber

// 재귀로 풀었으나 시간 초과 아쉽...
function take(nums, n) {
  if (n < 0) return 0;
  if (n == 0) return nums[0];

  return Math.max(nums[n] + take(nums, n - 2), take(nums, n - 1));
}

var rob = function (nums) {
  return take(nums, nums.length - 1);
};

// 아쉽지만, 반복문으로..
// d[n] = max(nums[n] + d[n-2], d[n-1])
// d[1] = nums[1]
// 핵심은 d[2] 는 2번째꺼까지 최대효과를 본 상태를 의미한다는 것!
var rob = function (nums) {
  if (nums.length < 1) return 0;

  let len = nums.length;
  let d = [];
  d[0] = nums[0];
  d[1] = Math.max(nums[0], nums[1]);
  for (let i = 2; i < len; ++i) {
    d[i] = Math.max(nums[i] + d[i - 2], d[i - 1]);
  }

  return d[nums.length - 1];
};
