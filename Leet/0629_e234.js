/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @return {boolean}
 */

// 아이디어
// 스택을 준비한다.
// 하나씩 넣는다.
// 다 넣으면 reverse() 해서 비교한다.
var isPalindrome = function (head) {
  let stack = [];
  while (head != null) {
    stack.push(head.val);
    head = head.next;
  }

  let revStack = Array.prototype.slice.call(stack).reverse();

  return stack.join("") == revStack.join("") ? true : false;
};
