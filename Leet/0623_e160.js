/**
 * Definition for singly-linked list.
 * function ListNode(val) {
 *     this.val = val;
 *     this.next = null;
 * }
 */

/**
 * @param {ListNode} headA
 * @param {ListNode} headB
 * @return {ListNode}
 */
// 포인터 두개의 차이를 통해 푸는 문제
var getIntersectionNode = function (headA, headB) {
  let a = headA;
  let b = headB;

  while (a != b) {
    console.log(a, b);
    a = a ? a.next : headB;
    b = b ? b.next : headA;
  }

  return a;
};
