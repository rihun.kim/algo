/**
 * @param {number} n
 * @return {boolean}
 */

// 아이디어
// 2로 나눈다.
// 2로 나눴을 때, 나머지가 있으면 return false;
// 예외 경우는 1 이고, 바로 true;
var isPowerOfTwo = function (n) {
  if (n == 0) return false;
  if (n == 1) return true;

  while (n != 1) {
    n = n / 2;
    if (!Number.isInteger(n)) return false;
  }
  return true;
};
