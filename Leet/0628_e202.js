/**
 * @param {number} n
 * @return {boolean}
 */

// 아이디어
// 1. 들어온 숫자를 각각 쪼갠다.
// 2. 쪼갠 숫자를 제곱하여 더한다.
// 3. 1인지 판단하고 아니면, 더한 값은 Map 에 저장해둔다.
// 4. 다시 숫자들을 쪼갠다.
// 5. 반복

var isHappy = function (n) {
  let strNum = "" + n;

  let map = new Map();

  while (true) {
    let temp = strNum.split("");
    let sum = 0;
    for (let num of temp) sum += Math.pow(Number(num), 2);

    if (sum == 1) return true;
    if (!map[sum]) map[sum] = true;
    else return false;

    strNum = "" + sum;
  }
};
