// 67. Add Binary

var addBinary = function (a, b) {
  let stackA = [];
  let stackB = [];
  for (let aa of a) stackA.push(parseInt(aa));
  for (let bb of b) stackB.push(parseInt(bb));

  let over = 0;
  let ans = [];
  while (stackA.length != 0 && stackB.length != 0) {
    let sum = stackA.pop() + stackB.pop() + over;
    if (sum == 3) {
      over = 1;
      ans.unshift(1);
    } else if (sum == 2) {
      over = 1;
      ans.unshift(0);
    } else if (sum == 1) {
      over = 0;
      ans.unshift(1);
    } else {
      over = 0;
      ans.unshift(0);
    }
  }

  let temp = stackA.length != 0 ? stackA : stackB;
  while (temp.length != 0) {
    let sum = temp.pop() + over;
    if (sum == 2) {
      ans.unshift(0);
      over = 1;
    } else if (sum == 1) {
      ans.unshift(1);
      over = 0;
    } else {
      ans.unshift(0);
      over = 0;
    }
  }

  let res = "";
  if (over == 1) ans.unshift(1);
  for (let an of ans) res += an;

  return res;
};
