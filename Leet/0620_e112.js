// 112. Path Sum

function dfs(root, oSum, nSum) {
  if (root.left == null && root.right == null) {
    if (root.val + nSum == oSum) return true;
    return false;
  }

  let L, R;
  if (root.left != null) L = dfs(root.left, oSum, nSum + root.val);
  if (root.right != null) R = dfs(root.right, oSum, nSum + root.val);

  return !!L || !!R;
}

var hasPathSum = function (root, sum) {
  if (root == null) return false;
  return dfs(root, sum, 0);
};
