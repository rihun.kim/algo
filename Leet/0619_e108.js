// 108. Convert Sorted Array to Binary Search Tree

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} nums
 * @return {TreeNode}
 */
// balanced search tree
function bst(root, leftArr, rightArr) {
  if (leftArr.length != 0) {
    let leftHalf = parseInt(leftArr.length / 2);
    root.left = new TreeNode(leftArr[leftHalf]);
    bst(root.left, leftArr.slice(0, leftHalf), leftArr.slice(leftHalf + 1, leftHalf.length));
  }

  if (rightArr.length != 0) {
    let rightHalf = parseInt(rightArr.length / 2);
    root.right = new TreeNode(rightArr[rightHalf]);
    bst(root.right, rightArr.slice(0, rightHalf), rightArr.slice(rightHalf + 1, rightHalf.length));
  }
}

var sortedArrayToBST = function (nums) {
  if (nums.length == 0) return null;

  let half = parseInt(nums.length / 2);
  let root = new TreeNode(nums[half]);
  bst(root, nums.slice(0, half), nums.slice(half + 1, nums.length));
  return root;
};
