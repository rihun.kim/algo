// 9. Palindrome Number

var isPalindrome = function (x) {
  if (x < 0) return false;
  if (x === 0) return true;

  let arr = [];
  while (x > 0) {
    let lastDigit = x % 10; // 뒷자리부터 뽑아내기, 변수명 Digit 좋은듯
    arr.push(lastDigit);
    x = parseInt(x / 10); // 소수점 없애기 위해
  }

  for (let i = 0; i < arr.length / 2; ++i) {
    if (arr[i] !== arr[arr.length - i - 1]) {
      return false;
    }
  }

  return true;
};
