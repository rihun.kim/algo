// 141. Linked List Cycle

// HashTable 이용하는 문제
// 중복된 걸 구하는 건 모두 해쉬 문제로 바꿀 것
var hasCycle = function (head) {
  while (head != null) {
    if (head.check == undefined) {
      head.check = true;
    } else {
      return true;
    }
    head = head.next;
  }
  return false;
};

var hasCycle = function (head) {
  let slow = head;
  let fast = head;

  while (fast && fast.next) {
    slow = slow.next;
    fast = fast.next.next;
    if (slow == fast) return true;
  }
  return false;
};
