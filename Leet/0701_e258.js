// https://jeonyeohun.github.io/2020/01/08/258-Add-Digits.html
// https://tenderleo.gitbooks.io/leetcode-solutions-/GoogleEasy/258.html

// 반복문으론 그냥 풀지만,
// 수학적으론 이해가 안된다..
// 다음에 또보기

// 23 => 5
// 23 = 10*2 + 3 = 10 자리를 없애면서 10자리에 있는 고유의 자리수를 갖고 오기 위해선 9로 나눔 그러면 2가 나오고 3과 더하면 5가 됨

// 근데 이것을 왜 처음부터 9로 나누는 것인가?

/**
 * @param {number} num
 * @return {number}
 */
var addDigits = function (num) {
  if (num < 10) return num;

  let arr = [];
  while (true) {
    while (num >= 1) {
      arr.push(num % 10);
      num = parseInt(num / 10);
    }

    num = 0;
    for (let ele of arr) {
      num += ele;
    }
    arr = [];

    if (num < 10) return num;
  }
};
