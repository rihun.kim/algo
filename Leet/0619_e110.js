// 110. Balanced Binary Tree == AVL Tree

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {boolean}
 */

// AVL 트리문제, 재귀적으로 양쪽의 서브 트리의 차가 2 이상이면 AVL 트리가 아님을 이용
function avl(root) {
  if (root == null) return 0;

  let maxLeft = avl(root.left);
  let maxRight = avl(root.right);

  if (maxLeft == null || maxRight == null) return null;
  if (Math.abs(maxLeft - maxRight) > 1) return null;

  return Math.max(maxLeft, maxRight) + 1;
}

var isBalanced = function (root) {
  return avl(root) == null ? false : true;
};
