// 53. Maximum Subarray

// 현재와 다음꺼 간의 관계를 통한 로직 전개 문제
// current 와 sum 을 잘 해나아가야함
var maxSubArray = function (nums) {
  let curSum = nums[0];
  let maxSum = nums[0];

  for (let i = 1; i < nums.length; i++) {
    curSum = Math.max(nums[i], curSum + nums[i]);
    if (curSum > maxSum) {
      maxSum = curSum;
    }
  }
  return maxSum;
};
