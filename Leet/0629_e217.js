/**
 * @param {number[]} nums
 * @return {boolean}
 */

// 아이디어
// map 을 생성한다.
// nums 에 있는 값을 루프로 돌면서 하나씩 map 에 넣는다.
// map 에 있는 게 확인되면 바로 true
// 루프문 끝나면 false

var containsDuplicate = function (nums) {
  let map = new Map();

  for (let num of nums) {
    if (map[num] == undefined) map[num] = true;
    else return true;
  }

  return false;
};

let x = "";
x.substring();
