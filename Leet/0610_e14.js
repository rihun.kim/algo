// 14. Longest Common Prefix

var longestCommonPrefix = function (strs) {
  if (strs.includes("") || strs.length === 0) {
    return "";
  } else if (strs.length === 1) {
    return strs[0];
  }

  let model = strs.pop();
  for (let i = 0; i < model.length; ++i) {
    for (let j = 0; j < strs.length; ++j) {
      if (strs[j][i] !== model[i]) {
        return model.substring(0, i);
      }
    }
  }
  return model;
};

//
var longestCommonPrefix = function (strs) {
  if (!strs.length) return "";
  if (strs.length === 1) return strs[0];

  let prefix = strs[0];
  for (let i in strs) {
    while (strs[i].indexOf(prefix) !== 0) {
      prefix = prefix.substring(0, prefix.length - 1);
      if (!prefix.length) return "";
    }
  }

  return prefix;
};
