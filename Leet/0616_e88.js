// 88. Merge Sorted Array

var merge = function (nums1, m, nums2, n) {
  nums1.splice(m, nums1.length);

  let j = 0;
  for (let i = 0; i < nums1.length; ) {
    if (j >= nums2.length) break;

    if (nums1[i] >= nums2[j]) {
      nums1.splice(i, 0, nums2[j]);
      j++;
    } else {
      i++;
    }
  }

  for (; j < n; ++j) nums1.push(nums2[j]);

  return nums1;
};
