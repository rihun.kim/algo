// 21. Merge Two Sorted Lists

var mergeTwoLists = function (l1, l2) {
  let arr1 = [];
  let arr2 = [];
  let h1 = l1;
  let h2 = l2;
  while (l1 !== null) {
    arr1.push(l1.val);
    l1 = l1.next;
  }
  while (l2 !== null) {
    arr2.push(l2.val);
    l2 = l2.next;
  }

  if (arr1.length === 0 && arr2.length === 0) {
    return null;
  } else if (arr1.length === 0 && arr2.length !== 0) {
    return h2;
  } else if (arr2.length === 0 && arr1.length !== 0) {
    return h1;
  }

  let ans = [];
  let a1 = arr1.shift();
  let a2 = arr2.shift();
  while (true) {
    if (a1 >= a2) {
      ans.push(a2);
      if (arr2.length === 0) {
        ans.push(a1);
        while (arr1.length !== 0) {
          ans.push(arr1.shift());
        }
        break;
      }
      a2 = arr2.shift();
    } else if (a1 < a2) {
      ans.push(a1);
      if (arr1.length === 0) {
        ans.push(a2);
        while (arr2.length !== 0) {
          ans.push(arr2.shift());
        }
        break;
      }
      a1 = arr1.shift();
    }
  }

  let node = new ListNode(ans.shift());
  let root = node;
  for (let ele of ans) {
    node.next = new ListNode(ele);
    node = node.next;
  }

  return root;
};
