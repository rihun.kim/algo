// 아이디어
// 소인수분해 시도
// 2,3,5 이외에 숫자 있는지 파악
// 그냥 나눠야 한다.

var isUgly = function (num) {
  if (num == 1) return true;
  if (num <= 0) return false;

  while (num != 1) {
    for (let i = 2; i <= num; ++i) {
      if (num % 2 == 0) {
        num /= 2;
        break;
      } else if (num % 3 == 0) {
        num /= 3;

        break;
      } else if (num % 5 == 0) {
        num /= 5;

        break;
      } else {
        return false;
      }
    }
  }

  return true;
};
