// 35. Search Insert Position

// 비교 검사를 target 을 목적 대상으로 두고, nums 의 값으로 비교하는 것이 핵심
var searchInsert = function (nums, target) {
  for (let i = 0; i < nums.length; i++) {
    if (nums[i] >= target) {
      return i;
    }
  }
  return nums.length;
};

var searchInsert = function (nums, target) {
  if (nums[0] > target) return 0;

  for (let i = 0; i < nums.length; i++) {
    if (nums[i] === target) {
      return i;
    } else if (i !== nums.length - 1 && nums[i] < target && target < nums[i + 1]) {
      return i + 1;
    } else if (i === nums.length - 1) {
      if (nums[i] === target) {
        return i;
      } else {
        return i + 1;
      }
    }
  }
};
