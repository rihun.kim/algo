// 171. Excel Sheet Column Number

// DP 문제
var titleToNumber = function (s) {
  let sum = 0;

  for (let i = 0; i < s.length; ++i)
    sum += Math.pow(26, s.length - (i + 1)) * (s[i].charCodeAt() - 64);

  return sum;
};
