/**
 * Definition for singly-linked list.
 * function ListNode(val, next) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.next = (next===undefined ? null : next)
 * }
 */
/**
 * @param {ListNode} head
 * @param {number} val
 * @return {ListNode}
 */

// 아이디어
// 1. 노드를 도는 반복문을 제작
// 2. 값을 비교하고, 다르면 넘어가고
//    값이 같으면 이전 노드 next 에 뒤에 노드를 연결
//    만약 뒤에 노드가 없다면, 이전노드 next 에 null 넣으면 됨
// 이전노드 연결이 쉽지 않음...
var removeElements = function (head, val) {
  let prev = null;
  let present = head;

  while (present != null) {
    if (present.val == val) {
      if (prev == null) {
        head = present.next;
        prev = null;
        present = present.next;
      } else {
        if (present.next == null) {
          prev.next = null;
          break;
        }
        prev.next = present.next;
        present = present.next;
      }
    } else {
      prev = present;
      present = present.next;
    }
  }

  return head;
};
