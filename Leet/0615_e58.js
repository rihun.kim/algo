// 58. Length of Last Word

var lengthOfLastWord = function (s) {
  let stack = [];
  let word = "";
  for (let i = 0; i < s.length; ++i) {
    if (s[i] === " " && word !== "") {
      stack.push(word);
      word = "";
    } else if (s[i] !== " ") {
      word += s[i];
    }
  }

  if (word !== "") stack.push(word);

  return stack.length === 0 ? 0 : stack.pop().length;
};
