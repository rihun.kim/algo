// 107. Binary Tree Level Order Traversal II

/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[][]}
 */
// breadth first search
function bfs(root) {
  let queue = [root];
  let temps = [];
  let ans = [];
  let node = root;

  while (true) {
    while (queue.length != 0) {
      node = queue.shift();
      if (node.left != null) temps.push(node.left);
      if (node.right != null) temps.push(node.right);
    }
    if (temps.length == 0) break;
    let st = [];
    for (let temp of temps) st.push(temp.val);
    ans.unshift(st);
    queue = temps;
    temps = [];
  }
  ans.push([root.val]);
  return ans;
}

var levelOrderBottom = function (root) {
  if (root == null) return [];

  return bfs(root);
};
