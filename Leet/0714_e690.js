/**
 * Definition for Employee.
 * function Employee(id, importance, subordinates) {
 *     this.id = id;
 *     this.importance = importance;
 *     this.subordinates = subordinates;
 * }
 */

/**
 * @param {Employee[]} employees
 * @param {number} id
 * @return {number}
 */

function recurv(employees, id) {
  let em = employees.filter((employee) => employee.id == id)[0];
  if (em === undefined) return 0;

  let sum = em.importance;
  if (em.subordinates === undefined) return sum;

  for (let ele of em.subordinates) {
    sum += recurv(employees, ele);
  }

  return sum;
}

var GetImportance = function (employees, id) {
  return recurv(employees, id);
};
